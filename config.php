<?php

return [
	'apiEndpoint' => getenv('OPTIMY_API_ENDPOINT'),
	'apiKey' => getenv('OPTIMY_API_KEY'),
	'apiSecret' => getenv('OPTIMY_API_SECRET')
];
