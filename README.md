# Callback receiver LAMBDA

This is a rewrite of the original "callback-receiver" project.

# Configuration

Duplicate `config.example.php` as `config.php` and adapt to your needs.

# Tests

## Requirements

Run `composer install --dev -o`.

## Unit tests

Tests can be run using `vendor/bin/phpunit --bootstrap vendor/autoload.php tests/`.

OR

` docker run -it --net=host --rm --name my-running-crl -v "$PWD":/usr/src/myapp -w /usr/src/myapp php:7.2-cli php vendor/bin/phpunit tests/`

## Code coverage

Tests coverage can be generated using
`vendor/bin/phpunit --bootstrap vendor/autoload.php --coverage-html=tests-report tests/`
and then accessible in `tests-report/index.html`.

## Local development
run ` docker run -p 8081:80 -e OPTIMY_API_ENDPOINT="http://localhost:5000/v1.3" -e OPTIMY_API_KEY="aa" -e OPTIMY_API_SECRET="bb" --rm --name my-apache-php-app -v "$PWD":/var/www/html php:7.2-apache`

With the help of any HTTP REST client, send the following test payload to `http://localhost:8081/run.php`
```
{
  "api_key": "OISKLJ85T1HJPHMB4ZY5",
  "event_name": "project.statusChanged",
  "event_data": {
    "project_status_id": "d6478357-f37a-5353-9cba-3d782d867c5c"
  },
  "event_time": "2018-07-30 12:34:17",
  "object_type": "project",
  "object_id": "1adeaa24-ed76-5ba7-9eee-6b4d433cbdde",
  "callback_try_counter": "2",
  "callback_maximum_tries": 3
}
```

# AWS Lambda builder

Run `docker build . -t optimy/callback-receiver-lambda-builder` then `docker run --rm -it --mount type=bind,source="$(pwd)",target=/build-src/ optimy/callback-receiver-lambda-builder`.
The Lambda package will be available as a ZIP file in the `dist` folder.

# Automatic deployment

TODO

# Manual deployment

1. Setup an Optimy API user "Optimy scoring app";
2. Ensure Optimy API keys are setup in the `config.php` file;
3. Run the "AWS Lambda builder" step above;
4. Ensure that the following environment variables are defined: `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY`, `AWS_DEFAULT_REGION` and `CONSUL_HTTP_TOKEN`;
5. `cd src/terraform`;
6. Run `terraform init` and `terraform plan -out stack.plan`;
7. Run `terraform apply "stack.plan"`;
8. Edit the "Optimy scoring app" API user, enable "publish callbacks" to the `api_gateway_url` output from Terraform, 
  use version "v1.1" and enable needed triggers .