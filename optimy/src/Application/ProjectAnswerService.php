<?php

namespace Optimy\Application;


use Optimy\Infrastructure\Repositories\FormRepository;
use Optimy\Infrastructure\Repositories\ProjectRepository;

class ProjectAnswerService
{
    /**
     * @var ProjectRepository
     */
    private $projectRepository;

    /**
     * @var FormRepository
     */
    private $formRepository;

    public function __construct(ProjectRepository $projectRepository, FormRepository $formRepository)
    {
        $this->projectRepository = $projectRepository;
        $this->formRepository = $formRepository;
    }
    /**
     * @param string $projectId
     * @param string $versionId
     * @param string $formId
     * @param string $formPartId
     * @param string $formScreenId
     * @param array $addRemoveSectionIds
     * @return array
     */
    public function getAddRemoveSectionProjectAnswers(
        string $projectId,
        string $versionId,
        string $formId,
        string $formPartId,
        string $formScreenId,
        array $addRemoveSectionIds
    ): array
    {
        $projectAnswers = $this->projectRepository->getFormScreenAnswers($projectId, $versionId, $formScreenId);
        $addRemoveAnswers = [];

        foreach ($addRemoveSectionIds as $addRemoveSectionId) {
            $addRemoveAnswers = array_merge($addRemoveAnswers, $this->formRepository->getAddRemoveAnswersStructure($formId, $formPartId, $formScreenId, $addRemoveSectionId));
        }

        return $this->mapProjectAnswersToAddRemoves($projectAnswers, $addRemoveAnswers);
    }

    /**
     * Get all the ProjectAnswers split by add-remove section
     *
     * @param \Optimy\Domain\ProjectAnswer[] $answers of the screen
     * @param array $addRemoveElements
     * @return array of array of ProjectAnswer
     */
    private function mapProjectAnswersToAddRemoves(array $answers, array $addRemoveElements): array
    {
        // Get all ProjectAnswer ids
        $answerIds = array_map(
            function ($answer) {
                return $answer->getId();
            },
            $answers
        );

        $mappings = [];

        foreach ($addRemoveElements as $addRemoveAnswers) {
            // If there is a ProjectAnswer for the first element of the add-remove section,
            // that means the add-remove section has been filled in the project.
            // Mapping corresponding ProjectAnswers.
            // Todo might not be true all the time
            $projectAnswersExistForAddRemove = in_array($addRemoveAnswers[0]->getId(), $answerIds);
            if (!$projectAnswersExistForAddRemove) {
                continue;
            }

            // We start from the add-remove to keep the order of the answers
            $mapping = array_map(
                function ($addRemoveAnswer) use ($answers) {
                    $projectAnswer = array_filter(
                        $answers,
                        function ($answer) use ($addRemoveAnswer) {
                            return $answer->getId() === $addRemoveAnswer->getId();
                        });
                    return array_shift($projectAnswer);
                },
                $addRemoveAnswers);

            // We need to filter out null values
            array_push($mappings, array_values(array_filter($mapping)));
        }

        return $mappings;
    }
}