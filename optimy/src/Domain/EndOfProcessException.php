<?php
namespace Optimy\Domain;

use Throwable;

/**
 * Class EndOfProcessException
 *
 * This exception should be thrown when you want to end the process
 * as part of a normal behavior of the application.
 * This exception should NOT be used to notify the user that the process went wrong.
 * It must be used as part of a normal workflow to end the process (or part of it) without errors.
 *
 * Use cases:
 *  - An event is not handled by the service you are writing
 *  - The workflow does not meet a condition in your requirements
 *  - etc.
 *
 * The handling of this exception must be done on service level.
 *
 * @package Optimy\CallbackReceiverLambda\Utils
 */
class EndOfProcessException extends \Exception
{
    public function __construct(string $message = "", int $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}