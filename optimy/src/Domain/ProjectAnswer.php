<?php

namespace Optimy\Domain;


class ProjectAnswer
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $value;

    /**
     * @var string
     */
    private $questionId;

    /**
     * @var string
     */
    private $screenId;


    /**
     * ProjectAnswer constructor.
     * @param string $id
     * @param string $name
     * @param string $value
     * @param string $questionId
     * @param string $screenId
     */
    public function __construct(string $id, string $name, string $value, string $questionId, string $screenId)
    {
        $this->id = $id;
        $this->name = $name;
        $this->value = $value;
        $this->questionId = $questionId;
        $this->screenId = $screenId;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @return string
     */
    public function getQuestionId(): string
    {
        return $this->questionId;
    }

    /**
     * @return string
     */
    public function getScreenId(): string
    {
        return $this->screenId;
    }
}