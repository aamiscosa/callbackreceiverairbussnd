<?php

namespace Optimy\Domain;


class Project
{
    private $id;
    private $toolReference;
    private $name;
    private $statusId;
    private $formId;
    private $leaderTeamId;
    private $currentFormPartId;

    public function __construct(
        string $id,
        string $toolReference,
        ?string $name,
        string $statusId,
        string $formId,
        ?string $leaderTeamId,
        ?string $currentFormPartId
    ) {
        $this->id = $id;
        $this->toolReference = $toolReference;
        $this->name = $name;
        $this->statusId = $statusId;
        $this->formId = $formId;
        $this->leaderTeamId = $leaderTeamId;
        $this->currentFormPartId = $currentFormPartId;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getToolReference(): string
    {
        return $this->toolReference;
    }

    /**
     * @return null|string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getStatusId(): string
    {
        return $this->statusId;
    }

    /**
     * @return string
     */
    public function getFormId(): string
    {
        return $this->formId;
    }

    /**
     * @return string
     */
    public function getLeaderTeamId(): ?string
    {
        return $this->leaderTeamId;
    }

    /**
     * @return string
     */
    public function getCurrentFormPartId(): ?string 
    {
        return $this->currentFormPartId;
    }
}