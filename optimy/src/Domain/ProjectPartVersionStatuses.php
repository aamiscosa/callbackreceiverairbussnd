<?php

namespace Optimy\Domain;


use Esky\Enum\Enum;

class ProjectPartVersionStatuses extends Enum
{
    public const OBSOLETE = 0;
    public const DRAFT = 1;
    public const PUBLISHED = 2;
}