<?php

namespace Optimy\Domain;


class FormAnswer
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $systemName;

    /**
     * FormAnswer constructor.
     * @param string $id
     * @param string $name
     * @param string $systemName
     */
    public function __construct(string $id, ?string $name, string $systemName)
    {
        $this->id = $id;
        $this->name = $name;
        $this->systemName = $systemName;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getSystemName(): string
    {
        return $this->systemName;
    }
}