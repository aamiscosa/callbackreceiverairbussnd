<?php

namespace Optimy\Domain;


class ProjectPartVersion
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $formPartId;

    /**
     * @var int
     */
    private $version;

    /**
     * @var int
     */
    private $status;

    /**
     * @var string
     */
    private $submittedTime;

    public function __construct(
        string $id,
        string $formPartId,
        int $version,
        int $status,
        string $submittedTime
    ){
        $this->id = $id;
        $this->formPartId = $formPartId;
        $this->version = $version; // Number of the version
        $this->status = $status;
        $this->submittedTime = $submittedTime;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getFormPartId(): string
    {
        return $this->formPartId;
    }

    /**
     * @return int
     */
    public function getVersion(): int
    {
        return $this->version;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @return string
     */
    public function getSubmittedTime(): string
    {
        return $this->submittedTime;
    }
}