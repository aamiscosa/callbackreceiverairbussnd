<?php

namespace Optimy\Domain;


class EmailTemplate
{
     private $id;
     private $subject;
     private $body;

    /**
     * EmailTemplate constructor.
     * @param $id
     * @param $subject
     * @param $body
     */
    public function __construct(int $id, string $subject, string $body)
    {
        $this->id = $id;
        $this->subject = $subject;
        $this->body = $body;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getSubject(): string
    {
        return $this->subject;
    }

    /**
     * @return string
     */
    public function getBody(): string
    {
        return $this->body;
    }

    /**
     * @param array $patterns
     * @return $this
     */
    public function replaceTokens(array $patterns)
    {
        foreach ($patterns as $token => $replacement) {
            $this->body = preg_replace('/{' . $token . '}/', $replacement, $this->body);
        }

        return $this;
    }

    /**
     * @param string $tag
     * @return $this
     */
    public function appendTag(string $tag)
    {
        $this ->body = $this->body . '<br />' . '#'.$tag;
        return $this;
    }
}