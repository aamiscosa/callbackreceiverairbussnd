<?php

namespace Optimy\Domain;


class ProjectHistory
{
    /**
     * @var string
     */
    private $project_id;

    /**
     * @var string
     */
    private $event_date;

    /**
     * @var string
     */
    private $event_title;

    /**
     * @var string
     */
    private $event_title_tag;

    /**
     * @var string
     */
    private $event_description;


    /**
     * ProjectHistory constructor.
     * @param string $project_id
     * @param string $event_date
     * @param string $event_title
     * @param string $event_title_tag
     * @param string $event_description
     */
    public function __construct(
        string $project_id, 
        string $event_date, 
        string $event_title, 
        string $event_title_tag, 
        string $event_description
    )
    {
        $this->project_id = $project_id;
        $this->event_date = $event_date;
        $this->event_title = $event_title;
        $this->event_title_tag = $event_title_tag;
        $this->event_description = $event_description;
    }

    /**
     * @return string
     */
    public function getProjectId(): string
    {
        return $this->project_id;
    }

    /**
     * @return string
     */
    public function getEventDate(): string
    {
        return $this->event_date;
    }

    /**
     * @return string
     */
    public function getEventTitle(): string
    {
        return $this->event_title;
    }

    /**
     * @return string
     */
    public function getEventTitleTag(): string
    {
        return $this->event_title_tag;
    }

    /**
     * @return string
     */
    public function getEventDescription(): string
    {
        return $this->event_description;
    }
}