<?php

namespace Optimy\Domain;


class Task
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $taskType;

    /**
     * @var int
     */
    private $assignerId;

    /**
     * @var int
     */
    private $assigneeId;

    /**
     * @var string
     */
    private $dueDate;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $projectId;

    /**
     * @var string
     */
    private $completionDate;

    public function __construct(string $name, int $assignerId, int $assigneeId, string $dueDate, string $projectId)
    {
         $this->name = $name;
         $this->assignerId = $assignerId;
         $this->assigneeId = $assigneeId;
         $this->dueDate = $dueDate;
         $this->projectId = $projectId;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId(int $id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @param string $dueDate
     * @return $this
     */
    public function setDueDate(string $dueDate)
    {
        $this->dueDate = $dueDate;
        return $this;
    }

    /**
     * @param string $taskType
     * @return $this
     */
    public function setTaskType(string $taskType)
    {
        $this->taskType = $taskType;
        return $this;
    }

    /**
     * @param string $description
     * @return $this
     */
    public function addDescription(string $description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @param null|string $completionDate
     * @return $this
     */
    public function wasCompletedOn(?string $completionDate)
    {
        $this->completionDate = $completionDate;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getAssignerId(): int
    {
        return $this->assignerId;
    }

    /**
     * @return int
     */
    public function getAssigneeId(): int
    {
        return $this->assigneeId;
    }

    /**
     * @return string
     */
    public function getDueDate(): string
    {
        return $this->dueDate;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getProjectId(): string
    {
        return $this->projectId;
    }

    /**
     * @return mixed
     */
    public function getCompletionDate()
    {
        return $this->completionDate;
    }

    /**
     * @return string
     */
    public function getTaskType(): string
    {
        return $this->taskType;
    }

    /**
     * @return bool
     */
    public function isCompleted(): bool
    {
        return $this->completionDate !== null;
    }
}