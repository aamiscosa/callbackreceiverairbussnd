<?php

namespace Optimy\Infrastructure\Events;

/**
 * This event must be used as argument of the function execute()
 * inside the class TaskCompleteEventHandler.
 * It's the service responsibility to create this class.
 * TaskCompleteEventHandler MUST extends AbstractEventHandler
 *
 * Class TaskCompleteEvent
 * @package Optimy\Infrastructure\Events
 */
class TaskCompleteEvent
{

}