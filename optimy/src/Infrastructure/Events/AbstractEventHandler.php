<?php

namespace Optimy\Infrastructure\Events;


use Optimy\CallbackReceiverLambda\Application\BusinessFlows\BusinessFlowFactory;
use Optimy\Domain\EndOfProcessException;
use Optimy\Infrastructure\Events\AbstractEvent;
use Psr\Log\LoggerInterface;

abstract class AbstractEventHandler
{
    /**
     * @var LoggerInterface
     */
    protected $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function execute(AbstractEvent $event): bool
    {
        try {
            $this->logger->info("Executing {$event->getType()} with " . join(',', $event->toArray()));
            return $this->handle($event);

        } catch (EndOfProcessException $e) {
            // In this case we need to forward the exception to be handled on service level
            throw $e;

        } catch (\Exception $exception) {
            $this->logger->error("Failed: {$event->getType()} with " . join(',', $event->toArray()), [$exception->getMessage()]);
            return false;
        }
    }

    abstract protected function handle(\Optimy\Infrastructure\Events\AbstractEvent $event);
}