<?php

namespace Optimy\Infrastructure\Events;


use Esky\Enum\Enum;


/**
 * Class ObjectTypes
 * @package Optimy\CallbackReceiverLambda\EventHandlers
 */
class ObjectTypes extends Enum
{
    public const PROJECT = 'project';
    public const USER = 'user';
    public const FORM = 'form';
    public const FILE = 'file';
    public const TASK = 'task';
}