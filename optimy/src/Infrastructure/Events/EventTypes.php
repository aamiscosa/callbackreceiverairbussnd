<?php

namespace Optimy\Infrastructure\Events;


use Esky\Enum\Enum;


/**
 * Class EventTypes
 * Add here the event types to which the callback receiver should listen to
 * @package Optimy\CallbackReceiverLambda\EventHandlers
 */
class EventTypes extends Enum
{
    public const PROJECT_PART_VERSION_SUBMITTED = 'project.partVersionSubmitted';
    public const PROJECT_STATUS_CHANGED = 'project.statusChanged';
    public const WORKFLOW_HTTP_CALL= 'workflow.httpCall';
    public const TASK_COMPLETE = 'task.completed';
}