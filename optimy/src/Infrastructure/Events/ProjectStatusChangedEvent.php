<?php

namespace Optimy\Infrastructure\Events;


use Optimy\Infrastructure\Events\AbstractEvent;
use Optimy\Infrastructure\Events\EventTypes;
use Optimy\Infrastructure\Events\ObjectTypes;

/**
 * This event must be used as argument of the function execute()
 * inside the class ProjectStatusChangedEventHandler.
 * It's the service responsibility to create this class.
 * ProjectStatusChangedEventHandler MUST extends AbstractEventHandler
 *
 * Class ProjectStatusChangedEvent
 * @package Optimy\Infrastructure\Events
 */
class ProjectStatusChangedEvent extends \Optimy\Infrastructure\Events\AbstractEvent
{
    /**
     * @var string
     */
    private $projectStatusId;

    public function __construct(string $objectId, array $eventData)
    {
        // $objectId here refers to a projectId
        parent::__construct($objectId, $eventData);
        $this->type = EventTypes::PROJECT_STATUS_CHANGED;
        $this->objectType = \Optimy\Infrastructure\Events\ObjectTypes::PROJECT;
        $this->projectStatusId = $eventData['project_status_id'];
    }

    /**
     * @return string
     */
    public function getProjectStatusId(): string
    {
        return $this->projectStatusId;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return array_merge(
            parent::toArray(),
            [
                'project_status_id' => $this->getProjectStatusId()
            ]
        );
    }
}