<?php

namespace Optimy\Infrastructure\Events;


use Optimy\Infrastructure\Events\AbstractEvent;
use Optimy\Infrastructure\Events\EventTypes;
use Optimy\Infrastructure\Events\ObjectTypes;

/**
 * This event must be used as argument of the function execute()
 * inside the class ProjectPartVersionSubmittedEventHandler.
 * It's the service responsibility to create this class.
 * ProjectPartVersionSubmittedEventHandler MUST extends AbstractEventHandler
 *
 * Class ProjectPartVersionSubmittedEvent
 * @package Optimy\Infrastructure\Events
 */
class ProjectPartVersionSubmittedEvent extends \Optimy\Infrastructure\Events\AbstractEvent
{
    /**
     * @var string
     */
    private $projectPartVersionId;

    public function __construct(string $objectId, array $eventData = array())
    {
        // $objectId here refers to a projectId
        parent::__construct($objectId, $eventData);
        $this->type = \Optimy\Infrastructure\Events\EventTypes::PROJECT_PART_VERSION_SUBMITTED;
        $this->objectType = \Optimy\Infrastructure\Events\ObjectTypes::PROJECT;
        $this->projectPartVersionId = $eventData['project_part_version_id'];
    }

    public function getProjectPartVersionId(): string
    {
        return $this->projectPartVersionId;
    }

    public function toArray(): array
    {
        return array_merge(
            parent::toArray(),
            [
                'project_part_version_id' => $this->getProjectPartVersionId()
            ]
        );
    }
}