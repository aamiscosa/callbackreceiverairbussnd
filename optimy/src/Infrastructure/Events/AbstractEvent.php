<?php

namespace Optimy\Infrastructure\Events;



abstract class AbstractEvent
{
    protected $type;
    protected $objectType;
    protected $objectId;

    public function __construct(string $objectId, array $eventData)
    {
        $this->objectId = $objectId;
    }

    public final function getType(): string
    {
        return $this->type;
    }

    public final function getObjectType(): string
    {
        return $this->objectType;
    }

    public final function getObjectId(): string
    {
        return $this->objectId;
    }

    public function toArray(): array
    {
        return [
            'type' => $this->getType(),
            $this->getObjectType() => $this->getObjectId()
        ];
    }
}