<?php

namespace Optimy\Infrastructure\Repositories;


use Optimy\Domain\EmailTemplate;
use Optimy\Infrastructure\Repositories\Client;

class EmailTemplateRepository
{
    /**
     * @var Client
     */
    private $client;
    private const BASE_ROUTE = 'messaging-templates?language=';

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @param int $templateId
     * @param string $language
     * @return null|EmailTemplate
     */
    public function getTemplateById(int $templateId, string $language): ?EmailTemplate
    {
        $templates = array_map(
            function ($t) {
                return new EmailTemplate($t['id'], $t['subject'], $t['body']);
            },
            $this->client->get(self::BASE_ROUTE . $language)
        );

        $template = array_filter(
            $templates,
            function ($template) use ($templateId) {
                return $template->getId() === $templateId;
            });

        return array_shift($template);
    }

}