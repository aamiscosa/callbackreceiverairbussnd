<?php

namespace Optimy\Infrastructure\Repositories;


use Psr\Log\LoggerInterface;

class Client
{
    private $client;
    private $logger;

    public function __construct(LoggerInterface $logger, \GuzzleHttp\Client $client)
    {
        $this->logger = $logger;
        $this->client = $client;
    }

    public function get(string $path, array $placeholders = [], $queryParams = []): array
    {
        $url = $this->buildUrlWith($path, $placeholders, $queryParams);
        try {
            $this->logger->info("Executing GET request to the following url: {$url}");
            $response = $this->client->get($url)->getBody()->getContents();
            $data = json_decode($response, true)['data'];
            $this->logger->info("Service responded with {$response}");

        } catch (\Exception $e) {
            $this->logger->error("Failed: GET request to the following url: {$url}", ['exception' => $e->getMessage()]);
            $data = [];
        }

        return $data;
    }

    /**
     * @param string $path
     * @param $postData
     * @param array $placeholders
     * @return array
     */
    public function post(string $path, $postData, array $placeholders = [])
    {
        $url = $this->buildUrlWith($path, $placeholders);
        try {
            $this->logger->info("Executing POST request to the following url: {$url} with data: " . json_encode($postData));
            $response = $this->client->post($url, ['form_params' => $postData])->getBody()->getContents();
            $data = json_decode($response, true)['data'];
            $this->logger->info("Service responded with {$response}");

        } catch (\Exception $e) {
            $this->logger->error("Failed: post request to the following url: {$url} with data: " . json_encode($postData), ['exception' => $e->getMessage()]);
            $data = [
                'success' => false,
                'error' => $e->getMessage()
            ];
        }

        return $data;
    }

    /**
     * Replace placeholders in the url (for instance, defined as '/:placeholder')
     * by the value of the corresponding key in $placeholders
     *
     * @param string $path
     * @param array $placeholders
     * @param array $queryParams
     * @return string
     */
    private function buildUrlWith(string $path, array $placeholders = [], $queryParams = []): string
    {
        $url = preg_replace_callback('/:\w+/', function ($matches) use ($placeholders) {
                return $placeholders[$matches[0]];
            }, $path);

        if (count($queryParams)) {
            $suffix = []; //'?';
            foreach ($queryParams as $key => $value) {
                $suffix[] = $key . '=' . $value;
            }
            $url .= '?' . implode('&', $suffix);
        }

        return $url;
    }
}