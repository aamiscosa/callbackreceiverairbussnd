<?php

namespace Optimy\Infrastructure\Repositories;



class TaskAssigneeRepository
{
    /**
     * @var Client
     */
    private $client;

    private const BASE_ROUTE_TEAMS = 'optimy-groups/:team_id/users';
    private const BASE_ROUTE_ROLES = 'optimy-roles/:role_id/users';

    private const DEFAULT_ASSIGNEE_ID = 2210; //Anne Boyon-Fuster

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * Todo use cache
     *
     * This function returns the id of the user
     * to who the task needs to be assigned.
     * It depends on its role in the team
     * the project is part of.
     * If no user is found, the task is assigned to the
     * self::DEFAULT_USER_ID
     *
     * No object returned here, we only need the id
     *
     * @param int $teamId
     * @param string $roleId
     * @return int
     */
    public function getUserIdFromTeamWithGivenRole(int $teamId, string $roleId): int
    {
        $usersFromTeam = $this->getUsersFromTeam($teamId);
        $userIdsWithRole = array_map(
            function ($user) {
                return $user['id'];
            },
            $this->getUsersWithRole($roleId)
        );

        $taskAssignee = array_filter(
            $usersFromTeam,
            function ($user) use ($userIdsWithRole) {
                return in_array($user['id'], $userIdsWithRole);
            });

        $taskAssignee = array_shift($taskAssignee);

        return is_null($taskAssignee) ? self::DEFAULT_ASSIGNEE_ID : $taskAssignee['id'];
    }

    /**
     * @param string $teamId
     * @return array
     *
     * Format of the data:
     *  [
            [
                "id" => "3664",
                "name" => "Rose-Marie Capuano",
                "email" => "rose-marie.capuano@total.com",
                "language" => "fr"
            ],
            [
                "id" => "3716",
                "name" => "Anne-Claire Lienhardt",
                "email" => "anne-claire.lienhardt@total.com",
                "language" => "fr"
            ]
        ]
     */
    private function getUsersFromTeam(string $teamId): array
    {
        return $this->client->get(self::BASE_ROUTE_TEAMS, [':team_id' => $teamId]);
    }

    /**
     * @param string $roleId
     * @return array
     *
     * Format of the data:
     *  [
            [
                "id" => "3664",
                "name" => "Rose-Marie Capuano",
                "email" => "rose-marie.capuano@total.com",
                "language" => "fr"
            ],
            [
                "id" => "3716",
                "name" => "Anne-Claire Lienhardt",
                "email" => "anne-claire.lienhardt@total.com",
                "language" => "fr"
            ]
        ]
     */
    private function getUsersWithRole(string $roleId): array
    {
        return $this->client->get(self::BASE_ROUTE_ROLES, [':role_id' => $roleId]);
    }
}