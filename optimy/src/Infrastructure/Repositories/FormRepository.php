<?php

namespace Optimy\Infrastructure\Repositories;


use Optimy\Domain\FormAnswer;
use Optimy\Infrastructure\Repositories\Client;

class FormRepository
{
    /**
     * @var Client
     */
    private $client;
    private const BASE_ROUTE = 'forms/:form_id/parts/:part_id/screens/:screen_id/elements/:element_id';

    public function __construct(Client $client)
    {
        $this->client = $client;
    }


    /**
     * @param string $formId
     * @param string $partId
     * @param string $screenId
     * @param string $sectionId
     * @return array An array of arrays (corresponding to each section) of FormAnswer
     */
    public function getAddRemoveAnswersStructure(string $formId, string $partId, string $screenId, string $sectionId): array
    {
        $structure = $this->client->get(
            self::BASE_ROUTE,
            [
                ':form_id' => $formId,
                ':part_id' => $partId,
                ':screen_id' => $screenId,
                ':element_id' => $sectionId
            ]
        );

        $addRemoves = $structure['elements'];
        $addRemoveAnswers = [];

        foreach ($addRemoves as $section) {
            $questions = $section['elements'];
            $sectionAnswers = [];

            foreach ($questions as $question) {
                $answers = array_map(
                    function ($answer) {
                        // We take the french translation $answer['translations']['fr']['name']
                        // as the answer for readability/usability reason (mainly in case of dropdowns)
                        return new FormAnswer($answer['id'], $answer['translations']['fr']['name'], $answer['name']);
                    },
                    $question['elements']
                );

                $sectionAnswers = array_merge($sectionAnswers, $answers);
            }

            array_push($addRemoveAnswers, $sectionAnswers);
        }

        return $addRemoveAnswers;
    }
}