<?php

namespace Optimy\Infrastructure\Repositories;


use Optimy\Domain\Task;
use Optimy\Infrastructure\Repositories\Client;

class TaskRepository
{
    /**
     * @var Client
     */
    private $client;

    private const BASE_ROUTE = 'projects/:project_id/tasks';

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     *  Create or update task
     *
     * @param Task $task
     * @return bool
     */
    public function saveTask(Task $task)
    {
        $postData = [
            'name' => $task->getName(),
            'assignee' => $task->getAssigneeId(),
            'assigner' => $task->getAssignerId(),
            'due_date' => $task->getDueDate(),
            'description' => $task->getDescription(),
            'task_type_id' => $task->getTaskType(),
            'completed' => $task->isCompleted() ? 1 : 0,
            'completer' => $task->isCompleted() ? $task->getAssigneeId() : null
        ];

        $path = self::BASE_ROUTE . (is_null($task->getId()) ? '' : '/' . $task->getId());
        $taskId = $this->client->post($path, $postData, [':project_id' => $task->getProjectId()]);
        return array_key_exists('task_id', $taskId);
    }

    /**
     * @param string $projectId
     * @param bool $includeCompleted - include tasks that have been completed
     * @return Task[]
     */
    public function getProjectTasks(string $projectId, bool $includeCompleted = true)
    {
        $tasks = $this->client->get(self::BASE_ROUTE, [':project_id' => $projectId], ['include_completed' => $includeCompleted]);

        return array_map(
            function ($t) {
                $task = new Task($t['name'], $t['assigner_id'], $t['assignee_id'], $t['due_date'], $t['projectId']);
                $task->addDescription($t['description'])
                    ->setId($t['id'])
                    ->setTaskType($t['task_type_id'])
                    ->wasCompletedOn($t['completion_time']);

                return $task;
            },
            $tasks
        );
    }

    /**
     * @param string $projectId
     * @param string $tag - Value or TaskTags::class
     * @return Task[]
     */
    public function getProjectTasksByTag(string $projectId, string $tag): array
    {
        $tasks = $this->getProjectTasks($projectId, true);
        $tasks = array_values(
            array_filter($tasks, function ($task) use ($tag) {
                return strrpos($task->getDescription(), $tag) !== false;
            })
        );

        return $tasks;
    }
}