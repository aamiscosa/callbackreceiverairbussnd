<?php

namespace Optimy\Infrastructure\Repositories;

use Optimy\Domain\Project;
use Optimy\Domain\ProjectAnswer;
use Optimy\Domain\ProjectPartVersion;
use Optimy\Domain\ProjectPartVersionStatuses;
use Optimy\Domain\ProjectHistory;

class ProjectRepository
{
    /**
     * @var Client
     */
    private $client;
    private const BASE_ROUTE = 'projects/:project_id';

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @param string $projectId
     * @return null|\Optimy\Domain\Project
     */
    public function getProject(string $projectId): ?Project
    {
        $project = $this->client->get(self::BASE_ROUTE, [':project_id' => $projectId]);

        return $project ?
            new Project(
                $project['id'],
                $project['tool_reference'],
                $project['name'],
                $project['status_id'],
                $project['form_id'],
                $project['leader_team_id'],
                $project['current_form_part_id']
            ) :
            null;
    }

    /**
     * Returns an array of ppv structured as follow:
     * [
     *  'version_id' => $partVersion->id,
     *  'form_part_id' => $partVersion->formPartId,
     *  'version' => $version,
     *  'submitted_time' => $submittedTime,
     *  'status' => $partVersion->status (obsolete, draft, submitted)
     * ]
     * @param string $projectId
     * @return \Optimy\Domain\ProjectPartVersion[]
     */
    public function getProjectVersions(string $projectId): array
    {
        return array_map(
            function ($ppv) {
                return new ProjectPartVersion($ppv['version_id'], $ppv['form_part_id'], $ppv['version'], $ppv['status'], $ppv['submitted_time']);
            },
            $this->client->get(self::BASE_ROUTE . '/versions', [':project_id' => $projectId])
        );
	}

    /**
     * @param string $projectId
     * @param string $formPartId
     * @return null|ProjectPartVersion
     */
	public function getProjectLastPublishedVersionOfFormPart(string $projectId, string $formPartId): ?ProjectPartVersion
    {
        $versions = $this->getProjectVersions($projectId);
        $lastPublishedVersions = array_values(array_filter($versions, function ($version) use ($formPartId) {
            return $version->getFormPartId() === $formPartId && $version->getStatus() === ProjectPartVersionStatuses::PUBLISHED;
        }));

        usort($lastPublishedVersions, function ($a, $b) {
            return ($a->getVersion() < $b->getVersion()) ? -1 : 1;
        });

        return array_pop($lastPublishedVersions);
    }

    /**
     * @param string $projectId
     * @param string $versionId
     * @return ProjectPartVersion|null
     */
    public function getProjectVersionById(string $projectId, string $versionId): ?ProjectPartVersion
    {
        $versions = $this->getProjectVersions($projectId);
        $versionById = array_values(array_filter($versions, function ($version) use ($versionId) {
            return $version->getId() === $versionId;
        }));

        return count($versionById) === 0 ? null : $versionById[0];
    }

    /**
     * @param string $projectId
     * @param string $versionId
     * @param null|string $questionId
     * @return ProjectAnswer[]
     */
    public function getProjectAnswers(string $projectId, string $versionId, ?string $questionId = null): array
    {
        return array_map(
            function ($answer) {
                return new ProjectAnswer(
                    $answer['answer_id'],
                    $answer['answer_name'],
                    $answer['value'],
                    $answer['question_id'],
                    $answer['screen_id']
                );
            },
            $this->client->get(
                self::BASE_ROUTE . '/versions/:versionId/answers',
                [':project_id' => $projectId, ':versionId' => $versionId],
                is_null($questionId) ? [] : ['question_id' => $questionId]
            )
        );
    }

    /**
     * @param string $projectId
     * @param string $versionId
     * @param string $screenId
     * @return ProjectAnswer[]
     */
    public function getFormScreenAnswers(string $projectId, string $versionId, string $screenId)
    {
        $answers = $this->getProjectAnswers($projectId, $versionId);

        $answers = array_values(array_filter(
            $answers,
            function ($answer) use ($screenId) {
                return $answer->getScreenId() === $screenId;
            }));

        return $answers;
    }

    /**
     * @param string $projectId
     * @return Project[]
     */
    public function getRelatedProjects(string $projectId)
    {
        $result = $this->client->get(self::BASE_ROUTE . '/related-projects', [':project_id' => $projectId]);

        return array_key_exists('related_project_ids', $result) ?
            array_map(
                function ($id) {
                    return $this->getProject($id);
                },
                $result['related_project_ids']
            ) :
            [];
    }


    /**
     * @param string $projectId
     * @return ProjectHistory[]
     */
    public function getProjectHistory(string $projectId)
    {
        return array_map(
            function ($history) {
                return new ProjectHistory(
                    $history['project_id'],
                    $history['event_date'],
                    $history['event_title'],
                    $history['event_title_tag'],
                    $history['event_description']
                );
            },
            $this->client->get(
                self::BASE_ROUTE . '/history',
                [':project_id' => $projectId],
                []
            )
        );
    }

    /**
     * @param string $projectId
     * @param string $projectStatusId
     * @return array
     */
    public function setProjectStatus(string $projectId, string $projectStatusId){
        return $this->client->post(
            self::BASE_ROUTE . '/', 
            ['status_id' => $projectStatusId],
            [':project_id' => $projectId]
        );
    }
}