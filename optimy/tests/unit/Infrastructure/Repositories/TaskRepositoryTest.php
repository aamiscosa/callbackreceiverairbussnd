<?php

namespace Optimy\Infrastructure\Repositories;


use Optimy\Domain\Task;
use Optimy\CallbackReceiverLambda\Domain\TaskTags;
use Optimy\Infrastructure\Repositories\Client;
use Optimy\Infrastructure\Repositories\TaskRepository;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;

class TaskRepositoryTest extends TestCase
{
    private $client;
    private $repository;
    private const BASE_ROUTE = 'projects/:project_id/tasks';


    public function setUp()
    {
        $this->client = $this->createMock(Client::class);
        $this->repository =  new TaskRepository($this->client);
    }

    public function test_getProjectTasks_empty_data_should_return_empty_array()
    {
        $projectId = Uuid::uuid4()->toString();
        $this->client
            ->expects($this->once())
            ->method('get')
            ->with(
                $this->equalTo(self::BASE_ROUTE),
                $this->equalTo([':project_id' => $projectId]),
                $this->equalTo(['include_completed' => false])
            )
            ->willReturn([]);

        self::assertEmpty($this->repository->getProjectTasks($projectId, false));
    }

    public function test_getProjectTasks_should_return_array_of_tasks()
    {
        $projectId = Uuid::uuid4()->toString();
        $tasks = $this->generateTaskData($projectId, 3, [TaskTags::LAB, TaskTags::ATF, TaskTags::VISUELS]);

        $this->client
            ->expects($this->once())
            ->method('get')
            ->with(
                $this->equalTo(self::BASE_ROUTE),
                $this->equalTo([':project_id' => $projectId]),
                $this->equalTo(['include_completed' => true])
            )
            ->willReturn($tasks);

        $result = $this->repository->getProjectTasks($projectId);

        $this->assertCount(3, $result);
        $this->assertInstanceOf(Task::class, array_pop($result));
    }

    public function test_getProjectTaskByTag_should_return_empty_array()
    {
        $projectId = Uuid::uuid4()->toString();
        $tasks = $this->generateTaskData($projectId, 3, [TaskTags::LAB, TaskTags::ATF, TaskTags::VISUELS]);

        $this->client
            ->expects($this->once())
            ->method('get')
            ->with(
                $this->equalTo(self::BASE_ROUTE),
                $this->equalTo([':project_id' => $projectId]),
                $this->equalTo(['include_completed' => true])
            )
            ->willReturn($tasks);

        self::assertEmpty($this->repository->getProjectTasksByTag($projectId, TaskTags::COMITE));
    }

    public function test_getProjectTaskByTag_should_return_array_of_one()
    {
        $projectId = Uuid::uuid4()->toString();
        $tasks = $this->generateTaskData($projectId, 3, [TaskTags::LAB, TaskTags::ATF, TaskTags::VISUELS]);

        $this->client
            ->expects($this->once())
            ->method('get')
            ->with(
                $this->equalTo(self::BASE_ROUTE),
                $this->equalTo([':project_id' => $projectId]),
                $this->equalTo(['include_completed' => true])
            )
            ->willReturn($tasks);

        $result = $this->repository->getProjectTasksByTag($projectId, TaskTags::LAB);

        self::assertInstanceOf(Task::class, $result[0]);
        self::assertCount(1, $result);
    }

    public function test_getProjectTaskByTag_should_return_array_of_two()
    {
        $projectId = Uuid::uuid4()->toString();
        $tasks = $this->generateTaskData($projectId, 4, [TaskTags::LAB, TaskTags::LAB, TaskTags::ATF, TaskTags::VISUELS]);

        $this->client
            ->expects($this->once())
            ->method('get')
            ->with(
                $this->equalTo(self::BASE_ROUTE),
                $this->equalTo([':project_id' => $projectId]),
                $this->equalTo(['include_completed' => true])
            )
            ->willReturn($tasks);

        $result = $this->repository->getProjectTasksByTag($projectId, TaskTags::LAB);

        self::assertInstanceOf(Task::class, $result[0]);
        self::assertCount(2, $result);
    }

    /**
     * @param string $projectId
     * @param int $amount
     * @param array $tagsOfEachTask - Values of TaskTags
     * @return array
     * @throws \Exception
     */
    private function generateTaskData(string $projectId, int $amount, array $tagsOfEachTask): array
    {
        $tasks = [];
        for ($i = 0; $i < $amount; $i++) {
            array_push(
                $tasks,
                [
                    'id' => rand(1, 1000),
                    'name' => 'Task #' . $i,
                    'assigner_id' => rand(1, 1000),
                    'assignee_id' => rand(1, 1000),
                    'due_date' => (new \DateTime('now + 1 month'))->format('Y-m-d'),
                    'projectId' => $projectId,
                    'description' => 'Lorem ipsum #' . $tagsOfEachTask[$i],
                    'task_type_id' => Uuid::uuid4()->toString(),
                    'completion_time' => (new \DateTime('now - 1 week'))->format('Y-m-d')
                ]
            );
        }
        return $tasks;
    }
}
