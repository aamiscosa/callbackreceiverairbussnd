<?php

namespace Optimy\Infrastructure\Repositories;


use Optimy\Domain\Project;
use Optimy\Domain\ProjectAnswer;
use Optimy\Domain\ProjectPartVersion;
use Optimy\Domain\ProjectPartVersionStatuses;
use Optimy\Infrastructure\Repositories\Client;
use Optimy\Infrastructure\Repositories\ProjectRepository;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;

class ProjectRepositoryTest extends TestCase
{
    private $client;
    private $repository;

    public function setUp()
    {
        $this->client = $this->createMock(Client::class);
        $this->repository =  new ProjectRepository($this->client);
    }

    public function test_getProject_should_return_null()
    {
        $projectId = Uuid::uuid4()->toString();
        $this->client
            ->expects($this->once())
            ->method('get')
            ->with(
                $this->equalTo('projects/:project_id'),
                $this->equalTo([':project_id' => $projectId])
            )
            ->willReturn([]);

        self::assertNull($this->repository->getProject($projectId));
    }

    public function test_getProject_should_return_Project()
    {
        $projectId = Uuid::uuid4()->toString();
        $projectData = [
            'id' => Uuid::uuid4()->toString(),
            'tool_reference' => Uuid::uuid4()->toString(),
            'name' => 'Project name',
            'status_id' => Uuid::uuid4()->toString(),
            'form_id' => Uuid::uuid4()->toString(),
            'leader_team_id' => Uuid::uuid4()->toString(),
        ];

        $this->client
            ->expects($this->once())
            ->method('get')
            ->with(
                $this->equalTo('projects/:project_id'),
                $this->equalTo([':project_id' => $projectId])
            )
            ->willReturn($projectData);

        self::assertInstanceOf(Project::class, $this->repository->getProject($projectId));
    }

    public function test_getProjectVersions_should_return_empty_array()
    {
        $projectId = Uuid::uuid4()->toString();
        $this->client
            ->expects($this->once())
            ->method('get')
            ->with(
                $this->equalTo('projects/:project_id/versions'),
                $this->equalTo([':project_id' => $projectId])
            )
            ->willReturn([]);

        $this->assertEmpty($this->repository->getProjectVersions($projectId));
    }

    public function test_getProjectVersions_should_return_array_of_ProjectPartVersion()
    {
        $projectId = Uuid::uuid4()->toString();
        $formPartId = Uuid::uuid4()->toString();
        $versions = [
            [
                'version_id' => Uuid::uuid4()->toString(),
                'form_part_id' => $formPartId,
                'version' => 1,
                'status' => ProjectPartVersionStatuses::PUBLISHED,
                'submitted_time' => (new \DateTime('yesterday'))->format('Y-m-d H:i:s')
            ],
            [
                'version_id' => Uuid::uuid4()->toString(),
                'form_part_id' => $formPartId,
                'version' => 2,
                'status' => ProjectPartVersionStatuses::PUBLISHED,
                'submitted_time' => (new \DateTime('now'))->format('Y-m-d H:i:s')
            ]
        ];
        $this->client
            ->expects($this->once())
            ->method('get')
            ->with(
                $this->equalTo('projects/:project_id/versions'),
                $this->equalTo([':project_id' => $projectId])
            )
            ->willReturn($versions);

        $result = $this->repository->getProjectVersions($projectId);

        self::assertNotEmpty($result);
        self::assertInstanceOf(ProjectPartVersion::class, array_shift($result));
    }

    public function test_getProjectLastPublishedVersionOfFormPart_should_return_null()
    {
        $projectId = Uuid::uuid4()->toString();
        $this->client
            ->expects($this->once())
            ->method('get')
            ->with(
                $this->equalTo('projects/:project_id/versions'),
                $this->equalTo([':project_id' => $projectId])
            )
            ->willReturn([]);

        self::assertNull($this->repository->getProjectLastPublishedVersionOfFormPart($projectId, Uuid::uuid4()->toString()));
    }

    public function test_getProjectLastPublishedVersionOfFormPart_should_return_null_with_result_from_the_request()
    {
        $projectId = Uuid::uuid4()->toString();
        $formPartId = Uuid::uuid4()->toString();
        $versions = [
            [
                'version_id' => Uuid::uuid4()->toString(),
                'form_part_id' => $formPartId,
                'version' => 1,
                'status' => ProjectPartVersionStatuses::DRAFT,
                'submitted_time' => (new \DateTime('yesterday'))->format('Y-m-d H:i:s')
            ],
            [
                'version_id' => Uuid::uuid4()->toString(),
                'form_part_id' => $formPartId,
                'version' => 2,
                'status' => ProjectPartVersionStatuses::DRAFT,
                'submitted_time' => (new \DateTime('now'))->format('Y-m-d H:i:s')
            ]
        ];

        $this->client
            ->expects($this->once())
            ->method('get')
            ->with(
                $this->equalTo('projects/:project_id/versions'),
                $this->equalTo([':project_id' => $projectId])
            )
            ->willReturn($versions);

        self::assertNull($this->repository->getProjectLastPublishedVersionOfFormPart($projectId, Uuid::uuid4()->toString()));
    }

    public function test_getProjectLastPublishedVersionOfFormPart_should_return_last_published_ProjectPartVersion()
    {
        $projectId = Uuid::uuid4()->toString();
        $formPartId = Uuid::uuid4()->toString();
        $firstVersion = Uuid::uuid4()->toString();
        $lastVersion = Uuid::uuid4()->toString();

        $versions = [
            [
                'version_id' => $firstVersion,
                'form_part_id' => $formPartId,
                'version' => 1,
                'status' => ProjectPartVersionStatuses::PUBLISHED,
                'submitted_time' => (new \DateTime('yesterday'))->format('Y-m-d H:i:s')
            ],
            [
                'version_id' => $lastVersion,
                'form_part_id' => $formPartId,
                'version' => 2,
                'status' => ProjectPartVersionStatuses::PUBLISHED,
                'submitted_time' => (new \DateTime('now'))->format('Y-m-d H:i:s')
            ],
            [
                'version_id' => Uuid::uuid4()->toString(),
                'form_part_id' => Uuid::uuid4()->toString(),
                'version' => 3,
                'status' => ProjectPartVersionStatuses::PUBLISHED,
                'submitted_time' => (new \DateTime('now'))->format('Y-m-d H:i:s')
            ]
        ];

        $this->client
            ->expects($this->once())
            ->method('get')
            ->with(
                $this->equalTo('projects/:project_id/versions'),
                $this->equalTo([':project_id' => $projectId])
            )
            ->willReturn($versions);

        $result = $this->repository->getProjectLastPublishedVersionOfFormPart($projectId, $formPartId);

        self::assertInstanceOf(ProjectPartVersion::class, $result);
        self::assertEquals($lastVersion, $result->getId());
        self::assertEquals($formPartId, $result->getFormPartId());
        self::assertEquals(2, $result->getVersion());
    }

    public function test_getProjectVersionById_should_return_null()
    {
        $projectId = Uuid::uuid4()->toString();
        $formPartId = Uuid::uuid4()->toString();
        $versions = [
            [
                'version_id' => Uuid::uuid4()->toString(),
                'form_part_id' => $formPartId,
                'version' => 1,
                'status' => ProjectPartVersionStatuses::PUBLISHED,
                'submitted_time' => (new \DateTime('yesterday'))->format('Y-m-d H:i:s')
            ],
            [
                'version_id' => Uuid::uuid4()->toString(),
                'form_part_id' => $formPartId,
                'version' => 2,
                'status' => ProjectPartVersionStatuses::PUBLISHED,
                'submitted_time' => (new \DateTime('now'))->format('Y-m-d H:i:s')
            ]
        ];
        $this->client
            ->expects($this->once())
            ->method('get')
            ->with(
                $this->equalTo('projects/:project_id/versions'),
                $this->equalTo([':project_id' => $projectId])
            )
            ->willReturn($versions);

        self::assertNull($this->repository->getProjectVersionById($projectId, Uuid::uuid4()->toString()));
    }

    public function test_getProjectVersionById_should_return_ProjectPartVersion()
    {
        $projectId = Uuid::uuid4()->toString();
        $formPartId = Uuid::uuid4()->toString();
        $versionId = Uuid::uuid4()->toString();
        $versions = [
            [
                'version_id' => $versionId,
                'form_part_id' => $formPartId,
                'version' => 1,
                'status' => ProjectPartVersionStatuses::PUBLISHED,
                'submitted_time' => (new \DateTime('yesterday'))->format('Y-m-d H:i:s')
            ],
            [
                'version_id' => Uuid::uuid4()->toString(),
                'form_part_id' => $formPartId,
                'version' => 2,
                'status' => ProjectPartVersionStatuses::PUBLISHED,
                'submitted_time' => (new \DateTime('now'))->format('Y-m-d H:i:s')
            ]
        ];
        $this->client
            ->expects($this->once())
            ->method('get')
            ->with(
                $this->equalTo('projects/:project_id/versions'),
                $this->equalTo([':project_id' => $projectId])
            )
            ->willReturn($versions);

        self::assertInstanceOf(ProjectPartVersion::class, $this->repository->getProjectVersionById($projectId, $versionId));
    }

    public function test_getProjectAnswers_should_return_empty_array_with_questionId_null()
    {
        $projectId = Uuid::uuid4()->toString();
        $versionId = Uuid::uuid4()->toString();

        $this->client
            ->expects($this->once())
            ->method('get')
            ->with(
                $this->equalTo('projects/:project_id/versions/:versionId/answers'),
                $this->equalTo([':project_id' => $projectId, ':versionId' => $versionId]),
                $this->equalTo([])
            )
            ->willReturn([]);

        self::assertEmpty($this->repository->getProjectAnswers($projectId, $versionId));
    }

    public function test_getProjectAnswers_should_return_empty_array_with_questionId_not_null()
    {
        $projectId = Uuid::uuid4()->toString();
        $versionId = Uuid::uuid4()->toString();
        $questionId = Uuid::uuid4()->toString();

        $this->client
            ->expects($this->once())
            ->method('get')
            ->with(
                $this->equalTo('projects/:project_id/versions/:versionId/answers'),
                $this->equalTo([':project_id' => $projectId, ':versionId' => $versionId]),
                $this->equalTo(['question_id' => $questionId])
            )
            ->willReturn([]);

        self::assertEmpty($this->repository->getProjectAnswers($projectId, $versionId, $questionId));
    }

    public function test_getProjectAnswers_should_return_array_of_answers_with_questionId_null()
    {
        $projectId = Uuid::uuid4()->toString();
        $versionId = Uuid::uuid4()->toString();

        $answers = $this->generateAnswers(4);

        $this->client
            ->expects($this->once())
            ->method('get')
            ->with(
                $this->equalTo('projects/:project_id/versions/:versionId/answers'),
                $this->equalTo([':project_id' => $projectId, ':versionId' => $versionId]),
                $this->equalTo([])
            )
            ->willReturn($answers);


        $result = $this->repository->getProjectAnswers($projectId, $versionId);

        self::assertEquals(4, count($result));
        self::assertInstanceOf(ProjectAnswer::class, $result[0]);
    }

    public function test_getProjectAnswers_should_return_array_of_answers_with_questionId_not_null()
    {
        $projectId = Uuid::uuid4()->toString();
        $versionId = Uuid::uuid4()->toString();
        $questionId = Uuid::uuid4()->toString();

        $answers = $this->generateAnswers(null, $questionId);

        $this->client
            ->expects($this->once())
            ->method('get')
            ->with(
                $this->equalTo('projects/:project_id/versions/:versionId/answers'),
                $this->equalTo([':project_id' => $projectId, ':versionId' => $versionId]),
                $this->equalTo(['question_id' => $questionId])
            )
            ->willReturn($answers);


        $result = $this->repository->getProjectAnswers($projectId, $versionId, $questionId);

        self::assertEquals(1, count($result));
        self::assertInstanceOf(ProjectAnswer::class, $result[0]);
        self::assertEquals($questionId, $result[0]->getQuestionId());
    }

    public function test_getRelatedProjects_should_return_empty_array()
    {
        $projectId = Uuid::uuid4()->toString();
        $this->client
            ->expects($this->once())
            ->method('get')
            ->with(
                $this->equalTo('projects/:project_id/related-projects'),
                $this->equalTo([':project_id' => $projectId])
            )
            ->willReturn([]);

        self::assertEmpty($this->repository->getRelatedProjects($projectId));
    }

    public function test_getRelatedProjects_should_return_array_of_related_projects()
    {
        $projectId = Uuid::uuid4()->toString();
        $repository = $this->getMockBuilder(ProjectRepository::class)->setConstructorArgs([$this->client])->setMethods(['getProject'])->getMock();

        $this->client
            ->expects($this->at(0))
            ->method('get')
            ->with(
                $this->equalTo('projects/:project_id/related-projects'),
                $this->equalTo([':project_id' => $projectId])
            )
            ->willReturn(['related_project_ids' => [Uuid::uuid4()->toString(), Uuid::uuid4()->toString(), Uuid::uuid4()->toString()]]);

        $repository
            ->expects($this->exactly(3))
            ->method('getProject')
            ->willReturn($this->generateProject());

        $result = $repository->getRelatedProjects($projectId);

        self::assertCount(3, $result);
        self::assertInstanceOf(Project::class, array_pop($result));
    }

    private function generateAnswers($amount = null, $questionId = null)
    {
        $questionIds = [];

        if ($questionId !== null) {
            array_push($questionIds, $questionId);
        } else {
            for ($i = 0; $i < $amount; $i++) {
                array_push($questionIds, Uuid::uuid4()->toString());
            }
        }

        return array_map(
            function ($questionId) {
                return [
                    'answer_id' => Uuid::uuid4()->toString(),
                    'answer_name' => 'answer name for ' . $questionId,
                    'value' => 'value for ' . $questionId,
                    'question_id' => $questionId,
                    'screen_id' => Uuid::uuid4()->toString()
                ];
            },
            $questionIds
        );
    }

    private function generateProject()
    {
        return new Project(
            Uuid::uuid4()->toString(),
            '2018-' . rand(1,1000),
            'Project ' . rand(1,1000),
            Uuid::uuid4()->toString(),
            Uuid::uuid4()->toString(),
            "1238",
            ''
        );
    }
}
