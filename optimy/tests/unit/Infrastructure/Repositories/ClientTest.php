<?php

namespace Optimy\Infrastructure\Repositories;


use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Ramsey\Uuid\Uuid;

class ClientTest extends TestCase
{
    private $client;
    private $guzzleClient;
    private $logger;

    public function setUp()
    {
        $this->logger = $this->createMock(LoggerInterface::class);
        $this->guzzleClient = $this->getMockBuilder(\GuzzleHttp\Client::class)->setMethods(['get', 'post'])->getMock();

        $this->client = new Client($this->logger, $this->guzzleClient);
    }

    public function test_get_should_return_empty_array_and_log_error()
    {
        $projectId = Uuid::uuid4()->toString();
        $url = '/projects/' . $projectId;

        $this->logger
            ->expects($this->at(0))
            ->method('info')
            ->with(
                $this->equalTo("Executing GET request to the following url: {$url}")
            );

        $exception = new \Exception('Ouch ! Bad request!');

        $this->guzzleClient
            ->expects($this->once())
            ->method('get')
            ->with(
                $this->equalTo($url)
            )
            ->will($this->throwException($exception));

        $this->logger
            ->expects($this->at(1))
            ->method('error')
            ->with(
                $this->equalTo("Failed: GET request to the following url: {$url}"),
                $this->equalTo(['exception' => $exception->getMessage()])
            );

        self::assertEmpty($this->client->get('/projects/:projectId', [':projectId' => $projectId ]));
    }

    public function test_get_should_return_array_and_log_info()
    {
        $projectId = Uuid::uuid4()->toString();
        $url = '/projects/' . $projectId;

        $this->logger
            ->expects($this->at(0))
            ->method('info')
            ->with(
                $this->equalTo("Executing GET request to the following url: {$url}")
            );

        $projectInfo = [
            'project_id'=> Uuid::uuid4()->toString(),
            'project_name' => 'Project #001'
        ];

        $streamedData = fopen('php://memory', 'r+');
        fputs($streamedData, json_encode(['data'=>[$projectInfo]]));
        rewind($streamedData);
        $response = new Response(200, [], $streamedData);

        $this->guzzleClient
            ->expects($this->once())
            ->method('get')
            ->with(
                $this->equalTo($url)
            )
            ->willReturn($response);

        $this->logger
            ->expects($this->at(1))
            ->method('info')
            ->with(
                $this->equalTo("Service responded with ". json_encode(['data'=>[$projectInfo]]))
            );

        self::assertEquals([$projectInfo], $this->client->get('/projects/:projectId', [':projectId' => $projectId ]));
    }

    public function test_post_should_return_array_success_false_and_error_message()
    {
        $projectId = Uuid::uuid4()->toString();
        $url = '/projects/' . $projectId;

        $postData = [
            'project_name' => 'Project #001',
            'start_date' => '2020-01-01',
            'end_date' => '2020-12-01',
        ];

        $this->logger
            ->expects($this->at(0))
            ->method('info')
            ->with(
                $this->equalTo("Executing POST request to the following url: {$url} with data: " . json_encode($postData))
            );

        $exception = new \Exception('Ouch ! Bad request!');

        $this->guzzleClient
            ->expects($this->once())
            ->method('post')
            ->with(
                $this->equalTo($url),
                $this->equalTo(['form_params' => $postData])
            )
            ->will($this->throwException($exception));

        $this->logger
            ->expects($this->at(1))
            ->method('error')
            ->with(
                $this->equalTo("Failed: post request to the following url: {$url} with data: " . json_encode($postData)),
                $this->equalTo(['exception' => $exception->getMessage()])
            );

        self::assertEquals(
            [
                'success' => false,
                'error' => $exception->getMessage()
            ],
            $this->client->post('/projects/:projectId', $postData, [':projectId' => $projectId ])
        );
    }

    public function test_post_should_return_array_success_true()
    {
        $url = '/projects';

        $postData = [
            'project_name' => 'Project #001',
            'start_date' => '2020-01-01',
            'end_date' => '2020-12-01',
        ];

        $this->logger
            ->expects($this->at(0))
            ->method('info')
            ->with(
                $this->equalTo("Executing POST request to the following url: {$url} with data: " . json_encode($postData))
            );

        $projectInfo = [
            'success' => true,
            'project_id' => Uuid::uuid4()->toString()
        ];

        $streamedData = fopen('php://memory', 'r+');
        fputs($streamedData, json_encode(['data'=> $projectInfo]));
        rewind($streamedData);
        $response = new Response(200, [], $streamedData);

        $this->guzzleClient
            ->expects($this->once())
            ->method('post')
            ->with(
                $this->equalTo($url),
                $this->equalTo(['form_params' => $postData])
            )
            ->willReturn($response);

        $this->logger
            ->expects($this->at(1))
            ->method('info')
            ->with(
                $this->equalTo("Service responded with " . json_encode(['data'=>$projectInfo]))
            );

        self::assertEquals(
            $projectInfo,
            $this->client->post('/projects', $postData)
        );
    }
}
