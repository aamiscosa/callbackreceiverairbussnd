<?php

namespace Optimy\Infrastructure\Repositories;


use Optimy\Domain\FormAnswer;
use Optimy\CallbackReceiverLambda\Domain\FormParts;
use Optimy\CallbackReceiverLambda\Domain\Forms;
use Optimy\CallbackReceiverLambda\Domain\FormScreens;
use PHPUnit\Framework\TestCase;

class FormRepositoryTest extends TestCase
{
    private $client;
    private $repository;
    private const BASE_ROUTE = 'forms/:form_id/parts/:part_id/screens/:screen_id/elements/:element_id';


    public function setUp()
    {
        $this->client = $this->createMock(Client::class);
        $this->repository = new FormRepository($this->client);
    }

    public function test_getAddRemoveAnswersStructure_should_return_an_array_of_arrays_of_FormAnswer()
    {
        $formId = Forms::REFERENTIEL_CONVENTIONS;
        $partId = FormParts::SAISIE_DES_CALENDRIERS;
        $screenId = FormScreens::SAISIE_DES_CALENDRIERS;
        $sectionId = 'cd3c4eb9-0cff-5d79-8397-ee5ccab3253b';

        $structure = $this->getAddRemoveStructure();

        $this->client
            ->expects($this->once())
            ->method('get')
            ->with(
                $this->equalTo(self::BASE_ROUTE),
                $this->equalTo([
                    ':form_id' => $formId,
                    ':part_id' => $partId,
                    ':screen_id' => $screenId,
                    ':element_id' => $sectionId
                ])
            )
            ->willReturn($structure);

        $result = $this->repository->getAddRemoveAnswersStructure($formId, $partId, $screenId, $sectionId);

        self::assertCount(count($structure['elements']), $result);
        self::assertCount(23, $result[0]);
        self::assertInstanceOf(FormAnswer::class, $result[0][0]);
        self::assertEquals('FS1', $result[0][0]->getName());
        self::assertEquals('Date prévisionnelle (copy)', $result[0][22]->getSystemName());
        self::assertEquals('Date prévisionnelle (copy)', $result[1][22]->getSystemName());
        self::assertEquals('Date prévisionnelle (copy)', $result[2][22]->getSystemName());
    }

    private function getAddRemoveStructure()
    {
        return array (
            'id' => 'cd3c4eb9-0cff-5d79-8397-ee5ccab3253b',
            'name' => 'Documents & Rapports',
            'css' => 'template-container table add-remove-container',
            'type' => 'section',
            'translations' =>
                array (
                    'en' =>
                        array (
                            'name' => 'Documents & Rapports',
                            'introduction' => NULL,
                        ),
                    'fr' =>
                        array (
                            'name' => 'Documents & Rapports',
                            'introduction' => NULL,
                        ),
                    'es' =>
                        array (
                            'name' => 'Documents & Rapports',
                            'introduction' => NULL,
                        ),
                ),
            'elements' =>
                array (
                    0 =>
                        array (
                            'id' => '88627c2c-9c8e-5501-9aea-a237701c8a6f',
                            'name' => 'Document/Rapport n°22',
                            'css' => 'add-remove template-section table-row saisie-des-calendriers--rapports--line',
                            'type' => 'section',
                            'translations' =>
                                array (
                                    'en' =>
                                        array (
                                            'name' => 'Document/Rapport n°22',
                                            'introduction' => NULL,
                                        ),
                                    'fr' =>
                                        array (
                                            'name' => 'Document/Rapport n°22',
                                            'introduction' => NULL,
                                        ),
                                    'es' =>
                                        array (
                                            'name' => 'Document/Rapport n°22',
                                            'introduction' => NULL,
                                        ),
                                ),
                            'elements' =>
                                array (
                                    0 =>
                                        array (
                                            'id' => '952a3f2b-91a1-5236-829f-1b618599b331',
                                            'name' => 'Type de document (copy)',
                                            'css' => '',
                                            'type' => 'question',
                                            'question_type' => 'dropdown',
                                            'required' => 1,
                                            'custom_data' =>
                                                array (
                                                    'location_country' => '',
                                                ),
                                            'alpha_sort' => 0,
                                            'min_length' => 1,
                                            'max_length' => NULL,
                                            'validation_type' => NULL,
                                            'validation_mask' => NULL,
                                            'width' => NULL,
                                            'height' => NULL,
                                            'size_limit' => NULL,
                                            'file_types' =>
                                                array (
                                                ),
                                            'translations' =>
                                                array (
                                                    'en' =>
                                                        array (
                                                            'name' => 'Type de document',
                                                            'introduction' => NULL,
                                                        ),
                                                    'fr' =>
                                                        array (
                                                            'name' => 'Type de document',
                                                            'introduction' => NULL,
                                                        ),
                                                    'es' =>
                                                        array (
                                                            'name' => 'Type de document',
                                                            'introduction' => NULL,
                                                        ),
                                                ),
                                            'elements' =>
                                                array (
                                                    0 =>
                                                        array (
                                                            'id' => '0aa9dd9d-7601-580c-a0c3-5fe50d1e7f97',
                                                            'name' => 'FS1 (copy) (copy) (copy) (copy)',
                                                            'css' => '',
                                                            'type' => 'answer',
                                                            'exclusive_answer' => 0,
                                                            'complex_answer_mapping_key' => NULL,
                                                            'reusable_user_data_key' => NULL,
                                                            'custom_data' =>
                                                                array (
                                                                ),
                                                            'special_position' => NULL,
                                                            'translations' =>
                                                                array (
                                                                    'en' =>
                                                                        array (
                                                                            'name' => 'FS1',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'fr' =>
                                                                        array (
                                                                            'name' => 'FS1',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'es' =>
                                                                        array (
                                                                            'name' => 'FS1',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                ),
                                                        ),
                                                    1 =>
                                                        array (
                                                            'id' => 'fa3a3109-a768-5c89-814a-0231c6aa6aff',
                                                            'name' => 'FS2 (copy) (copy) (copy) (copy)',
                                                            'css' => '',
                                                            'type' => 'answer',
                                                            'exclusive_answer' => 0,
                                                            'complex_answer_mapping_key' => NULL,
                                                            'reusable_user_data_key' => NULL,
                                                            'custom_data' =>
                                                                array (
                                                                ),
                                                            'special_position' => NULL,
                                                            'translations' =>
                                                                array (
                                                                    'en' =>
                                                                        array (
                                                                            'name' => 'FS2',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'fr' =>
                                                                        array (
                                                                            'name' => 'FS2',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'es' =>
                                                                        array (
                                                                            'name' => 'FS2',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                ),
                                                        ),
                                                    2 =>
                                                        array (
                                                            'id' => 'ba0df4d1-bd7a-5e1d-ab8e-4daa86832977',
                                                            'name' => 'FS3 (copy) (copy) (copy) (copy)',
                                                            'css' => '',
                                                            'type' => 'answer',
                                                            'exclusive_answer' => 0,
                                                            'complex_answer_mapping_key' => NULL,
                                                            'reusable_user_data_key' => NULL,
                                                            'custom_data' =>
                                                                array (
                                                                ),
                                                            'special_position' => NULL,
                                                            'translations' =>
                                                                array (
                                                                    'en' =>
                                                                        array (
                                                                            'name' => 'FS3',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'fr' =>
                                                                        array (
                                                                            'name' => 'FS3',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'es' =>
                                                                        array (
                                                                            'name' => 'FS3',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                ),
                                                        ),
                                                    3 =>
                                                        array (
                                                            'id' => 'e86d311e-f704-5994-933b-aef1b40087c1',
                                                            'name' => 'FS4 (copy) (copy) (copy) (copy)',
                                                            'css' => '',
                                                            'type' => 'answer',
                                                            'exclusive_answer' => 0,
                                                            'complex_answer_mapping_key' => NULL,
                                                            'reusable_user_data_key' => NULL,
                                                            'custom_data' =>
                                                                array (
                                                                ),
                                                            'special_position' => NULL,
                                                            'translations' =>
                                                                array (
                                                                    'en' =>
                                                                        array (
                                                                            'name' => 'FS4',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'fr' =>
                                                                        array (
                                                                            'name' => 'FS4',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'es' =>
                                                                        array (
                                                                            'name' => 'FS4',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                ),
                                                        ),
                                                    4 =>
                                                        array (
                                                            'id' => 'f008467d-dc44-51b2-88c6-dd5985fd9eff',
                                                            'name' => 'FS5 (copy) (copy) (copy) (copy)',
                                                            'css' => '',
                                                            'type' => 'answer',
                                                            'exclusive_answer' => 0,
                                                            'complex_answer_mapping_key' => NULL,
                                                            'reusable_user_data_key' => NULL,
                                                            'custom_data' =>
                                                                array (
                                                                ),
                                                            'special_position' => NULL,
                                                            'translations' =>
                                                                array (
                                                                    'en' =>
                                                                        array (
                                                                            'name' => 'FS5',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'fr' =>
                                                                        array (
                                                                            'name' => 'FS5',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'es' =>
                                                                        array (
                                                                            'name' => 'FS5',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                ),
                                                        ),
                                                    5 =>
                                                        array (
                                                            'id' => '33a542b3-ce24-58b1-aa6a-792c21ec4d2b',
                                                            'name' => 'FS6 (copy) (copy) (copy) (copy)',
                                                            'css' => '',
                                                            'type' => 'answer',
                                                            'exclusive_answer' => 0,
                                                            'complex_answer_mapping_key' => NULL,
                                                            'reusable_user_data_key' => NULL,
                                                            'custom_data' =>
                                                                array (
                                                                ),
                                                            'special_position' => NULL,
                                                            'translations' =>
                                                                array (
                                                                    'en' =>
                                                                        array (
                                                                            'name' => 'FS6',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'fr' =>
                                                                        array (
                                                                            'name' => 'FS6',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'es' =>
                                                                        array (
                                                                            'name' => 'FS6',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                ),
                                                        ),
                                                    6 =>
                                                        array (
                                                            'id' => '56d1883f-0212-5047-8e27-cb62f6306e61',
                                                            'name' => 'RI1 (copy) (copy) (copy) (copy)',
                                                            'css' => '',
                                                            'type' => 'answer',
                                                            'exclusive_answer' => 0,
                                                            'complex_answer_mapping_key' => NULL,
                                                            'reusable_user_data_key' => NULL,
                                                            'custom_data' =>
                                                                array (
                                                                ),
                                                            'special_position' => NULL,
                                                            'translations' =>
                                                                array (
                                                                    'en' =>
                                                                        array (
                                                                            'name' => 'RI1',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'fr' =>
                                                                        array (
                                                                            'name' => 'RI1',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'es' =>
                                                                        array (
                                                                            'name' => 'RI1',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                ),
                                                        ),
                                                    7 =>
                                                        array (
                                                            'id' => 'b2e57fb9-acfa-59a6-8d54-acd5e1babdbb',
                                                            'name' => 'RI2 (copy) (copy) (copy) (copy)',
                                                            'css' => '',
                                                            'type' => 'answer',
                                                            'exclusive_answer' => 0,
                                                            'complex_answer_mapping_key' => NULL,
                                                            'reusable_user_data_key' => NULL,
                                                            'custom_data' =>
                                                                array (
                                                                ),
                                                            'special_position' => NULL,
                                                            'translations' =>
                                                                array (
                                                                    'en' =>
                                                                        array (
                                                                            'name' => 'RI2',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'fr' =>
                                                                        array (
                                                                            'name' => 'RI2',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'es' =>
                                                                        array (
                                                                            'name' => 'RI2',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                ),
                                                        ),
                                                    8 =>
                                                        array (
                                                            'id' => '57ea0559-9109-5b2e-8daa-c0e26e87a425',
                                                            'name' => 'RI3 (copy) (copy) (copy) (copy)',
                                                            'css' => '',
                                                            'type' => 'answer',
                                                            'exclusive_answer' => 0,
                                                            'complex_answer_mapping_key' => NULL,
                                                            'reusable_user_data_key' => NULL,
                                                            'custom_data' =>
                                                                array (
                                                                ),
                                                            'special_position' => NULL,
                                                            'translations' =>
                                                                array (
                                                                    'en' =>
                                                                        array (
                                                                            'name' => 'RI3',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'fr' =>
                                                                        array (
                                                                            'name' => 'RI3',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'es' =>
                                                                        array (
                                                                            'name' => 'RI3',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                ),
                                                        ),
                                                    9 =>
                                                        array (
                                                            'id' => '659407f8-d033-5403-8f42-512ad7857b8f',
                                                            'name' => 'RI4 (copy) (copy) (copy) (copy)',
                                                            'css' => '',
                                                            'type' => 'answer',
                                                            'exclusive_answer' => 0,
                                                            'complex_answer_mapping_key' => NULL,
                                                            'reusable_user_data_key' => NULL,
                                                            'custom_data' =>
                                                                array (
                                                                ),
                                                            'special_position' => NULL,
                                                            'translations' =>
                                                                array (
                                                                    'en' =>
                                                                        array (
                                                                            'name' => 'RI4',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'fr' =>
                                                                        array (
                                                                            'name' => 'RI4',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'es' =>
                                                                        array (
                                                                            'name' => 'RI4',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                ),
                                                        ),
                                                    10 =>
                                                        array (
                                                            'id' => '8ece5407-7909-5044-b367-95fe345a6fe7',
                                                            'name' => 'RI5 (copy) (copy) (copy) (copy)',
                                                            'css' => '',
                                                            'type' => 'answer',
                                                            'exclusive_answer' => 0,
                                                            'complex_answer_mapping_key' => NULL,
                                                            'reusable_user_data_key' => NULL,
                                                            'custom_data' =>
                                                                array (
                                                                ),
                                                            'special_position' => NULL,
                                                            'translations' =>
                                                                array (
                                                                    'en' =>
                                                                        array (
                                                                            'name' => 'RI5',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'fr' =>
                                                                        array (
                                                                            'name' => 'RI5',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'es' =>
                                                                        array (
                                                                            'name' => 'RI5',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                ),
                                                        ),
                                                    11 =>
                                                        array (
                                                            'id' => '111986d0-c014-5a06-9efa-424a048b491c',
                                                            'name' => 'RI6 (copy) (copy) (copy) (copy)',
                                                            'css' => '',
                                                            'type' => 'answer',
                                                            'exclusive_answer' => 0,
                                                            'complex_answer_mapping_key' => NULL,
                                                            'reusable_user_data_key' => NULL,
                                                            'custom_data' =>
                                                                array (
                                                                ),
                                                            'special_position' => NULL,
                                                            'translations' =>
                                                                array (
                                                                    'en' =>
                                                                        array (
                                                                            'name' => 'RI6',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'fr' =>
                                                                        array (
                                                                            'name' => 'RI6',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'es' =>
                                                                        array (
                                                                            'name' => 'RI6',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                ),
                                                        ),
                                                    12 =>
                                                        array (
                                                            'id' => 'ec95431b-28af-5cb8-a639-ecad4d46bb96',
                                                            'name' => 'RI7 (copy) (copy) (copy) (copy)',
                                                            'css' => '',
                                                            'type' => 'answer',
                                                            'exclusive_answer' => 0,
                                                            'complex_answer_mapping_key' => NULL,
                                                            'reusable_user_data_key' => NULL,
                                                            'custom_data' =>
                                                                array (
                                                                ),
                                                            'special_position' => NULL,
                                                            'translations' =>
                                                                array (
                                                                    'en' =>
                                                                        array (
                                                                            'name' => 'RI7',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'fr' =>
                                                                        array (
                                                                            'name' => 'RI7',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'es' =>
                                                                        array (
                                                                            'name' => 'RI7',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                ),
                                                        ),
                                                    13 =>
                                                        array (
                                                            'id' => '30eab7ce-b55d-538b-8f1b-c9031873dfdc',
                                                            'name' => 'RI8 (copy) (copy) (copy) (copy)',
                                                            'css' => '',
                                                            'type' => 'answer',
                                                            'exclusive_answer' => 0,
                                                            'complex_answer_mapping_key' => NULL,
                                                            'reusable_user_data_key' => NULL,
                                                            'custom_data' =>
                                                                array (
                                                                ),
                                                            'special_position' => NULL,
                                                            'translations' =>
                                                                array (
                                                                    'en' =>
                                                                        array (
                                                                            'name' => 'RI8',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'fr' =>
                                                                        array (
                                                                            'name' => 'RI8',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'es' =>
                                                                        array (
                                                                            'name' => 'RI8',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                ),
                                                        ),
                                                    14 =>
                                                        array (
                                                            'id' => 'a8f94471-9a78-5c89-b66b-41c954d3e78b',
                                                            'name' => 'RI9 (copy) (copy) (copy) (copy)',
                                                            'css' => '',
                                                            'type' => 'answer',
                                                            'exclusive_answer' => 0,
                                                            'complex_answer_mapping_key' => NULL,
                                                            'reusable_user_data_key' => NULL,
                                                            'custom_data' =>
                                                                array (
                                                                ),
                                                            'special_position' => NULL,
                                                            'translations' =>
                                                                array (
                                                                    'en' =>
                                                                        array (
                                                                            'name' => 'RI9',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'fr' =>
                                                                        array (
                                                                            'name' => 'RI9',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'es' =>
                                                                        array (
                                                                            'name' => 'RI9',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                ),
                                                        ),
                                                    15 =>
                                                        array (
                                                            'id' => '004db559-a9c3-5aea-8b6e-78ed71f0e66b',
                                                            'name' => 'P1 (copy) (copy) (copy) (copy)',
                                                            'css' => '',
                                                            'type' => 'answer',
                                                            'exclusive_answer' => 0,
                                                            'complex_answer_mapping_key' => NULL,
                                                            'reusable_user_data_key' => NULL,
                                                            'custom_data' =>
                                                                array (
                                                                ),
                                                            'special_position' => NULL,
                                                            'translations' =>
                                                                array (
                                                                    'en' =>
                                                                        array (
                                                                            'name' => 'P1',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'fr' =>
                                                                        array (
                                                                            'name' => 'P1',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'es' =>
                                                                        array (
                                                                            'name' => 'P1',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                ),
                                                        ),
                                                    16 =>
                                                        array (
                                                            'id' => 'e00a19b8-8f30-5d54-9272-5116d52f92d9',
                                                            'name' => 'P2 (copy) (copy) (copy) (copy)',
                                                            'css' => '',
                                                            'type' => 'answer',
                                                            'exclusive_answer' => 0,
                                                            'complex_answer_mapping_key' => NULL,
                                                            'reusable_user_data_key' => NULL,
                                                            'custom_data' =>
                                                                array (
                                                                ),
                                                            'special_position' => NULL,
                                                            'translations' =>
                                                                array (
                                                                    'en' =>
                                                                        array (
                                                                            'name' => 'P2',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'fr' =>
                                                                        array (
                                                                            'name' => 'P2',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'es' =>
                                                                        array (
                                                                            'name' => 'P2',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                ),
                                                        ),
                                                    17 =>
                                                        array (
                                                            'id' => '578ba7d9-3a50-51aa-bd9b-35e4352635ea',
                                                            'name' => 'P3 (copy) (copy) (copy) (copy)',
                                                            'css' => '',
                                                            'type' => 'answer',
                                                            'exclusive_answer' => 0,
                                                            'complex_answer_mapping_key' => NULL,
                                                            'reusable_user_data_key' => NULL,
                                                            'custom_data' =>
                                                                array (
                                                                ),
                                                            'special_position' => NULL,
                                                            'translations' =>
                                                                array (
                                                                    'en' =>
                                                                        array (
                                                                            'name' => 'P3',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'fr' =>
                                                                        array (
                                                                            'name' => 'P3',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'es' =>
                                                                        array (
                                                                            'name' => 'P3',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                ),
                                                        ),
                                                    18 =>
                                                        array (
                                                            'id' => '2fda650e-a3cf-5a44-b820-890e4653d1ef',
                                                            'name' => 'P4 (copy) (copy) (copy) (copy)',
                                                            'css' => '',
                                                            'type' => 'answer',
                                                            'exclusive_answer' => 0,
                                                            'complex_answer_mapping_key' => NULL,
                                                            'reusable_user_data_key' => NULL,
                                                            'custom_data' =>
                                                                array (
                                                                ),
                                                            'special_position' => NULL,
                                                            'translations' =>
                                                                array (
                                                                    'en' =>
                                                                        array (
                                                                            'name' => 'P4',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'fr' =>
                                                                        array (
                                                                            'name' => 'P4',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'es' =>
                                                                        array (
                                                                            'name' => 'P4',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                ),
                                                        ),
                                                    19 =>
                                                        array (
                                                            'id' => 'd5084cba-4fe7-5153-a97c-67bbc6477e52',
                                                            'name' => 'P5 (copy) (copy) (copy) (copy)',
                                                            'css' => '',
                                                            'type' => 'answer',
                                                            'exclusive_answer' => 0,
                                                            'complex_answer_mapping_key' => NULL,
                                                            'reusable_user_data_key' => NULL,
                                                            'custom_data' =>
                                                                array (
                                                                ),
                                                            'special_position' => NULL,
                                                            'translations' =>
                                                                array (
                                                                    'en' =>
                                                                        array (
                                                                            'name' => 'P5',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'fr' =>
                                                                        array (
                                                                            'name' => 'P5',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'es' =>
                                                                        array (
                                                                            'name' => 'P5',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                ),
                                                        ),
                                                    20 =>
                                                        array (
                                                            'id' => '2c7bcb23-5b2c-5c4e-a35a-cb7714ce261b',
                                                            'name' => 'P6 (copy) (copy) (copy) (copy)',
                                                            'css' => '',
                                                            'type' => 'answer',
                                                            'exclusive_answer' => 0,
                                                            'complex_answer_mapping_key' => NULL,
                                                            'reusable_user_data_key' => NULL,
                                                            'custom_data' =>
                                                                array (
                                                                ),
                                                            'special_position' => NULL,
                                                            'translations' =>
                                                                array (
                                                                    'en' =>
                                                                        array (
                                                                            'name' => 'P6',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'fr' =>
                                                                        array (
                                                                            'name' => 'P6',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'es' =>
                                                                        array (
                                                                            'name' => 'P6',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                ),
                                                        ),
                                                    21 =>
                                                        array (
                                                            'id' => '82cacc2f-1b72-5bea-ac79-bc6a9eea009b',
                                                            'name' => 'RF',
                                                            'css' => '',
                                                            'type' => 'answer',
                                                            'exclusive_answer' => 0,
                                                            'complex_answer_mapping_key' => NULL,
                                                            'reusable_user_data_key' => NULL,
                                                            'custom_data' =>
                                                                array (
                                                                ),
                                                            'special_position' => NULL,
                                                            'translations' =>
                                                                array (
                                                                    'en' =>
                                                                        array (
                                                                            'name' => 'RF',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'fr' =>
                                                                        array (
                                                                            'name' => 'RF',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'es' =>
                                                                        array (
                                                                            'name' => 'RF',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                ),
                                                        ),
                                                ),
                                        ),
                                    1 =>
                                        array (
                                            'id' => 'b7cfc7ce-b234-59ee-a30b-a37180a72696',
                                            'name' => 'Date prévisionnelle de réception (copy)',
                                            'css' => '',
                                            'type' => 'question',
                                            'question_type' => 'calendar',
                                            'required' => 1,
                                            'custom_data' =>
                                                array (
                                                    'location_country' => '',
                                                ),
                                            'alpha_sort' => 0,
                                            'min_length' => 1,
                                            'max_length' => NULL,
                                            'validation_type' => NULL,
                                            'validation_mask' => NULL,
                                            'width' => NULL,
                                            'height' => NULL,
                                            'size_limit' => NULL,
                                            'file_types' =>
                                                array (
                                                ),
                                            'translations' =>
                                                array (
                                                    'en' =>
                                                        array (
                                                            'name' => 'Date prévisionnelle de réception',
                                                            'introduction' => NULL,
                                                        ),
                                                    'fr' =>
                                                        array (
                                                            'name' => 'Date prévisionnelle de réception',
                                                            'introduction' => NULL,
                                                        ),
                                                    'es' =>
                                                        array (
                                                            'name' => 'Date prévisionnelle de réception',
                                                            'introduction' => NULL,
                                                        ),
                                                ),
                                            'elements' =>
                                                array (
                                                    0 =>
                                                        array (
                                                            'id' => '43c33d2e-3301-5a58-b306-c67842282ec9',
                                                            'name' => 'Date prévisionnelle (copy)',
                                                            'css' => '',
                                                            'type' => 'answer',
                                                            'exclusive_answer' => 0,
                                                            'complex_answer_mapping_key' => NULL,
                                                            'reusable_user_data_key' => NULL,
                                                            'custom_data' =>
                                                                array (
                                                                ),
                                                            'special_position' => NULL,
                                                            'translations' =>
                                                                array (
                                                                    'en' =>
                                                                        array (
                                                                            'name' => '',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'fr' =>
                                                                        array (
                                                                            'name' => NULL,
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'es' =>
                                                                        array (
                                                                            'name' => '',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                ),
                                                        ),
                                                ),
                                        ),
                                ),
                        ),
                    1 =>
                        array (
                            'id' => 'b7afe482-2d17-5cad-9a5f-84696f434683',
                            'name' => 'Document/Rapport n°21',
                            'css' => 'add-remove template-section table-row saisie-des-calendriers--rapports--line',
                            'type' => 'section',
                            'translations' =>
                                array (
                                    'en' =>
                                        array (
                                            'name' => 'Document/Rapport n°21',
                                            'introduction' => NULL,
                                        ),
                                    'fr' =>
                                        array (
                                            'name' => 'Document/Rapport n°21',
                                            'introduction' => NULL,
                                        ),
                                    'es' =>
                                        array (
                                            'name' => 'Document/Rapport n°21',
                                            'introduction' => NULL,
                                        ),
                                ),
                            'elements' =>
                                array (
                                    0 =>
                                        array (
                                            'id' => '484d0d9c-9a97-5cef-a88e-2f374988dc77',
                                            'name' => 'Type de document (copy)',
                                            'css' => '',
                                            'type' => 'question',
                                            'question_type' => 'dropdown',
                                            'required' => 1,
                                            'custom_data' =>
                                                array (
                                                    'location_country' => '',
                                                ),
                                            'alpha_sort' => 0,
                                            'min_length' => 1,
                                            'max_length' => NULL,
                                            'validation_type' => NULL,
                                            'validation_mask' => NULL,
                                            'width' => NULL,
                                            'height' => NULL,
                                            'size_limit' => NULL,
                                            'file_types' =>
                                                array (
                                                ),
                                            'translations' =>
                                                array (
                                                    'en' =>
                                                        array (
                                                            'name' => 'Type de document',
                                                            'introduction' => NULL,
                                                        ),
                                                    'fr' =>
                                                        array (
                                                            'name' => 'Type de document',
                                                            'introduction' => NULL,
                                                        ),
                                                    'es' =>
                                                        array (
                                                            'name' => 'Type de document',
                                                            'introduction' => NULL,
                                                        ),
                                                ),
                                            'elements' =>
                                                array (
                                                    0 =>
                                                        array (
                                                            'id' => 'bd71b0ef-d6fd-5eed-9e4f-56fd9545eefd',
                                                            'name' => 'FS1 (copy) (copy) (copy) (copy)',
                                                            'css' => '',
                                                            'type' => 'answer',
                                                            'exclusive_answer' => 0,
                                                            'complex_answer_mapping_key' => NULL,
                                                            'reusable_user_data_key' => NULL,
                                                            'custom_data' =>
                                                                array (
                                                                ),
                                                            'special_position' => NULL,
                                                            'translations' =>
                                                                array (
                                                                    'en' =>
                                                                        array (
                                                                            'name' => 'FS1',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'fr' =>
                                                                        array (
                                                                            'name' => 'FS1',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'es' =>
                                                                        array (
                                                                            'name' => 'FS1',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                ),
                                                        ),
                                                    1 =>
                                                        array (
                                                            'id' => '0766a14e-54a1-5f7b-9e8b-0f58d515baf3',
                                                            'name' => 'FS2 (copy) (copy) (copy) (copy)',
                                                            'css' => '',
                                                            'type' => 'answer',
                                                            'exclusive_answer' => 0,
                                                            'complex_answer_mapping_key' => NULL,
                                                            'reusable_user_data_key' => NULL,
                                                            'custom_data' =>
                                                                array (
                                                                ),
                                                            'special_position' => NULL,
                                                            'translations' =>
                                                                array (
                                                                    'en' =>
                                                                        array (
                                                                            'name' => 'FS2',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'fr' =>
                                                                        array (
                                                                            'name' => 'FS2',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'es' =>
                                                                        array (
                                                                            'name' => 'FS2',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                ),
                                                        ),
                                                    2 =>
                                                        array (
                                                            'id' => '664e277e-a54a-54c4-af3c-37524d878068',
                                                            'name' => 'FS3 (copy) (copy) (copy) (copy)',
                                                            'css' => '',
                                                            'type' => 'answer',
                                                            'exclusive_answer' => 0,
                                                            'complex_answer_mapping_key' => NULL,
                                                            'reusable_user_data_key' => NULL,
                                                            'custom_data' =>
                                                                array (
                                                                ),
                                                            'special_position' => NULL,
                                                            'translations' =>
                                                                array (
                                                                    'en' =>
                                                                        array (
                                                                            'name' => 'FS3',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'fr' =>
                                                                        array (
                                                                            'name' => 'FS3',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'es' =>
                                                                        array (
                                                                            'name' => 'FS3',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                ),
                                                        ),
                                                    3 =>
                                                        array (
                                                            'id' => '8fbf5a8a-6a91-59be-a897-1f9f343f475f',
                                                            'name' => 'FS4 (copy) (copy) (copy) (copy)',
                                                            'css' => '',
                                                            'type' => 'answer',
                                                            'exclusive_answer' => 0,
                                                            'complex_answer_mapping_key' => NULL,
                                                            'reusable_user_data_key' => NULL,
                                                            'custom_data' =>
                                                                array (
                                                                ),
                                                            'special_position' => NULL,
                                                            'translations' =>
                                                                array (
                                                                    'en' =>
                                                                        array (
                                                                            'name' => 'FS4',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'fr' =>
                                                                        array (
                                                                            'name' => 'FS4',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'es' =>
                                                                        array (
                                                                            'name' => 'FS4',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                ),
                                                        ),
                                                    4 =>
                                                        array (
                                                            'id' => '19c09696-65fe-55fd-9394-952b1ec19627',
                                                            'name' => 'FS5 (copy) (copy) (copy) (copy)',
                                                            'css' => '',
                                                            'type' => 'answer',
                                                            'exclusive_answer' => 0,
                                                            'complex_answer_mapping_key' => NULL,
                                                            'reusable_user_data_key' => NULL,
                                                            'custom_data' =>
                                                                array (
                                                                ),
                                                            'special_position' => NULL,
                                                            'translations' =>
                                                                array (
                                                                    'en' =>
                                                                        array (
                                                                            'name' => 'FS5',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'fr' =>
                                                                        array (
                                                                            'name' => 'FS5',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'es' =>
                                                                        array (
                                                                            'name' => 'FS5',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                ),
                                                        ),
                                                    5 =>
                                                        array (
                                                            'id' => 'f9ea7e74-d697-54b3-9052-74643a6636d4',
                                                            'name' => 'FS6 (copy) (copy) (copy) (copy)',
                                                            'css' => '',
                                                            'type' => 'answer',
                                                            'exclusive_answer' => 0,
                                                            'complex_answer_mapping_key' => NULL,
                                                            'reusable_user_data_key' => NULL,
                                                            'custom_data' =>
                                                                array (
                                                                ),
                                                            'special_position' => NULL,
                                                            'translations' =>
                                                                array (
                                                                    'en' =>
                                                                        array (
                                                                            'name' => 'FS6',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'fr' =>
                                                                        array (
                                                                            'name' => 'FS6',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'es' =>
                                                                        array (
                                                                            'name' => 'FS6',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                ),
                                                        ),
                                                    6 =>
                                                        array (
                                                            'id' => 'fbfd0060-3a29-54b7-a702-decea59474c0',
                                                            'name' => 'RI1 (copy) (copy) (copy) (copy)',
                                                            'css' => '',
                                                            'type' => 'answer',
                                                            'exclusive_answer' => 0,
                                                            'complex_answer_mapping_key' => NULL,
                                                            'reusable_user_data_key' => NULL,
                                                            'custom_data' =>
                                                                array (
                                                                ),
                                                            'special_position' => NULL,
                                                            'translations' =>
                                                                array (
                                                                    'en' =>
                                                                        array (
                                                                            'name' => 'RI1',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'fr' =>
                                                                        array (
                                                                            'name' => 'RI1',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'es' =>
                                                                        array (
                                                                            'name' => 'RI1',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                ),
                                                        ),
                                                    7 =>
                                                        array (
                                                            'id' => '6fa6ca8e-19a4-56a3-92eb-80b4225b35f0',
                                                            'name' => 'RI2 (copy) (copy) (copy) (copy)',
                                                            'css' => '',
                                                            'type' => 'answer',
                                                            'exclusive_answer' => 0,
                                                            'complex_answer_mapping_key' => NULL,
                                                            'reusable_user_data_key' => NULL,
                                                            'custom_data' =>
                                                                array (
                                                                ),
                                                            'special_position' => NULL,
                                                            'translations' =>
                                                                array (
                                                                    'en' =>
                                                                        array (
                                                                            'name' => 'RI2',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'fr' =>
                                                                        array (
                                                                            'name' => 'RI2',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'es' =>
                                                                        array (
                                                                            'name' => 'RI2',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                ),
                                                        ),
                                                    8 =>
                                                        array (
                                                            'id' => '5a5d829e-23a5-5d43-a6c7-a30717db5e16',
                                                            'name' => 'RI3 (copy) (copy) (copy) (copy)',
                                                            'css' => '',
                                                            'type' => 'answer',
                                                            'exclusive_answer' => 0,
                                                            'complex_answer_mapping_key' => NULL,
                                                            'reusable_user_data_key' => NULL,
                                                            'custom_data' =>
                                                                array (
                                                                ),
                                                            'special_position' => NULL,
                                                            'translations' =>
                                                                array (
                                                                    'en' =>
                                                                        array (
                                                                            'name' => 'RI3',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'fr' =>
                                                                        array (
                                                                            'name' => 'RI3',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'es' =>
                                                                        array (
                                                                            'name' => 'RI3',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                ),
                                                        ),
                                                    9 =>
                                                        array (
                                                            'id' => '2ea9b835-784e-5525-b490-5cb74af1822a',
                                                            'name' => 'RI4 (copy) (copy) (copy) (copy)',
                                                            'css' => '',
                                                            'type' => 'answer',
                                                            'exclusive_answer' => 0,
                                                            'complex_answer_mapping_key' => NULL,
                                                            'reusable_user_data_key' => NULL,
                                                            'custom_data' =>
                                                                array (
                                                                ),
                                                            'special_position' => NULL,
                                                            'translations' =>
                                                                array (
                                                                    'en' =>
                                                                        array (
                                                                            'name' => 'RI4',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'fr' =>
                                                                        array (
                                                                            'name' => 'RI4',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'es' =>
                                                                        array (
                                                                            'name' => 'RI4',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                ),
                                                        ),
                                                    10 =>
                                                        array (
                                                            'id' => '441fe285-d019-572c-8f37-79d0d90edb50',
                                                            'name' => 'RI5 (copy) (copy) (copy) (copy)',
                                                            'css' => '',
                                                            'type' => 'answer',
                                                            'exclusive_answer' => 0,
                                                            'complex_answer_mapping_key' => NULL,
                                                            'reusable_user_data_key' => NULL,
                                                            'custom_data' =>
                                                                array (
                                                                ),
                                                            'special_position' => NULL,
                                                            'translations' =>
                                                                array (
                                                                    'en' =>
                                                                        array (
                                                                            'name' => 'RI5',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'fr' =>
                                                                        array (
                                                                            'name' => 'RI5',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'es' =>
                                                                        array (
                                                                            'name' => 'RI5',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                ),
                                                        ),
                                                    11 =>
                                                        array (
                                                            'id' => '5824e912-d31f-5d57-b7a5-170e7b64a602',
                                                            'name' => 'RI6 (copy) (copy) (copy) (copy)',
                                                            'css' => '',
                                                            'type' => 'answer',
                                                            'exclusive_answer' => 0,
                                                            'complex_answer_mapping_key' => NULL,
                                                            'reusable_user_data_key' => NULL,
                                                            'custom_data' =>
                                                                array (
                                                                ),
                                                            'special_position' => NULL,
                                                            'translations' =>
                                                                array (
                                                                    'en' =>
                                                                        array (
                                                                            'name' => 'RI6',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'fr' =>
                                                                        array (
                                                                            'name' => 'RI6',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'es' =>
                                                                        array (
                                                                            'name' => 'RI6',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                ),
                                                        ),
                                                    12 =>
                                                        array (
                                                            'id' => '41f3a2ee-6e45-5a86-8b88-40714208c924',
                                                            'name' => 'RI7 (copy) (copy) (copy) (copy)',
                                                            'css' => '',
                                                            'type' => 'answer',
                                                            'exclusive_answer' => 0,
                                                            'complex_answer_mapping_key' => NULL,
                                                            'reusable_user_data_key' => NULL,
                                                            'custom_data' =>
                                                                array (
                                                                ),
                                                            'special_position' => NULL,
                                                            'translations' =>
                                                                array (
                                                                    'en' =>
                                                                        array (
                                                                            'name' => 'RI7',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'fr' =>
                                                                        array (
                                                                            'name' => 'RI7',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'es' =>
                                                                        array (
                                                                            'name' => 'RI7',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                ),
                                                        ),
                                                    13 =>
                                                        array (
                                                            'id' => 'ea376e15-39c2-57f5-9ee7-674fac3a5d2d',
                                                            'name' => 'RI8 (copy) (copy) (copy) (copy)',
                                                            'css' => '',
                                                            'type' => 'answer',
                                                            'exclusive_answer' => 0,
                                                            'complex_answer_mapping_key' => NULL,
                                                            'reusable_user_data_key' => NULL,
                                                            'custom_data' =>
                                                                array (
                                                                ),
                                                            'special_position' => NULL,
                                                            'translations' =>
                                                                array (
                                                                    'en' =>
                                                                        array (
                                                                            'name' => 'RI8',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'fr' =>
                                                                        array (
                                                                            'name' => 'RI8',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'es' =>
                                                                        array (
                                                                            'name' => 'RI8',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                ),
                                                        ),
                                                    14 =>
                                                        array (
                                                            'id' => 'df21ca9d-e9f9-5946-9c75-1846fad9dcb2',
                                                            'name' => 'RI9 (copy) (copy) (copy) (copy)',
                                                            'css' => '',
                                                            'type' => 'answer',
                                                            'exclusive_answer' => 0,
                                                            'complex_answer_mapping_key' => NULL,
                                                            'reusable_user_data_key' => NULL,
                                                            'custom_data' =>
                                                                array (
                                                                ),
                                                            'special_position' => NULL,
                                                            'translations' =>
                                                                array (
                                                                    'en' =>
                                                                        array (
                                                                            'name' => 'RI9',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'fr' =>
                                                                        array (
                                                                            'name' => 'RI9',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'es' =>
                                                                        array (
                                                                            'name' => 'RI9',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                ),
                                                        ),
                                                    15 =>
                                                        array (
                                                            'id' => 'a228922c-1841-5ab5-a3dd-0fbb1a530d1f',
                                                            'name' => 'P1 (copy) (copy) (copy) (copy)',
                                                            'css' => '',
                                                            'type' => 'answer',
                                                            'exclusive_answer' => 0,
                                                            'complex_answer_mapping_key' => NULL,
                                                            'reusable_user_data_key' => NULL,
                                                            'custom_data' =>
                                                                array (
                                                                ),
                                                            'special_position' => NULL,
                                                            'translations' =>
                                                                array (
                                                                    'en' =>
                                                                        array (
                                                                            'name' => 'P1',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'fr' =>
                                                                        array (
                                                                            'name' => 'P1',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'es' =>
                                                                        array (
                                                                            'name' => 'P1',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                ),
                                                        ),
                                                    16 =>
                                                        array (
                                                            'id' => '787696c9-8663-5adf-a8bd-9217961e3088',
                                                            'name' => 'P2 (copy) (copy) (copy) (copy)',
                                                            'css' => '',
                                                            'type' => 'answer',
                                                            'exclusive_answer' => 0,
                                                            'complex_answer_mapping_key' => NULL,
                                                            'reusable_user_data_key' => NULL,
                                                            'custom_data' =>
                                                                array (
                                                                ),
                                                            'special_position' => NULL,
                                                            'translations' =>
                                                                array (
                                                                    'en' =>
                                                                        array (
                                                                            'name' => 'P2',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'fr' =>
                                                                        array (
                                                                            'name' => 'P2',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'es' =>
                                                                        array (
                                                                            'name' => 'P2',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                ),
                                                        ),
                                                    17 =>
                                                        array (
                                                            'id' => '73825432-8aa5-5b09-9103-6f9ab29a90c6',
                                                            'name' => 'P3 (copy) (copy) (copy) (copy)',
                                                            'css' => '',
                                                            'type' => 'answer',
                                                            'exclusive_answer' => 0,
                                                            'complex_answer_mapping_key' => NULL,
                                                            'reusable_user_data_key' => NULL,
                                                            'custom_data' =>
                                                                array (
                                                                ),
                                                            'special_position' => NULL,
                                                            'translations' =>
                                                                array (
                                                                    'en' =>
                                                                        array (
                                                                            'name' => 'P3',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'fr' =>
                                                                        array (
                                                                            'name' => 'P3',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'es' =>
                                                                        array (
                                                                            'name' => 'P3',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                ),
                                                        ),
                                                    18 =>
                                                        array (
                                                            'id' => '635589c0-01ff-5dfc-943d-794413ed5591',
                                                            'name' => 'P4 (copy) (copy) (copy) (copy)',
                                                            'css' => '',
                                                            'type' => 'answer',
                                                            'exclusive_answer' => 0,
                                                            'complex_answer_mapping_key' => NULL,
                                                            'reusable_user_data_key' => NULL,
                                                            'custom_data' =>
                                                                array (
                                                                ),
                                                            'special_position' => NULL,
                                                            'translations' =>
                                                                array (
                                                                    'en' =>
                                                                        array (
                                                                            'name' => 'P4',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'fr' =>
                                                                        array (
                                                                            'name' => 'P4',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'es' =>
                                                                        array (
                                                                            'name' => 'P4',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                ),
                                                        ),
                                                    19 =>
                                                        array (
                                                            'id' => '17a8754c-c7f7-503c-8eb5-f6d466dcd7bc',
                                                            'name' => 'P5 (copy) (copy) (copy) (copy)',
                                                            'css' => '',
                                                            'type' => 'answer',
                                                            'exclusive_answer' => 0,
                                                            'complex_answer_mapping_key' => NULL,
                                                            'reusable_user_data_key' => NULL,
                                                            'custom_data' =>
                                                                array (
                                                                ),
                                                            'special_position' => NULL,
                                                            'translations' =>
                                                                array (
                                                                    'en' =>
                                                                        array (
                                                                            'name' => 'P5',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'fr' =>
                                                                        array (
                                                                            'name' => 'P5',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'es' =>
                                                                        array (
                                                                            'name' => 'P5',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                ),
                                                        ),
                                                    20 =>
                                                        array (
                                                            'id' => '18137035-0d14-5369-a4bd-4e8a6c11c0a2',
                                                            'name' => 'P6 (copy) (copy) (copy) (copy)',
                                                            'css' => '',
                                                            'type' => 'answer',
                                                            'exclusive_answer' => 0,
                                                            'complex_answer_mapping_key' => NULL,
                                                            'reusable_user_data_key' => NULL,
                                                            'custom_data' =>
                                                                array (
                                                                ),
                                                            'special_position' => NULL,
                                                            'translations' =>
                                                                array (
                                                                    'en' =>
                                                                        array (
                                                                            'name' => 'P6',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'fr' =>
                                                                        array (
                                                                            'name' => 'P6',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'es' =>
                                                                        array (
                                                                            'name' => 'P6',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                ),
                                                        ),
                                                    21 =>
                                                        array (
                                                            'id' => '7819caed-e803-5435-aad3-5717166fdb9a',
                                                            'name' => 'RF',
                                                            'css' => '',
                                                            'type' => 'answer',
                                                            'exclusive_answer' => 0,
                                                            'complex_answer_mapping_key' => NULL,
                                                            'reusable_user_data_key' => NULL,
                                                            'custom_data' =>
                                                                array (
                                                                ),
                                                            'special_position' => NULL,
                                                            'translations' =>
                                                                array (
                                                                    'en' =>
                                                                        array (
                                                                            'name' => 'RF',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'fr' =>
                                                                        array (
                                                                            'name' => 'RF',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'es' =>
                                                                        array (
                                                                            'name' => 'RF',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                ),
                                                        ),
                                                ),
                                        ),
                                    1 =>
                                        array (
                                            'id' => '6dd2404c-5ab4-5797-9d1a-c6926f8be616',
                                            'name' => 'Date prévisionnelle de réception (copy)',
                                            'css' => '',
                                            'type' => 'question',
                                            'question_type' => 'calendar',
                                            'required' => 1,
                                            'custom_data' =>
                                                array (
                                                    'location_country' => '',
                                                ),
                                            'alpha_sort' => 0,
                                            'min_length' => 1,
                                            'max_length' => NULL,
                                            'validation_type' => NULL,
                                            'validation_mask' => NULL,
                                            'width' => NULL,
                                            'height' => NULL,
                                            'size_limit' => NULL,
                                            'file_types' =>
                                                array (
                                                ),
                                            'translations' =>
                                                array (
                                                    'en' =>
                                                        array (
                                                            'name' => 'Date prévisionnelle de réception',
                                                            'introduction' => NULL,
                                                        ),
                                                    'fr' =>
                                                        array (
                                                            'name' => 'Date prévisionnelle de réception',
                                                            'introduction' => NULL,
                                                        ),
                                                    'es' =>
                                                        array (
                                                            'name' => 'Date prévisionnelle de réception',
                                                            'introduction' => NULL,
                                                        ),
                                                ),
                                            'elements' =>
                                                array (
                                                    0 =>
                                                        array (
                                                            'id' => 'ba915651-7e68-5d4b-8b9f-3c5fdc036340',
                                                            'name' => 'Date prévisionnelle (copy)',
                                                            'css' => '',
                                                            'type' => 'answer',
                                                            'exclusive_answer' => 0,
                                                            'complex_answer_mapping_key' => NULL,
                                                            'reusable_user_data_key' => NULL,
                                                            'custom_data' =>
                                                                array (
                                                                ),
                                                            'special_position' => NULL,
                                                            'translations' =>
                                                                array (
                                                                    'en' =>
                                                                        array (
                                                                            'name' => '',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'fr' =>
                                                                        array (
                                                                            'name' => NULL,
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'es' =>
                                                                        array (
                                                                            'name' => '',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                ),
                                                        ),
                                                ),
                                        ),
                                ),
                        ),
                    2 =>
                        array (
                            'id' => '6763840b-78f6-5646-b1a7-c06a53fd65b8',
                            'name' => 'Document/Rapport n°20',
                            'css' => 'add-remove template-section table-row saisie-des-calendriers--rapports--line',
                            'type' => 'section',
                            'translations' =>
                                array (
                                    'en' =>
                                        array (
                                            'name' => 'Document/Rapport n°20',
                                            'introduction' => NULL,
                                        ),
                                    'fr' =>
                                        array (
                                            'name' => 'Document/Rapport n°20',
                                            'introduction' => NULL,
                                        ),
                                    'es' =>
                                        array (
                                            'name' => 'Document/Rapport n°20',
                                            'introduction' => NULL,
                                        ),
                                ),
                            'elements' =>
                                array (
                                    0 =>
                                        array (
                                            'id' => '9bf20df8-73c6-5fc4-9350-ed71d8a08f31',
                                            'name' => 'Type de document (copy)',
                                            'css' => '',
                                            'type' => 'question',
                                            'question_type' => 'dropdown',
                                            'required' => 1,
                                            'custom_data' =>
                                                array (
                                                    'location_country' => '',
                                                ),
                                            'alpha_sort' => 0,
                                            'min_length' => 1,
                                            'max_length' => NULL,
                                            'validation_type' => NULL,
                                            'validation_mask' => NULL,
                                            'width' => NULL,
                                            'height' => NULL,
                                            'size_limit' => NULL,
                                            'file_types' =>
                                                array (
                                                ),
                                            'translations' =>
                                                array (
                                                    'en' =>
                                                        array (
                                                            'name' => 'Type de document',
                                                            'introduction' => NULL,
                                                        ),
                                                    'fr' =>
                                                        array (
                                                            'name' => 'Type de document',
                                                            'introduction' => NULL,
                                                        ),
                                                    'es' =>
                                                        array (
                                                            'name' => 'Type de document',
                                                            'introduction' => NULL,
                                                        ),
                                                ),
                                            'elements' =>
                                                array (
                                                    0 =>
                                                        array (
                                                            'id' => '63039664-d824-5dc1-a8be-014bfaee6285',
                                                            'name' => 'FS1 (copy) (copy) (copy) (copy)',
                                                            'css' => '',
                                                            'type' => 'answer',
                                                            'exclusive_answer' => 0,
                                                            'complex_answer_mapping_key' => NULL,
                                                            'reusable_user_data_key' => NULL,
                                                            'custom_data' =>
                                                                array (
                                                                ),
                                                            'special_position' => NULL,
                                                            'translations' =>
                                                                array (
                                                                    'en' =>
                                                                        array (
                                                                            'name' => 'FS1',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'fr' =>
                                                                        array (
                                                                            'name' => 'FS1',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'es' =>
                                                                        array (
                                                                            'name' => 'FS1',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                ),
                                                        ),
                                                    1 =>
                                                        array (
                                                            'id' => '19737a81-8f44-55e5-8d24-b0ecfa35bc26',
                                                            'name' => 'FS2 (copy) (copy) (copy) (copy)',
                                                            'css' => '',
                                                            'type' => 'answer',
                                                            'exclusive_answer' => 0,
                                                            'complex_answer_mapping_key' => NULL,
                                                            'reusable_user_data_key' => NULL,
                                                            'custom_data' =>
                                                                array (
                                                                ),
                                                            'special_position' => NULL,
                                                            'translations' =>
                                                                array (
                                                                    'en' =>
                                                                        array (
                                                                            'name' => 'FS2',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'fr' =>
                                                                        array (
                                                                            'name' => 'FS2',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'es' =>
                                                                        array (
                                                                            'name' => 'FS2',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                ),
                                                        ),
                                                    2 =>
                                                        array (
                                                            'id' => '9dce6b10-eb5b-5861-8ffa-f7ab67339e72',
                                                            'name' => 'FS3 (copy) (copy) (copy) (copy)',
                                                            'css' => '',
                                                            'type' => 'answer',
                                                            'exclusive_answer' => 0,
                                                            'complex_answer_mapping_key' => NULL,
                                                            'reusable_user_data_key' => NULL,
                                                            'custom_data' =>
                                                                array (
                                                                ),
                                                            'special_position' => NULL,
                                                            'translations' =>
                                                                array (
                                                                    'en' =>
                                                                        array (
                                                                            'name' => 'FS3',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'fr' =>
                                                                        array (
                                                                            'name' => 'FS3',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'es' =>
                                                                        array (
                                                                            'name' => 'FS3',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                ),
                                                        ),
                                                    3 =>
                                                        array (
                                                            'id' => '40d05217-f0f3-5357-b097-28624aab76cd',
                                                            'name' => 'FS4 (copy) (copy) (copy) (copy)',
                                                            'css' => '',
                                                            'type' => 'answer',
                                                            'exclusive_answer' => 0,
                                                            'complex_answer_mapping_key' => NULL,
                                                            'reusable_user_data_key' => NULL,
                                                            'custom_data' =>
                                                                array (
                                                                ),
                                                            'special_position' => NULL,
                                                            'translations' =>
                                                                array (
                                                                    'en' =>
                                                                        array (
                                                                            'name' => 'FS4',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'fr' =>
                                                                        array (
                                                                            'name' => 'FS4',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'es' =>
                                                                        array (
                                                                            'name' => 'FS4',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                ),
                                                        ),
                                                    4 =>
                                                        array (
                                                            'id' => '0a416909-7d48-575a-bf89-0260a6487b9d',
                                                            'name' => 'FS5 (copy) (copy) (copy) (copy)',
                                                            'css' => '',
                                                            'type' => 'answer',
                                                            'exclusive_answer' => 0,
                                                            'complex_answer_mapping_key' => NULL,
                                                            'reusable_user_data_key' => NULL,
                                                            'custom_data' =>
                                                                array (
                                                                ),
                                                            'special_position' => NULL,
                                                            'translations' =>
                                                                array (
                                                                    'en' =>
                                                                        array (
                                                                            'name' => 'FS5',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'fr' =>
                                                                        array (
                                                                            'name' => 'FS5',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'es' =>
                                                                        array (
                                                                            'name' => 'FS5',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                ),
                                                        ),
                                                    5 =>
                                                        array (
                                                            'id' => '255cde2f-0896-5a3b-8e09-062f1b36bab2',
                                                            'name' => 'FS6 (copy) (copy) (copy) (copy)',
                                                            'css' => '',
                                                            'type' => 'answer',
                                                            'exclusive_answer' => 0,
                                                            'complex_answer_mapping_key' => NULL,
                                                            'reusable_user_data_key' => NULL,
                                                            'custom_data' =>
                                                                array (
                                                                ),
                                                            'special_position' => NULL,
                                                            'translations' =>
                                                                array (
                                                                    'en' =>
                                                                        array (
                                                                            'name' => 'FS6',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'fr' =>
                                                                        array (
                                                                            'name' => 'FS6',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'es' =>
                                                                        array (
                                                                            'name' => 'FS6',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                ),
                                                        ),
                                                    6 =>
                                                        array (
                                                            'id' => 'f07b2b7d-cbe3-540e-8fda-6b57eccf3e7e',
                                                            'name' => 'RI1 (copy) (copy) (copy) (copy)',
                                                            'css' => '',
                                                            'type' => 'answer',
                                                            'exclusive_answer' => 0,
                                                            'complex_answer_mapping_key' => NULL,
                                                            'reusable_user_data_key' => NULL,
                                                            'custom_data' =>
                                                                array (
                                                                ),
                                                            'special_position' => NULL,
                                                            'translations' =>
                                                                array (
                                                                    'en' =>
                                                                        array (
                                                                            'name' => 'RI1',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'fr' =>
                                                                        array (
                                                                            'name' => 'RI1',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'es' =>
                                                                        array (
                                                                            'name' => 'RI1',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                ),
                                                        ),
                                                    7 =>
                                                        array (
                                                            'id' => '586c2b97-8be7-5929-8279-2f2edb3dd479',
                                                            'name' => 'RI2 (copy) (copy) (copy) (copy)',
                                                            'css' => '',
                                                            'type' => 'answer',
                                                            'exclusive_answer' => 0,
                                                            'complex_answer_mapping_key' => NULL,
                                                            'reusable_user_data_key' => NULL,
                                                            'custom_data' =>
                                                                array (
                                                                ),
                                                            'special_position' => NULL,
                                                            'translations' =>
                                                                array (
                                                                    'en' =>
                                                                        array (
                                                                            'name' => 'RI2',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'fr' =>
                                                                        array (
                                                                            'name' => 'RI2',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'es' =>
                                                                        array (
                                                                            'name' => 'RI2',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                ),
                                                        ),
                                                    8 =>
                                                        array (
                                                            'id' => '2c5241d9-6d19-589e-8cd9-0be27afecb6a',
                                                            'name' => 'RI3 (copy) (copy) (copy) (copy)',
                                                            'css' => '',
                                                            'type' => 'answer',
                                                            'exclusive_answer' => 0,
                                                            'complex_answer_mapping_key' => NULL,
                                                            'reusable_user_data_key' => NULL,
                                                            'custom_data' =>
                                                                array (
                                                                ),
                                                            'special_position' => NULL,
                                                            'translations' =>
                                                                array (
                                                                    'en' =>
                                                                        array (
                                                                            'name' => 'RI3',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'fr' =>
                                                                        array (
                                                                            'name' => 'RI3',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'es' =>
                                                                        array (
                                                                            'name' => 'RI3',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                ),
                                                        ),
                                                    9 =>
                                                        array (
                                                            'id' => '4300e550-651f-53dc-86ca-ab168348179b',
                                                            'name' => 'RI4 (copy) (copy) (copy) (copy)',
                                                            'css' => '',
                                                            'type' => 'answer',
                                                            'exclusive_answer' => 0,
                                                            'complex_answer_mapping_key' => NULL,
                                                            'reusable_user_data_key' => NULL,
                                                            'custom_data' =>
                                                                array (
                                                                ),
                                                            'special_position' => NULL,
                                                            'translations' =>
                                                                array (
                                                                    'en' =>
                                                                        array (
                                                                            'name' => 'RI4',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'fr' =>
                                                                        array (
                                                                            'name' => 'RI4',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'es' =>
                                                                        array (
                                                                            'name' => 'RI4',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                ),
                                                        ),
                                                    10 =>
                                                        array (
                                                            'id' => '650451fd-2c1e-5477-af1a-6dafafa522ef',
                                                            'name' => 'RI5 (copy) (copy) (copy) (copy)',
                                                            'css' => '',
                                                            'type' => 'answer',
                                                            'exclusive_answer' => 0,
                                                            'complex_answer_mapping_key' => NULL,
                                                            'reusable_user_data_key' => NULL,
                                                            'custom_data' =>
                                                                array (
                                                                ),
                                                            'special_position' => NULL,
                                                            'translations' =>
                                                                array (
                                                                    'en' =>
                                                                        array (
                                                                            'name' => 'RI5',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'fr' =>
                                                                        array (
                                                                            'name' => 'RI5',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'es' =>
                                                                        array (
                                                                            'name' => 'RI5',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                ),
                                                        ),
                                                    11 =>
                                                        array (
                                                            'id' => 'f490f768-9cc7-5a3b-bd2b-121ab97f9b40',
                                                            'name' => 'RI6 (copy) (copy) (copy) (copy)',
                                                            'css' => '',
                                                            'type' => 'answer',
                                                            'exclusive_answer' => 0,
                                                            'complex_answer_mapping_key' => NULL,
                                                            'reusable_user_data_key' => NULL,
                                                            'custom_data' =>
                                                                array (
                                                                ),
                                                            'special_position' => NULL,
                                                            'translations' =>
                                                                array (
                                                                    'en' =>
                                                                        array (
                                                                            'name' => 'RI6',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'fr' =>
                                                                        array (
                                                                            'name' => 'RI6',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'es' =>
                                                                        array (
                                                                            'name' => 'RI6',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                ),
                                                        ),
                                                    12 =>
                                                        array (
                                                            'id' => '9aa69bde-8e12-5c66-a0f9-aab13cc6c048',
                                                            'name' => 'RI7 (copy) (copy) (copy) (copy)',
                                                            'css' => '',
                                                            'type' => 'answer',
                                                            'exclusive_answer' => 0,
                                                            'complex_answer_mapping_key' => NULL,
                                                            'reusable_user_data_key' => NULL,
                                                            'custom_data' =>
                                                                array (
                                                                ),
                                                            'special_position' => NULL,
                                                            'translations' =>
                                                                array (
                                                                    'en' =>
                                                                        array (
                                                                            'name' => 'RI7',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'fr' =>
                                                                        array (
                                                                            'name' => 'RI7',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'es' =>
                                                                        array (
                                                                            'name' => 'RI7',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                ),
                                                        ),
                                                    13 =>
                                                        array (
                                                            'id' => '31101f62-a400-5959-a598-4d354213f1ca',
                                                            'name' => 'RI8 (copy) (copy) (copy) (copy)',
                                                            'css' => '',
                                                            'type' => 'answer',
                                                            'exclusive_answer' => 0,
                                                            'complex_answer_mapping_key' => NULL,
                                                            'reusable_user_data_key' => NULL,
                                                            'custom_data' =>
                                                                array (
                                                                ),
                                                            'special_position' => NULL,
                                                            'translations' =>
                                                                array (
                                                                    'en' =>
                                                                        array (
                                                                            'name' => 'RI8',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'fr' =>
                                                                        array (
                                                                            'name' => 'RI8',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'es' =>
                                                                        array (
                                                                            'name' => 'RI8',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                ),
                                                        ),
                                                    14 =>
                                                        array (
                                                            'id' => 'c06d7df1-45a8-53f0-ade4-c9beee05bf66',
                                                            'name' => 'RI9 (copy) (copy) (copy) (copy)',
                                                            'css' => '',
                                                            'type' => 'answer',
                                                            'exclusive_answer' => 0,
                                                            'complex_answer_mapping_key' => NULL,
                                                            'reusable_user_data_key' => NULL,
                                                            'custom_data' =>
                                                                array (
                                                                ),
                                                            'special_position' => NULL,
                                                            'translations' =>
                                                                array (
                                                                    'en' =>
                                                                        array (
                                                                            'name' => 'RI9',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'fr' =>
                                                                        array (
                                                                            'name' => 'RI9',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'es' =>
                                                                        array (
                                                                            'name' => 'RI9',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                ),
                                                        ),
                                                    15 =>
                                                        array (
                                                            'id' => '90822ed4-80ef-5b27-95a2-430adaa68bc6',
                                                            'name' => 'P1 (copy) (copy) (copy) (copy)',
                                                            'css' => '',
                                                            'type' => 'answer',
                                                            'exclusive_answer' => 0,
                                                            'complex_answer_mapping_key' => NULL,
                                                            'reusable_user_data_key' => NULL,
                                                            'custom_data' =>
                                                                array (
                                                                ),
                                                            'special_position' => NULL,
                                                            'translations' =>
                                                                array (
                                                                    'en' =>
                                                                        array (
                                                                            'name' => 'P1',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'fr' =>
                                                                        array (
                                                                            'name' => 'P1',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'es' =>
                                                                        array (
                                                                            'name' => 'P1',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                ),
                                                        ),
                                                    16 =>
                                                        array (
                                                            'id' => '1c25a152-0e59-5b21-883e-d54292052286',
                                                            'name' => 'P2 (copy) (copy) (copy) (copy)',
                                                            'css' => '',
                                                            'type' => 'answer',
                                                            'exclusive_answer' => 0,
                                                            'complex_answer_mapping_key' => NULL,
                                                            'reusable_user_data_key' => NULL,
                                                            'custom_data' =>
                                                                array (
                                                                ),
                                                            'special_position' => NULL,
                                                            'translations' =>
                                                                array (
                                                                    'en' =>
                                                                        array (
                                                                            'name' => 'P2',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'fr' =>
                                                                        array (
                                                                            'name' => 'P2',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'es' =>
                                                                        array (
                                                                            'name' => 'P2',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                ),
                                                        ),
                                                    17 =>
                                                        array (
                                                            'id' => '8cc909f3-f70f-54a3-8c00-dbcaaa1ea3d9',
                                                            'name' => 'P3 (copy) (copy) (copy) (copy)',
                                                            'css' => '',
                                                            'type' => 'answer',
                                                            'exclusive_answer' => 0,
                                                            'complex_answer_mapping_key' => NULL,
                                                            'reusable_user_data_key' => NULL,
                                                            'custom_data' =>
                                                                array (
                                                                ),
                                                            'special_position' => NULL,
                                                            'translations' =>
                                                                array (
                                                                    'en' =>
                                                                        array (
                                                                            'name' => 'P3',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'fr' =>
                                                                        array (
                                                                            'name' => 'P3',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'es' =>
                                                                        array (
                                                                            'name' => 'P3',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                ),
                                                        ),
                                                    18 =>
                                                        array (
                                                            'id' => '87118586-7e6e-5251-b500-b11a8f079ffa',
                                                            'name' => 'P4 (copy) (copy) (copy) (copy)',
                                                            'css' => '',
                                                            'type' => 'answer',
                                                            'exclusive_answer' => 0,
                                                            'complex_answer_mapping_key' => NULL,
                                                            'reusable_user_data_key' => NULL,
                                                            'custom_data' =>
                                                                array (
                                                                ),
                                                            'special_position' => NULL,
                                                            'translations' =>
                                                                array (
                                                                    'en' =>
                                                                        array (
                                                                            'name' => 'P4',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'fr' =>
                                                                        array (
                                                                            'name' => 'P4',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'es' =>
                                                                        array (
                                                                            'name' => 'P4',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                ),
                                                        ),
                                                    19 =>
                                                        array (
                                                            'id' => '395a6f9b-87d3-591d-8ee9-55d42ee8afa5',
                                                            'name' => 'P5 (copy) (copy) (copy) (copy)',
                                                            'css' => '',
                                                            'type' => 'answer',
                                                            'exclusive_answer' => 0,
                                                            'complex_answer_mapping_key' => NULL,
                                                            'reusable_user_data_key' => NULL,
                                                            'custom_data' =>
                                                                array (
                                                                ),
                                                            'special_position' => NULL,
                                                            'translations' =>
                                                                array (
                                                                    'en' =>
                                                                        array (
                                                                            'name' => 'P5',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'fr' =>
                                                                        array (
                                                                            'name' => 'P5',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'es' =>
                                                                        array (
                                                                            'name' => 'P5',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                ),
                                                        ),
                                                    20 =>
                                                        array (
                                                            'id' => '8c2397a1-c95e-5b4d-abd5-5d5bdfeb6958',
                                                            'name' => 'P6 (copy) (copy) (copy) (copy)',
                                                            'css' => '',
                                                            'type' => 'answer',
                                                            'exclusive_answer' => 0,
                                                            'complex_answer_mapping_key' => NULL,
                                                            'reusable_user_data_key' => NULL,
                                                            'custom_data' =>
                                                                array (
                                                                ),
                                                            'special_position' => NULL,
                                                            'translations' =>
                                                                array (
                                                                    'en' =>
                                                                        array (
                                                                            'name' => 'P6',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'fr' =>
                                                                        array (
                                                                            'name' => 'P6',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'es' =>
                                                                        array (
                                                                            'name' => 'P6',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                ),
                                                        ),
                                                    21 =>
                                                        array (
                                                            'id' => '4ab9e189-2cc8-5011-beae-b50fe8d775a8',
                                                            'name' => 'RF',
                                                            'css' => '',
                                                            'type' => 'answer',
                                                            'exclusive_answer' => 0,
                                                            'complex_answer_mapping_key' => NULL,
                                                            'reusable_user_data_key' => NULL,
                                                            'custom_data' =>
                                                                array (
                                                                ),
                                                            'special_position' => NULL,
                                                            'translations' =>
                                                                array (
                                                                    'en' =>
                                                                        array (
                                                                            'name' => 'RF',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'fr' =>
                                                                        array (
                                                                            'name' => 'RF',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'es' =>
                                                                        array (
                                                                            'name' => 'RF',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                ),
                                                        ),
                                                ),
                                        ),
                                    1 =>
                                        array (
                                            'id' => '5e159d90-3055-5aae-afc7-4d541b076190',
                                            'name' => 'Date prévisionnelle de réception (copy)',
                                            'css' => '',
                                            'type' => 'question',
                                            'question_type' => 'calendar',
                                            'required' => 1,
                                            'custom_data' =>
                                                array (
                                                    'location_country' => '',
                                                ),
                                            'alpha_sort' => 0,
                                            'min_length' => 1,
                                            'max_length' => NULL,
                                            'validation_type' => NULL,
                                            'validation_mask' => NULL,
                                            'width' => NULL,
                                            'height' => NULL,
                                            'size_limit' => NULL,
                                            'file_types' =>
                                                array (
                                                ),
                                            'translations' =>
                                                array (
                                                    'en' =>
                                                        array (
                                                            'name' => 'Date prévisionnelle de réception',
                                                            'introduction' => NULL,
                                                        ),
                                                    'fr' =>
                                                        array (
                                                            'name' => 'Date prévisionnelle de réception',
                                                            'introduction' => NULL,
                                                        ),
                                                    'es' =>
                                                        array (
                                                            'name' => 'Date prévisionnelle de réception',
                                                            'introduction' => NULL,
                                                        ),
                                                ),
                                            'elements' =>
                                                array (
                                                    0 =>
                                                        array (
                                                            'id' => '4b5e57f5-7abd-591c-a0b6-e3d13aebfcee',
                                                            'name' => 'Date prévisionnelle (copy)',
                                                            'css' => '',
                                                            'type' => 'answer',
                                                            'exclusive_answer' => 0,
                                                            'complex_answer_mapping_key' => NULL,
                                                            'reusable_user_data_key' => NULL,
                                                            'custom_data' =>
                                                                array (
                                                                ),
                                                            'special_position' => NULL,
                                                            'translations' =>
                                                                array (
                                                                    'en' =>
                                                                        array (
                                                                            'name' => '',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'fr' =>
                                                                        array (
                                                                            'name' => NULL,
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                    'es' =>
                                                                        array (
                                                                            'name' => '',
                                                                            'suffix' => NULL,
                                                                            'prefix' => NULL,
                                                                        ),
                                                                ),
                                                        ),
                                                ),
                                        ),
                                ),
                        )
                ),
        );
    }
}
