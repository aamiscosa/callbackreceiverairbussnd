<?php

namespace Optimy\Infrastructure\Repositories;


use Optimy\Domain\EmailTemplate;
use Optimy\CallbackReceiverLambda\Domain\EmailTemplates;
use PHPUnit\Framework\TestCase;

class EmailTemplateRepositoryTest extends TestCase
{
    private $client;
    private $repository;
    private const BASE_ROUTE = 'messaging-templates?language=fr';

    public function setUp()
    {
        $this->client = $this->createMock(Client::class);
        $this->repository =  new EmailTemplateRepository($this->client);
    }

    public function test_getTemplateById_should_return_null_id_not_in_array()
    {
        $this->client
            ->expects($this->once())
            ->method('get')
            ->with(
                $this->equalTo(self::BASE_ROUTE)
            )
            ->willReturn($this->generateTemplateData(2, [EmailTemplates::COMITE_NON_TENU, EmailTemplates::VISUELS_NON_RECUS]));

        self::assertNull($this->repository->getTemplateById(EmailTemplates::LAB, 'fr'));
    }

    public function test_getTemplateById_should_return_null_empty_result()
    {
        $this->client
            ->expects($this->once())
            ->method('get')
            ->with(
                $this->equalTo(self::BASE_ROUTE)
            )
            ->willReturn([]);

        self::assertNull($this->repository->getTemplateById(EmailTemplates::LAB, 'fr'));
    }

    public function test_getTemplateById_should_return_EmailTemplate()
    {
        $this->client
            ->expects($this->once())
            ->method('get')
            ->with(
                $this->equalTo(self::BASE_ROUTE)
            )
            ->willReturn($this->generateTemplateData(2, [EmailTemplates::LAB, EmailTemplates::VISUELS_NON_RECUS]));

        self::assertInstanceOf(EmailTemplate::class, $this->repository->getTemplateById(EmailTemplates::LAB, 'fr'));
    }

    /**
     * @param int $amount
     * @param array $idsOfEachTemplate
     * @return array
     */
    private function generateTemplateData(int $amount, array $idsOfEachTemplate): array
    {
        $templates = [];
        for ($i = 0; $i < $amount; $i++) {
            array_push(
                $templates,
                [
                    'id' => $idsOfEachTemplate[$i],
                    'subject' => 'Email' . $i,
                    'body' => 'Lorem ipsum'
                ]
            );
        }
        return $templates;
    }
}
