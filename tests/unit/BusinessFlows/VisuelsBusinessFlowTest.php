<?php

namespace Optimy\CallbackReceiverLambda\BusinessFlows;


use Optimy\CallbackReceiverLambda\Application\BusinessFlows\VisuelsBusinessFlow;
use Optimy\Domain\EmailTemplate;
use Optimy\Domain\FormAnswer;
use Optimy\Domain\Project;
use Optimy\Domain\ProjectAnswer;
use Optimy\Domain\ProjectPartVersion;
use Optimy\Domain\Task;
use Optimy\Infrastructure\Repositories\EmailTemplateRepository;
use Optimy\Infrastructure\Repositories\FormRepository;
use Optimy\Infrastructure\Repositories\ProjectRepository;
use Optimy\Infrastructure\Repositories\TaskAssigneeRepository;
use Optimy\Infrastructure\Repositories\TaskRepository;
use Optimy\CallbackReceiverLambda\Domain\AssigneeRoles;
use Optimy\CallbackReceiverLambda\Domain\EmailTemplates;
use Optimy\CallbackReceiverLambda\Domain\FormParts;
use Optimy\CallbackReceiverLambda\Domain\Forms;
use Optimy\CallbackReceiverLambda\Domain\FormScreens;
use Optimy\CallbackReceiverLambda\Domain\ProjectStatuses;
use Optimy\CallbackReceiverLambda\Domain\Tasks;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Ramsey\Uuid\Uuid;

class VisuelsBusinessFlowTest extends TestCase
{
    private $logger;
    private $projectRepository;
    private $taskRepository;
    private $emailTemplateRepository;
    private $taskAssigneeRepository;
    private $formRepository;

    private $flow;
    private const ADD_REMOVE = '54e9d077-0ff2-5789-9141-6d6a171c5d2b';

    public function setup()
    {
        $this->logger = $this->createMock(LoggerInterface::class);
        $this->projectRepository = $this->createMock(ProjectRepository::class);
        $this->taskRepository = $this->createMock(TaskRepository::class);
        $this->emailTemplateRepository = $this->createMock(EmailTemplateRepository::class);
        $this->taskAssigneeRepository = $this->createMock(TaskAssigneeRepository::class);
        $this->formRepository = $this->createMock(FormRepository::class);

        $this->flow = new VisuelsBusinessFlow($this->logger, $this->projectRepository, $this->taskRepository, $this->emailTemplateRepository, $this->taskAssigneeRepository, $this->formRepository);
    }

    public function test_execute_first_task_completed_second_to_update_third_to_create_should_return_true()
    {
        $project = new Project(
            Uuid::uuid4()->toString(),
            'BE-001',
            'Danse pour le futur',
            ProjectStatuses::PROJET_SIGNE_DES_PARTIES,
            Forms::REFERENTIEL_CONVENTIONS,
            '1235',''
        );

        $version = new ProjectPartVersion(
            Uuid::uuid4()->toString(),
            FormParts::SAISIE_DES_CALENDRIERS,
            1,
            2,
            (new \DateTime('now - 1 week'))->format('Y-m-d')
        );

        $this->projectRepository
            ->method('getProjectLastPublishedVersionOfFormPart')
            ->with(
                $this->equalTo($project->getId()),
                $this->equalTo(FormParts::SAISIE_DES_CALENDRIERS)
            )
            ->willReturn($version);

        $this->projectRepository
            ->method('getProjectAnswers')
            ->with(
                $this->equalTo($project->getId()),
                $this->equalTo($version->getId())
                )
            ->willReturn($this->getProjectAnswers());

        $this->formRepository
            ->method('getAddRemoveAnswersStructure')
            ->with(
                $this->equalTo(Forms::REFERENTIEL_CONVENTIONS),
                $this->equalTo(FormParts::SAISIE_DES_CALENDRIERS),
                $this->equalTo(FormScreens::SAISIE_DES_CALENDRIERS),
                $this->equalTo(self::ADD_REMOVE)
            )
            ->willReturn($this->getAddRemoveAnswers());

        $task1 = new Task(Tasks::VISUEL, 1234, 1234, '2019-05-02', $project->getId());
        $task1->wasCompletedOn('2017-05-06');
        $this->taskRepository
            ->expects($this->at(0))
            ->method('getProjectTasksByTag')
            ->willReturn([$task1]);

        $this->logger
            ->expects($this->at(0))
            ->method('info')
            ->with(
                $this->equalTo("Task '". Tasks::VISUEL ."' already exists for project id {$project->getId()} and visuel type Vidéo. Continue.")
            );

        $task2 = new Task(Tasks::VISUEL, 1234, 1234, '2019-05-02', $project->getId());
        $this->taskRepository
            ->expects($this->at(1))
            ->method('getProjectTasksByTag')
            ->willReturn([$task2]);

        $this->logger
            ->expects($this->at(1))
            ->method('info')
            ->with(
                $this->equalTo("Updating due date (new value: 2019-08-31) in task '". Tasks::VISUEL ."' for project id {$project->getId()} and visuel type Magazine.")
            );

        $this->taskRepository
            ->expects($this->at(2))
            ->method('getProjectTasksByTag')
            ->willReturn([]);

        $this->taskAssigneeRepository
            ->method('getUserIdFromTeamWithGivenRole')
            ->with(
                $this->equalTo($project->getLeaderTeamId()),
                $this->equalTo(AssigneeRoles::CHEF_DE_PROJET)
            )
            ->willReturn(123);

        $emailTemplate = new EmailTemplate(1, EmailTemplates::VISUELS_NON_RECUS, 'Les documents suivants auraient du nous être remis à la date suivante : {answer.number} {answer.type} {answer.date}.');
        $this->emailTemplateRepository
            ->method('getTemplateById')
            ->with(
                $this->equalTo(EmailTemplates::VISUELS_NON_RECUS)
            )
            ->willReturn($emailTemplate);

        $this->taskRepository
            ->method('saveTask')
            ->willReturn(true);

        self::assertTrue($this->flow->execute($project));
    }

    private function getProjectAnswers(): array
    {
        $projectAnswers = array (
            0 =>
                array (
                    'screen_id' => '286c5fb3-3456-590d-a04b-b5b4e2213718',
                    'question_id' => 'b9c3a5d0-5702-5840-bbed-6a26ecec75b5',
                    'question_name' => 'Date de remise prévisionnelle des visuels (copy)',
                    'answer_id' => '1958c083-dfc1-5f9b-b6eb-a87258fe2af0',
                    'answer_name' => 'Date prévisionnelle de réception (copy)',
                    'value' => '2019-08-23',
                    'complex_data' =>
                        array (
                        ),
                ),
            1 =>
                array (
                    'screen_id' => '286c5fb3-3456-590d-a04b-b5b4e2213718',
                    'question_id' => 'aa35f238-f087-5de3-b28f-afff8f9ef170',
                    'question_name' => 'Type de visuels prévus (copy)',
                    'answer_id' => '239293aa-11c8-5048-9aa1-7676b866ebf4',
                    'answer_name' => 'Autres (copy)',
                    'value' => '1',
                    'complex_data' =>
                        array (
                        ),
                ),
            2 =>
                array (
                    'screen_id' => '286c5fb3-3456-590d-a04b-b5b4e2213718',
                    'question_id' => '72ca64de-c877-5d70-88d2-bef81ac200fd',
                    'question_name' => 'Type de document (copy)',
                    'answer_id' => '2a61406c-19b7-5fe3-a5df-5d6cf535e3eb',
                    'answer_name' => 'FS2 (copy) (copy) (copy) (copy)',
                    'value' => '1',
                    'complex_data' =>
                        array (
                        ),
                ),
            3 =>
                array (
                    'screen_id' => '286c5fb3-3456-590d-a04b-b5b4e2213718',
                    'question_id' => '1934d379-1eee-5573-bd9f-4ebe5adf7b8c',
                    'question_name' => 'N° du lot',
                    'answer_id' => '38894a73-715f-5dca-a1e0-11a8fc850bc1',
                    'answer_name' => 'N° du lot',
                    'value' => '1',
                    'complex_data' =>
                        array (
                        ),
                ),
            4 =>
                array (
                    'screen_id' => '286c5fb3-3456-590d-a04b-b5b4e2213718',
                    'question_id' => '67b1fb1d-bec7-504c-a130-c2cb6d609276',
                    'question_name' => 'Date prévisionnelle de réception',
                    'answer_id' => '443fa2ca-1b2c-52c6-89e7-fa6278303416',
                    'answer_name' => 'Date prévisionnelle',
                    'value' => '2019-04-24',
                    'complex_data' =>
                        array (
                        ),
                ),
            5 =>
                array (
                    'screen_id' => '286c5fb3-3456-590d-a04b-b5b4e2213718',
                    'question_id' => 'fe15d007-9ca4-54c0-ba88-668334840900',
                    'question_name' => 'Date prévisionnelle de réception',
                    'answer_id' => '4a5918e5-a09d-5503-8a5f-720d5d422aca',
                    'answer_name' => 'Date prévisionnelle',
                    'value' => '2019-04-25',
                    'complex_data' =>
                        array (
                        ),
                ),
            6 =>
                array (
                    'screen_id' => '286c5fb3-3456-590d-a04b-b5b4e2213718',
                    'question_id' => '28588f5b-1b09-5169-bb90-f94a0dbbd6e8',
                    'question_name' => 'N° du lot (copy)',
                    'answer_id' => '4af06a34-eeb4-5c1b-90d6-28d5097e9451',
                    'answer_name' => 'N° du lot (copy)',
                    'value' => '2',
                    'complex_data' =>
                        array (
                        ),
                ),
            7 =>
                array (
                    'screen_id' => '286c5fb3-3456-590d-a04b-b5b4e2213718',
                    'question_id' => '17f5ae85-12de-5ec1-948a-b7e97bb2bf26',
                    'question_name' => 'Date prévisionnelle de réception',
                    'answer_id' => '4d61f33c-615d-5267-85c9-84d876260a68',
                    'answer_name' => 'Date prévisionnelle',
                    'value' => '2019-04-30',
                    'complex_data' =>
                        array (
                        ),
                ),
            8 =>
                array (
                    'screen_id' => '286c5fb3-3456-590d-a04b-b5b4e2213718',
                    'question_id' => '1dddf393-f807-5a19-9cd7-bbb0e7858712',
                    'question_name' => 'Type de document (copy)',
                    'answer_id' => '5197961f-5efc-5dee-b5b0-e99fcf22d3b6',
                    'answer_name' => 'RF',
                    'value' => '1',
                    'complex_data' =>
                        array (
                        ),
                ),
            9 =>
                array (
                    'screen_id' => '286c5fb3-3456-590d-a04b-b5b4e2213718',
                    'question_id' => '192f9e6c-b218-57b2-a9b0-3d9cad30eeba',
                    'question_name' => 'Type de document (copy)',
                    'answer_id' => '51b8ee62-0c74-5703-a736-6b06c291d1ae',
                    'answer_name' => 'FS1 (copy) (copy) (copy) (copy)',
                    'value' => '1',
                    'complex_data' =>
                        array (
                        ),
                ),
            10 =>
                array (
                    'screen_id' => '286c5fb3-3456-590d-a04b-b5b4e2213718',
                    'question_id' => '8d702953-a909-57cf-bee2-5070140894d2',
                    'question_name' => 'Type de visuels prévus (copy)',
                    'answer_id' => '5f914543-4d73-5b84-aa8f-5aeec0794791',
                    'answer_name' => 'Vidéo',
                    'value' => '1',
                    'complex_data' =>
                        array (
                        ),
                ),
            11 =>
                array (
                    'screen_id' => '286c5fb3-3456-590d-a04b-b5b4e2213718',
                    'question_id' => '7f58e55c-19a7-5a18-84db-41bf29562026',
                    'question_name' => 'Type de document (copy)',
                    'answer_id' => '6202fadb-f3ae-5528-8f36-f46a40c29e17',
                    'answer_name' => 'P1 (copy) (copy) (copy) (copy)',
                    'value' => '1',
                    'complex_data' =>
                        array (
                        ),
                ),
            12 =>
                array (
                    'screen_id' => '286c5fb3-3456-590d-a04b-b5b4e2213718',
                    'question_id' => 'c1a9cb70-0c36-5252-8eca-0e1f01415a07',
                    'question_name' => 'Type de document (copy)',
                    'answer_id' => '6b086203-099d-5216-83a2-a16b8c598456',
                    'answer_name' => 'RI1 (copy) (copy) (copy) (copy)',
                    'value' => '1',
                    'complex_data' =>
                        array (
                        ),
                ),
            13 =>
                array (
                    'screen_id' => '286c5fb3-3456-590d-a04b-b5b4e2213718',
                    'question_id' => '0d05d08e-970b-50d8-8e5b-f9f44983fddb',
                    'question_name' => 'Date prévisionnelle de réception',
                    'answer_id' => '6e767a51-f08e-51a7-ac64-9de4b3ac99cd',
                    'answer_name' => 'Date prévisionnelle',
                    'value' => '2019-05-24',
                    'complex_data' =>
                        array (
                        ),
                ),
            14 =>
                array (
                    'screen_id' => '286c5fb3-3456-590d-a04b-b5b4e2213718',
                    'question_id' => '16bb8d55-4cfd-548b-b2ee-463698368eb8',
                    'question_name' => 'Date de remise prévisionnelle des visuels',
                    'answer_id' => '78b4a536-d99b-5de8-9bd9-762d2adbfcb7',
                    'answer_name' => 'Date prévisionnelle de réception',
                    'value' => '2019-08-04',
                    'complex_data' =>
                        array (
                        ),
                ),
            15 =>
                array (
                    'screen_id' => '286c5fb3-3456-590d-a04b-b5b4e2213718',
                    'question_id' => '27b9e3e7-a8f3-5fbc-a8bf-c553a3e5d4c7',
                    'question_name' => 'Si "Autres", veuillez préciser :',
                    'answer_id' => '79a087db-8596-5522-9f4d-15a1e1e4a9b3',
                    'answer_name' => 'Veuillez préciser : (copy)',
                    'value' => 'Magazine',
                    'complex_data' =>
                        array (
                        ),
                ),
            16 =>
                array (
                    'screen_id' => '286c5fb3-3456-590d-a04b-b5b4e2213718',
                    'question_id' => '94018e11-b595-52b2-bb4f-fbb5628300b0',
                    'question_name' => 'Nombre de visuels prévus (copy)',
                    'answer_id' => '7abf5187-3071-5d2c-8689-4e2b263f4c87',
                    'answer_name' => 'Nombre de visuels prévus (copy)',
                    'value' => '1',
                    'complex_data' =>
                        array (
                        ),
                ),
            17 =>
                array (
                    'screen_id' => '286c5fb3-3456-590d-a04b-b5b4e2213718',
                    'question_id' => '5ead2c49-5706-556c-bf99-6113117e6fae',
                    'question_name' => 'N° du lot (copy)',
                    'answer_id' => '7d6874b0-ffad-5a9a-b123-86c383eb39dd',
                    'answer_name' => 'N° du lot (copy)',
                    'value' => '3',
                    'complex_data' =>
                        array (
                        ),
                ),
            18 =>
                array (
                    'screen_id' => '286c5fb3-3456-590d-a04b-b5b4e2213718',
                    'question_id' => 'a7e4ae4c-9dba-5f5b-80b9-174aae8f70b7',
                    'question_name' => 'Type de visuels prévus',
                    'answer_id' => '8bce65bb-9dbe-5f77-b98d-2574cd33597f',
                    'answer_name' => 'Photos',
                    'value' => '1',
                    'complex_data' =>
                        array (
                        ),
                ),
            19 =>
                array (
                    'screen_id' => '286c5fb3-3456-590d-a04b-b5b4e2213718',
                    'question_id' => '7217c4eb-080c-5224-8c0d-e5285c00e70f',
                    'question_name' => 'Nombre de visuels prévus (copy)',
                    'answer_id' => '976c73a5-0358-52c6-aefe-7fe9dfcb03f3',
                    'answer_name' => 'Nombre de visuels prévus (copy)',
                    'value' => '4',
                    'complex_data' =>
                        array (
                        ),
                ),
            20 =>
                array (
                    'screen_id' => '286c5fb3-3456-590d-a04b-b5b4e2213718',
                    'question_id' => '65f17d5d-61c2-5bfb-a1a3-c54451b8e7e4',
                    'question_name' => 'Date de remise prévisionnelle des visuels (copy)',
                    'answer_id' => 'a3093cf3-20ff-538f-9fab-0b935c3f9f94',
                    'answer_name' => 'Date prévisionnelle de réception (copy)',
                    'value' => '2019-08-31',
                    'complex_data' =>
                        array (
                        ),
                ),
            21 =>
                array (
                    'screen_id' => '286c5fb3-3456-590d-a04b-b5b4e2213718',
                    'question_id' => '78ae5080-4cb9-59a1-a3ff-2a6eba862e50',
                    'question_name' => 'Nombre de visuels prévus',
                    'answer_id' => 'df74840b-4bec-5891-842f-7dcda11dcd62',
                    'answer_name' => 'Nombre de visuels prévus',
                    'value' => '3',
                    'complex_data' =>
                        array (
                        ),
                ),
            22 =>
                array (
                    'screen_id' => '286c5fb3-3456-590d-a04b-b5b4e2213718',
                    'question_id' => '4ae87d6b-dc44-51e8-8226-85353e0caf3c',
                    'question_name' => 'Date prévisionnelle de réception',
                    'answer_id' => 'e979824d-3bd2-54e8-ae1c-92134eeba385',
                    'answer_name' => 'Date prévisionnelle',
                    'value' => '2019-05-03',
                    'complex_data' =>
                        array (
                        ),
                ),
        );

        return array_map(
            function ($answer) {
                return new ProjectAnswer(
                    $answer['answer_id'],
                    $answer['answer_name'],
                    $answer['value'],
                    $answer['question_id'],
                    $answer['screen_id']
                );
            },
            $projectAnswers);
    }

    private function getAddRemoveAnswers(): array
    {
        $addRemoves = array (
            0 =>
                array (
                    'id' => '46517c54-3de8-5d52-be06-9fa4af7272fb',
                    'name' => 'Visuel n°5',
                    'css' => 'add-remove template-section table-row saisie-des-calendriers--visuels--line',
                    'type' => 'section',
                    'translations' =>
                        array (
                            'en' =>
                                array (
                                    'name' => 'Visuel n°5',
                                    'introduction' => NULL,
                                ),
                            'fr' =>
                                array (
                                    'name' => 'Visuel n°5',
                                    'introduction' => NULL,
                                ),
                            'es' =>
                                array (
                                    'name' => 'Visuel n°5',
                                    'introduction' => NULL,
                                ),
                        ),
                    'elements' =>
                        array (
                            0 =>
                                array (
                                    'id' => '4848e031-80ac-5e12-a66b-93aaabe6f33c',
                                    'name' => 'N° du lot',
                                    'css' => '',
                                    'type' => 'question',
                                    'question_type' => 'text',
                                    'required' => 1,
                                    'custom_data' =>
                                        array (
                                            'location_country' => '',
                                        ),
                                    'alpha_sort' => 0,
                                    'min_length' => 1,
                                    'max_length' => NULL,
                                    'validation_type' => NULL,
                                    'validation_mask' => NULL,
                                    'width' => NULL,
                                    'height' => NULL,
                                    'size_limit' => NULL,
                                    'file_types' =>
                                        array (
                                        ),
                                    'translations' =>
                                        array (
                                            'en' =>
                                                array (
                                                    'name' => 'N° du lot',
                                                    'introduction' => NULL,
                                                ),
                                            'fr' =>
                                                array (
                                                    'name' => 'N° du lot',
                                                    'introduction' => NULL,
                                                ),
                                            'es' =>
                                                array (
                                                    'name' => 'N° du lot',
                                                    'introduction' => NULL,
                                                ),
                                        ),
                                    'elements' =>
                                        array (
                                            0 =>
                                                array (
                                                    'id' => '84a8df5c-e8d8-5e23-a689-3706ee68a739',
                                                    'name' => 'N° du lot',
                                                    'css' => NULL,
                                                    'type' => 'answer',
                                                    'exclusive_answer' => 0,
                                                    'complex_answer_mapping_key' => NULL,
                                                    'reusable_user_data_key' => NULL,
                                                    'custom_data' =>
                                                        array (
                                                        ),
                                                    'special_position' => NULL,
                                                    'translations' =>
                                                        array (
                                                            'en' =>
                                                                array (
                                                                    'name' => '',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'fr' =>
                                                                array (
                                                                    'name' => NULL,
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'es' =>
                                                                array (
                                                                    'name' => '',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                        ),
                                                ),
                                        ),
                                ),
                            1 =>
                                array (
                                    'id' => '843d05e2-912c-545c-b807-47712c02e07f',
                                    'name' => 'Nombre de visuels prévus',
                                    'css' => '',
                                    'type' => 'question',
                                    'question_type' => 'text',
                                    'required' => 1,
                                    'custom_data' =>
                                        array (
                                            'location_country' => '',
                                        ),
                                    'alpha_sort' => 0,
                                    'min_length' => 1,
                                    'max_length' => NULL,
                                    'validation_type' => 'digits',
                                    'validation_mask' => NULL,
                                    'width' => NULL,
                                    'height' => NULL,
                                    'size_limit' => NULL,
                                    'file_types' =>
                                        array (
                                        ),
                                    'translations' =>
                                        array (
                                            'en' =>
                                                array (
                                                    'name' => 'Nombre de visuels prévus',
                                                    'introduction' => NULL,
                                                ),
                                            'fr' =>
                                                array (
                                                    'name' => 'Nombre de visuels prévus',
                                                    'introduction' => NULL,
                                                ),
                                            'es' =>
                                                array (
                                                    'name' => 'Nombre de visuels prévus',
                                                    'introduction' => NULL,
                                                ),
                                        ),
                                    'elements' =>
                                        array (
                                            0 =>
                                                array (
                                                    'id' => '3030485e-bd03-571f-b8b2-e5cf7dd4f25e',
                                                    'name' => 'Nombre de visuels prévus',
                                                    'css' => NULL,
                                                    'type' => 'answer',
                                                    'exclusive_answer' => 0,
                                                    'complex_answer_mapping_key' => NULL,
                                                    'reusable_user_data_key' => NULL,
                                                    'custom_data' =>
                                                        array (
                                                        ),
                                                    'special_position' => NULL,
                                                    'translations' =>
                                                        array (
                                                            'en' =>
                                                                array (
                                                                    'name' => '',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'fr' =>
                                                                array (
                                                                    'name' => NULL,
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'es' =>
                                                                array (
                                                                    'name' => '',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                        ),
                                                ),
                                        ),
                                ),
                            2 =>
                                array (
                                    'id' => '31f778c6-c3ff-5714-92ad-f7a32206a53f',
                                    'name' => 'Type de visuels prévus',
                                    'css' => '',
                                    'type' => 'question',
                                    'question_type' => 'dropdown',
                                    'required' => 1,
                                    'custom_data' =>
                                        array (
                                            'location_country' => '',
                                        ),
                                    'alpha_sort' => 0,
                                    'min_length' => 1,
                                    'max_length' => NULL,
                                    'validation_type' => NULL,
                                    'validation_mask' => NULL,
                                    'width' => NULL,
                                    'height' => NULL,
                                    'size_limit' => NULL,
                                    'file_types' =>
                                        array (
                                        ),
                                    'translations' =>
                                        array (
                                            'en' =>
                                                array (
                                                    'name' => 'Type de visuels prévus',
                                                    'introduction' => NULL,
                                                ),
                                            'fr' =>
                                                array (
                                                    'name' => 'Type de visuels prévus',
                                                    'introduction' => NULL,
                                                ),
                                            'es' =>
                                                array (
                                                    'name' => 'Type de visuels prévus',
                                                    'introduction' => NULL,
                                                ),
                                        ),
                                    'elements' =>
                                        array (
                                            0 =>
                                                array (
                                                    'id' => '2dd17a17-29a6-523a-92c8-560e61228a2c',
                                                    'name' => 'Photos',
                                                    'css' => '',
                                                    'type' => 'answer',
                                                    'exclusive_answer' => 0,
                                                    'complex_answer_mapping_key' => NULL,
                                                    'reusable_user_data_key' => NULL,
                                                    'custom_data' =>
                                                        array (
                                                        ),
                                                    'special_position' => NULL,
                                                    'translations' =>
                                                        array (
                                                            'en' =>
                                                                array (
                                                                    'name' => 'Photos',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'fr' =>
                                                                array (
                                                                    'name' => 'Photos',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'es' =>
                                                                array (
                                                                    'name' => 'Photos',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                        ),
                                                ),
                                            1 =>
                                                array (
                                                    'id' => 'af43c17f-837b-54aa-8ddb-bfa2d3f352cb',
                                                    'name' => 'Affiche',
                                                    'css' => '',
                                                    'type' => 'answer',
                                                    'exclusive_answer' => 0,
                                                    'complex_answer_mapping_key' => NULL,
                                                    'reusable_user_data_key' => NULL,
                                                    'custom_data' =>
                                                        array (
                                                        ),
                                                    'special_position' => NULL,
                                                    'translations' =>
                                                        array (
                                                            'en' =>
                                                                array (
                                                                    'name' => 'Affiche',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'fr' =>
                                                                array (
                                                                    'name' => 'Affiche',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'es' =>
                                                                array (
                                                                    'name' => 'Affiche',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                        ),
                                                ),
                                            2 =>
                                                array (
                                                    'id' => '3551eda3-1819-5d44-a119-b195a3601bbf',
                                                    'name' => 'Vidéo',
                                                    'css' => '',
                                                    'type' => 'answer',
                                                    'exclusive_answer' => 0,
                                                    'complex_answer_mapping_key' => NULL,
                                                    'reusable_user_data_key' => NULL,
                                                    'custom_data' =>
                                                        array (
                                                        ),
                                                    'special_position' => NULL,
                                                    'translations' =>
                                                        array (
                                                            'en' =>
                                                                array (
                                                                    'name' => 'Vidéo',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'fr' =>
                                                                array (
                                                                    'name' => 'Vidéo',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'es' =>
                                                                array (
                                                                    'name' => 'Vidéo',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                        ),
                                                ),
                                            3 =>
                                                array (
                                                    'id' => '8ad5c429-0f04-568b-b730-1998caab760d',
                                                    'name' => 'Autres',
                                                    'css' => '',
                                                    'type' => 'answer',
                                                    'exclusive_answer' => 0,
                                                    'complex_answer_mapping_key' => NULL,
                                                    'reusable_user_data_key' => NULL,
                                                    'custom_data' =>
                                                        array (
                                                        ),
                                                    'special_position' => NULL,
                                                    'translations' =>
                                                        array (
                                                            'en' =>
                                                                array (
                                                                    'name' => 'Autres',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'fr' =>
                                                                array (
                                                                    'name' => 'Autres',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'es' =>
                                                                array (
                                                                    'name' => 'Autres',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                        ),
                                                ),
                                        ),
                                ),
                            3 =>
                                array (
                                    'id' => 'ad298976-7f16-54dd-aebd-b74e13d55841',
                                    'name' => 'Si "Autres", veuillez préciser :',
                                    'css' => '',
                                    'type' => 'question',
                                    'question_type' => 'text',
                                    'required' => 1,
                                    'custom_data' =>
                                        array (
                                            'location_country' => '',
                                        ),
                                    'alpha_sort' => 0,
                                    'min_length' => 1,
                                    'max_length' => NULL,
                                    'validation_type' => NULL,
                                    'validation_mask' => NULL,
                                    'width' => NULL,
                                    'height' => NULL,
                                    'size_limit' => NULL,
                                    'file_types' =>
                                        array (
                                        ),
                                    'translations' =>
                                        array (
                                            'en' =>
                                                array (
                                                    'name' => 'Si "Autres", veuillez préciser :',
                                                    'introduction' => NULL,
                                                ),
                                            'fr' =>
                                                array (
                                                    'name' => 'Si "Autres", veuillez préciser :',
                                                    'introduction' => NULL,
                                                ),
                                            'es' =>
                                                array (
                                                    'name' => 'Si "Autres", veuillez préciser :',
                                                    'introduction' => NULL,
                                                ),
                                        ),
                                    'elements' =>
                                        array (
                                            0 =>
                                                array (
                                                    'id' => 'a0f434aa-cd83-5c85-a152-41544ddf2074',
                                                    'name' => 'Veuillez préciser :',
                                                    'css' => NULL,
                                                    'type' => 'answer',
                                                    'exclusive_answer' => 0,
                                                    'complex_answer_mapping_key' => NULL,
                                                    'reusable_user_data_key' => NULL,
                                                    'custom_data' =>
                                                        array (
                                                        ),
                                                    'special_position' => NULL,
                                                    'translations' =>
                                                        array (
                                                            'en' =>
                                                                array (
                                                                    'name' => '',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'fr' =>
                                                                array (
                                                                    'name' => NULL,
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'es' =>
                                                                array (
                                                                    'name' => '',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                        ),
                                                ),
                                        ),
                                ),
                            4 =>
                                array (
                                    'id' => '04d07bba-77e3-56c7-af04-0771dfac62ea',
                                    'name' => 'Date de remise prévisionnelle des visuels',
                                    'css' => '',
                                    'type' => 'question',
                                    'question_type' => 'calendar',
                                    'required' => 1,
                                    'custom_data' =>
                                        array (
                                            'location_country' => '',
                                        ),
                                    'alpha_sort' => 0,
                                    'min_length' => 1,
                                    'max_length' => NULL,
                                    'validation_type' => NULL,
                                    'validation_mask' => NULL,
                                    'width' => NULL,
                                    'height' => NULL,
                                    'size_limit' => NULL,
                                    'file_types' =>
                                        array (
                                        ),
                                    'translations' =>
                                        array (
                                            'en' =>
                                                array (
                                                    'name' => 'Date de remise prévisionnelle des visuels',
                                                    'introduction' => NULL,
                                                ),
                                            'fr' =>
                                                array (
                                                    'name' => 'Date de remise prévisionnelle des visuels',
                                                    'introduction' => NULL,
                                                ),
                                            'es' =>
                                                array (
                                                    'name' => 'Date de remise prévisionnelle des visuels',
                                                    'introduction' => NULL,
                                                ),
                                        ),
                                    'elements' =>
                                        array (
                                            0 =>
                                                array (
                                                    'id' => '2fb27052-3b1e-5570-b897-5712575f7920',
                                                    'name' => 'Date prévisionnelle de réception',
                                                    'css' => NULL,
                                                    'type' => 'answer',
                                                    'exclusive_answer' => 0,
                                                    'complex_answer_mapping_key' => NULL,
                                                    'reusable_user_data_key' => NULL,
                                                    'custom_data' =>
                                                        array (
                                                        ),
                                                    'special_position' => NULL,
                                                    'translations' =>
                                                        array (
                                                            'en' =>
                                                                array (
                                                                    'name' => '',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'fr' =>
                                                                array (
                                                                    'name' => NULL,
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'es' =>
                                                                array (
                                                                    'name' => '',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                        ),
                                                ),
                                        ),
                                ),
                        ),
                ),
            1 =>
                array (
                    'id' => 'f681a373-9afd-5a78-b342-ae83f516d15a',
                    'name' => 'Visuel n°4',
                    'css' => 'add-remove template-section table-row saisie-des-calendriers--visuels--line',
                    'type' => 'section',
                    'translations' =>
                        array (
                            'en' =>
                                array (
                                    'name' => 'Visuel n°4',
                                    'introduction' => NULL,
                                ),
                            'fr' =>
                                array (
                                    'name' => 'Visuel n°4',
                                    'introduction' => NULL,
                                ),
                            'es' =>
                                array (
                                    'name' => 'Visuel n°4',
                                    'introduction' => NULL,
                                ),
                        ),
                    'elements' =>
                        array (
                            0 =>
                                array (
                                    'id' => 'ba85c33f-86da-5fbe-9337-4ff1a1d53424',
                                    'name' => 'N° du lot (copy)',
                                    'css' => '',
                                    'type' => 'question',
                                    'question_type' => 'text',
                                    'required' => 1,
                                    'custom_data' =>
                                        array (
                                            'location_country' => '',
                                        ),
                                    'alpha_sort' => 0,
                                    'min_length' => 1,
                                    'max_length' => NULL,
                                    'validation_type' => NULL,
                                    'validation_mask' => NULL,
                                    'width' => NULL,
                                    'height' => NULL,
                                    'size_limit' => NULL,
                                    'file_types' =>
                                        array (
                                        ),
                                    'translations' =>
                                        array (
                                            'en' =>
                                                array (
                                                    'name' => 'N° du lot',
                                                    'introduction' => NULL,
                                                ),
                                            'fr' =>
                                                array (
                                                    'name' => 'N° du lot',
                                                    'introduction' => NULL,
                                                ),
                                            'es' =>
                                                array (
                                                    'name' => 'N° du lot',
                                                    'introduction' => NULL,
                                                ),
                                        ),
                                    'elements' =>
                                        array (
                                            0 =>
                                                array (
                                                    'id' => '5c523319-ac68-5daa-929b-2f5c6ff7366f',
                                                    'name' => 'N° du lot (copy)',
                                                    'css' => NULL,
                                                    'type' => 'answer',
                                                    'exclusive_answer' => 0,
                                                    'complex_answer_mapping_key' => NULL,
                                                    'reusable_user_data_key' => NULL,
                                                    'custom_data' =>
                                                        array (
                                                        ),
                                                    'special_position' => NULL,
                                                    'translations' =>
                                                        array (
                                                            'en' =>
                                                                array (
                                                                    'name' => '',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'fr' =>
                                                                array (
                                                                    'name' => NULL,
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'es' =>
                                                                array (
                                                                    'name' => '',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                        ),
                                                ),
                                        ),
                                ),
                            1 =>
                                array (
                                    'id' => '80b967dc-f0e4-5d31-a070-fcfe36c9739d',
                                    'name' => 'Nombre de visuels prévus (copy)',
                                    'css' => '',
                                    'type' => 'question',
                                    'question_type' => 'text',
                                    'required' => 1,
                                    'custom_data' =>
                                        array (
                                            'location_country' => '',
                                        ),
                                    'alpha_sort' => 0,
                                    'min_length' => 1,
                                    'max_length' => NULL,
                                    'validation_type' => 'digits',
                                    'validation_mask' => NULL,
                                    'width' => NULL,
                                    'height' => NULL,
                                    'size_limit' => NULL,
                                    'file_types' =>
                                        array (
                                        ),
                                    'translations' =>
                                        array (
                                            'en' =>
                                                array (
                                                    'name' => 'Nombre de visuels prévus',
                                                    'introduction' => NULL,
                                                ),
                                            'fr' =>
                                                array (
                                                    'name' => 'Nombre de visuels prévus',
                                                    'introduction' => NULL,
                                                ),
                                            'es' =>
                                                array (
                                                    'name' => 'Nombre de visuels prévus',
                                                    'introduction' => NULL,
                                                ),
                                        ),
                                    'elements' =>
                                        array (
                                            0 =>
                                                array (
                                                    'id' => '9953e6a9-aac0-5627-b03c-d47a893d7ba2',
                                                    'name' => 'Nombre de visuels prévus (copy)',
                                                    'css' => NULL,
                                                    'type' => 'answer',
                                                    'exclusive_answer' => 0,
                                                    'complex_answer_mapping_key' => NULL,
                                                    'reusable_user_data_key' => NULL,
                                                    'custom_data' =>
                                                        array (
                                                        ),
                                                    'special_position' => NULL,
                                                    'translations' =>
                                                        array (
                                                            'en' =>
                                                                array (
                                                                    'name' => '',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'fr' =>
                                                                array (
                                                                    'name' => NULL,
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'es' =>
                                                                array (
                                                                    'name' => '',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                        ),
                                                ),
                                        ),
                                ),
                            2 =>
                                array (
                                    'id' => '025dde83-2b85-5817-9b73-f4cfb2faf459',
                                    'name' => 'Type de visuels prévus (copy)',
                                    'css' => '',
                                    'type' => 'question',
                                    'question_type' => 'dropdown',
                                    'required' => 1,
                                    'custom_data' =>
                                        array (
                                            'location_country' => '',
                                        ),
                                    'alpha_sort' => 0,
                                    'min_length' => 1,
                                    'max_length' => NULL,
                                    'validation_type' => NULL,
                                    'validation_mask' => NULL,
                                    'width' => NULL,
                                    'height' => NULL,
                                    'size_limit' => NULL,
                                    'file_types' =>
                                        array (
                                        ),
                                    'translations' =>
                                        array (
                                            'en' =>
                                                array (
                                                    'name' => 'Type de visuels prévus',
                                                    'introduction' => NULL,
                                                ),
                                            'fr' =>
                                                array (
                                                    'name' => 'Type de visuels prévus',
                                                    'introduction' => NULL,
                                                ),
                                            'es' =>
                                                array (
                                                    'name' => 'Type de visuels prévus',
                                                    'introduction' => NULL,
                                                ),
                                        ),
                                    'elements' =>
                                        array (
                                            0 =>
                                                array (
                                                    'id' => 'd0c48b0e-f7d2-5c45-b0ce-a3187e1844b4',
                                                    'name' => 'Photos (copy)',
                                                    'css' => '',
                                                    'type' => 'answer',
                                                    'exclusive_answer' => 0,
                                                    'complex_answer_mapping_key' => NULL,
                                                    'reusable_user_data_key' => NULL,
                                                    'custom_data' =>
                                                        array (
                                                        ),
                                                    'special_position' => NULL,
                                                    'translations' =>
                                                        array (
                                                            'en' =>
                                                                array (
                                                                    'name' => 'Photos',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'fr' =>
                                                                array (
                                                                    'name' => 'Photos',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'es' =>
                                                                array (
                                                                    'name' => 'Photos',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                        ),
                                                ),
                                            1 =>
                                                array (
                                                    'id' => 'f0f8f73d-762f-57a6-95fa-481736946634',
                                                    'name' => 'Affiche',
                                                    'css' => '',
                                                    'type' => 'answer',
                                                    'exclusive_answer' => 0,
                                                    'complex_answer_mapping_key' => NULL,
                                                    'reusable_user_data_key' => NULL,
                                                    'custom_data' =>
                                                        array (
                                                        ),
                                                    'special_position' => NULL,
                                                    'translations' =>
                                                        array (
                                                            'en' =>
                                                                array (
                                                                    'name' => 'Affiche',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'fr' =>
                                                                array (
                                                                    'name' => 'Affiche',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'es' =>
                                                                array (
                                                                    'name' => 'Affiche',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                        ),
                                                ),
                                            2 =>
                                                array (
                                                    'id' => '2a6c48fc-0dff-54cb-9c01-b6afa8849f27',
                                                    'name' => 'Vidéo',
                                                    'css' => '',
                                                    'type' => 'answer',
                                                    'exclusive_answer' => 0,
                                                    'complex_answer_mapping_key' => NULL,
                                                    'reusable_user_data_key' => NULL,
                                                    'custom_data' =>
                                                        array (
                                                        ),
                                                    'special_position' => NULL,
                                                    'translations' =>
                                                        array (
                                                            'en' =>
                                                                array (
                                                                    'name' => 'Vidéo',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'fr' =>
                                                                array (
                                                                    'name' => 'Vidéo',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'es' =>
                                                                array (
                                                                    'name' => 'Vidéo',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                        ),
                                                ),
                                            3 =>
                                                array (
                                                    'id' => '6b600e79-4c56-551f-b9a0-fcfc3e1f7b25',
                                                    'name' => 'Autres (copy)',
                                                    'css' => '',
                                                    'type' => 'answer',
                                                    'exclusive_answer' => 0,
                                                    'complex_answer_mapping_key' => NULL,
                                                    'reusable_user_data_key' => NULL,
                                                    'custom_data' =>
                                                        array (
                                                        ),
                                                    'special_position' => NULL,
                                                    'translations' =>
                                                        array (
                                                            'en' =>
                                                                array (
                                                                    'name' => 'Autres',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'fr' =>
                                                                array (
                                                                    'name' => 'Autres',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'es' =>
                                                                array (
                                                                    'name' => 'Autres',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                        ),
                                                ),
                                        ),
                                ),
                            3 =>
                                array (
                                    'id' => 'a82149fb-6f55-549b-b181-c1db8df2f4ae',
                                    'name' => 'Si "Autres", veuillez préciser :',
                                    'css' => '',
                                    'type' => 'question',
                                    'question_type' => 'text',
                                    'required' => 1,
                                    'custom_data' =>
                                        array (
                                            'location_country' => '',
                                        ),
                                    'alpha_sort' => 0,
                                    'min_length' => 1,
                                    'max_length' => NULL,
                                    'validation_type' => NULL,
                                    'validation_mask' => NULL,
                                    'width' => NULL,
                                    'height' => NULL,
                                    'size_limit' => NULL,
                                    'file_types' =>
                                        array (
                                        ),
                                    'translations' =>
                                        array (
                                            'en' =>
                                                array (
                                                    'name' => 'Si "Autres", veuillez préciser :',
                                                    'introduction' => NULL,
                                                ),
                                            'fr' =>
                                                array (
                                                    'name' => 'Si "Autres", veuillez préciser :',
                                                    'introduction' => NULL,
                                                ),
                                            'es' =>
                                                array (
                                                    'name' => 'Si "Autres", veuillez préciser :',
                                                    'introduction' => NULL,
                                                ),
                                        ),
                                    'elements' =>
                                        array (
                                            0 =>
                                                array (
                                                    'id' => 'e0c6a433-df0c-59d2-bc35-bedc3a41f5b2',
                                                    'name' => 'Veuillez préciser : (copy)',
                                                    'css' => NULL,
                                                    'type' => 'answer',
                                                    'exclusive_answer' => 0,
                                                    'complex_answer_mapping_key' => NULL,
                                                    'reusable_user_data_key' => NULL,
                                                    'custom_data' =>
                                                        array (
                                                        ),
                                                    'special_position' => NULL,
                                                    'translations' =>
                                                        array (
                                                            'en' =>
                                                                array (
                                                                    'name' => '',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'fr' =>
                                                                array (
                                                                    'name' => NULL,
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'es' =>
                                                                array (
                                                                    'name' => '',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                        ),
                                                ),
                                        ),
                                ),
                            4 =>
                                array (
                                    'id' => '5e3f1e46-71ec-5ece-ba23-f2b532576c4d',
                                    'name' => 'Date de remise prévisionnelle des visuels (copy)',
                                    'css' => '',
                                    'type' => 'question',
                                    'question_type' => 'calendar',
                                    'required' => 1,
                                    'custom_data' =>
                                        array (
                                            'location_country' => '',
                                        ),
                                    'alpha_sort' => 0,
                                    'min_length' => 1,
                                    'max_length' => NULL,
                                    'validation_type' => NULL,
                                    'validation_mask' => NULL,
                                    'width' => NULL,
                                    'height' => NULL,
                                    'size_limit' => NULL,
                                    'file_types' =>
                                        array (
                                        ),
                                    'translations' =>
                                        array (
                                            'en' =>
                                                array (
                                                    'name' => 'Date de remise prévisionnelle des visuels',
                                                    'introduction' => NULL,
                                                ),
                                            'fr' =>
                                                array (
                                                    'name' => 'Date de remise prévisionnelle des visuels',
                                                    'introduction' => NULL,
                                                ),
                                            'es' =>
                                                array (
                                                    'name' => 'Date de remise prévisionnelle des visuels',
                                                    'introduction' => NULL,
                                                ),
                                        ),
                                    'elements' =>
                                        array (
                                            0 =>
                                                array (
                                                    'id' => '3ae96209-798f-5c98-af4c-341bd32d0f47',
                                                    'name' => 'Date prévisionnelle de réception (copy)',
                                                    'css' => NULL,
                                                    'type' => 'answer',
                                                    'exclusive_answer' => 0,
                                                    'complex_answer_mapping_key' => NULL,
                                                    'reusable_user_data_key' => NULL,
                                                    'custom_data' =>
                                                        array (
                                                        ),
                                                    'special_position' => NULL,
                                                    'translations' =>
                                                        array (
                                                            'en' =>
                                                                array (
                                                                    'name' => '',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'fr' =>
                                                                array (
                                                                    'name' => NULL,
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'es' =>
                                                                array (
                                                                    'name' => '',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                        ),
                                                ),
                                        ),
                                ),
                        ),
                ),
            2 =>
                array (
                    'id' => 'dc25defd-0f81-5c5f-8546-78c12c92dce2',
                    'name' => 'Visuel n°3',
                    'css' => 'add-remove template-section table-row saisie-des-calendriers--visuels--line',
                    'type' => 'section',
                    'translations' =>
                        array (
                            'en' =>
                                array (
                                    'name' => 'Visuel n°3',
                                    'introduction' => NULL,
                                ),
                            'fr' =>
                                array (
                                    'name' => 'Visuel n°3',
                                    'introduction' => NULL,
                                ),
                            'es' =>
                                array (
                                    'name' => 'Visuel n°3',
                                    'introduction' => NULL,
                                ),
                        ),
                    'elements' =>
                        array (
                            0 =>
                                array (
                                    'id' => 'd75cfcbf-14cd-56f2-bcda-c8338c793c94',
                                    'name' => 'N° du lot (copy)',
                                    'css' => '',
                                    'type' => 'question',
                                    'question_type' => 'text',
                                    'required' => 1,
                                    'custom_data' =>
                                        array (
                                            'location_country' => '',
                                        ),
                                    'alpha_sort' => 0,
                                    'min_length' => 1,
                                    'max_length' => NULL,
                                    'validation_type' => NULL,
                                    'validation_mask' => NULL,
                                    'width' => NULL,
                                    'height' => NULL,
                                    'size_limit' => NULL,
                                    'file_types' =>
                                        array (
                                        ),
                                    'translations' =>
                                        array (
                                            'en' =>
                                                array (
                                                    'name' => 'N° du lot',
                                                    'introduction' => NULL,
                                                ),
                                            'fr' =>
                                                array (
                                                    'name' => 'N° du lot',
                                                    'introduction' => NULL,
                                                ),
                                            'es' =>
                                                array (
                                                    'name' => 'N° du lot',
                                                    'introduction' => NULL,
                                                ),
                                        ),
                                    'elements' =>
                                        array (
                                            0 =>
                                                array (
                                                    'id' => '05500087-a604-5da0-8ff0-adfbf0f77ab9',
                                                    'name' => 'N° du lot (copy)',
                                                    'css' => NULL,
                                                    'type' => 'answer',
                                                    'exclusive_answer' => 0,
                                                    'complex_answer_mapping_key' => NULL,
                                                    'reusable_user_data_key' => NULL,
                                                    'custom_data' =>
                                                        array (
                                                        ),
                                                    'special_position' => NULL,
                                                    'translations' =>
                                                        array (
                                                            'en' =>
                                                                array (
                                                                    'name' => '',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'fr' =>
                                                                array (
                                                                    'name' => NULL,
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'es' =>
                                                                array (
                                                                    'name' => '',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                        ),
                                                ),
                                        ),
                                ),
                            1 =>
                                array (
                                    'id' => '102eed6e-226e-5f39-8a9a-9a557b09227c',
                                    'name' => 'Nombre de visuels prévus (copy)',
                                    'css' => '',
                                    'type' => 'question',
                                    'question_type' => 'text',
                                    'required' => 1,
                                    'custom_data' =>
                                        array (
                                            'location_country' => '',
                                        ),
                                    'alpha_sort' => 0,
                                    'min_length' => 1,
                                    'max_length' => NULL,
                                    'validation_type' => 'digits',
                                    'validation_mask' => NULL,
                                    'width' => NULL,
                                    'height' => NULL,
                                    'size_limit' => NULL,
                                    'file_types' =>
                                        array (
                                        ),
                                    'translations' =>
                                        array (
                                            'en' =>
                                                array (
                                                    'name' => 'Nombre de visuels prévus',
                                                    'introduction' => NULL,
                                                ),
                                            'fr' =>
                                                array (
                                                    'name' => 'Nombre de visuels prévus',
                                                    'introduction' => NULL,
                                                ),
                                            'es' =>
                                                array (
                                                    'name' => 'Nombre de visuels prévus',
                                                    'introduction' => NULL,
                                                ),
                                        ),
                                    'elements' =>
                                        array (
                                            0 =>
                                                array (
                                                    'id' => 'c8a873f0-d292-5743-9f7d-81a9f0135a49',
                                                    'name' => 'Nombre de visuels prévus (copy)',
                                                    'css' => NULL,
                                                    'type' => 'answer',
                                                    'exclusive_answer' => 0,
                                                    'complex_answer_mapping_key' => NULL,
                                                    'reusable_user_data_key' => NULL,
                                                    'custom_data' =>
                                                        array (
                                                        ),
                                                    'special_position' => NULL,
                                                    'translations' =>
                                                        array (
                                                            'en' =>
                                                                array (
                                                                    'name' => '',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'fr' =>
                                                                array (
                                                                    'name' => NULL,
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'es' =>
                                                                array (
                                                                    'name' => '',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                        ),
                                                ),
                                        ),
                                ),
                            2 =>
                                array (
                                    'id' => 'bf473cd8-f66f-5072-9495-0c16dda8c5ea',
                                    'name' => 'Type de visuels prévus (copy)',
                                    'css' => '',
                                    'type' => 'question',
                                    'question_type' => 'dropdown',
                                    'required' => 1,
                                    'custom_data' =>
                                        array (
                                            'location_country' => '',
                                        ),
                                    'alpha_sort' => 0,
                                    'min_length' => 1,
                                    'max_length' => NULL,
                                    'validation_type' => NULL,
                                    'validation_mask' => NULL,
                                    'width' => NULL,
                                    'height' => NULL,
                                    'size_limit' => NULL,
                                    'file_types' =>
                                        array (
                                        ),
                                    'translations' =>
                                        array (
                                            'en' =>
                                                array (
                                                    'name' => 'Type de visuels prévus',
                                                    'introduction' => NULL,
                                                ),
                                            'fr' =>
                                                array (
                                                    'name' => 'Type de visuels prévus',
                                                    'introduction' => NULL,
                                                ),
                                            'es' =>
                                                array (
                                                    'name' => 'Type de visuels prévus',
                                                    'introduction' => NULL,
                                                ),
                                        ),
                                    'elements' =>
                                        array (
                                            0 =>
                                                array (
                                                    'id' => '6e88894f-1043-55c4-85fe-dc13526c071b',
                                                    'name' => 'Photos (copy)',
                                                    'css' => '',
                                                    'type' => 'answer',
                                                    'exclusive_answer' => 0,
                                                    'complex_answer_mapping_key' => NULL,
                                                    'reusable_user_data_key' => NULL,
                                                    'custom_data' =>
                                                        array (
                                                        ),
                                                    'special_position' => NULL,
                                                    'translations' =>
                                                        array (
                                                            'en' =>
                                                                array (
                                                                    'name' => 'Photos',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'fr' =>
                                                                array (
                                                                    'name' => 'Photos',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'es' =>
                                                                array (
                                                                    'name' => 'Photos',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                        ),
                                                ),
                                            1 =>
                                                array (
                                                    'id' => 'fc501b8b-71d2-5dd4-8b3c-02a2484fecf4',
                                                    'name' => 'Affiche',
                                                    'css' => '',
                                                    'type' => 'answer',
                                                    'exclusive_answer' => 0,
                                                    'complex_answer_mapping_key' => NULL,
                                                    'reusable_user_data_key' => NULL,
                                                    'custom_data' =>
                                                        array (
                                                        ),
                                                    'special_position' => NULL,
                                                    'translations' =>
                                                        array (
                                                            'en' =>
                                                                array (
                                                                    'name' => 'Affiche',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'fr' =>
                                                                array (
                                                                    'name' => 'Affiche',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'es' =>
                                                                array (
                                                                    'name' => 'Affiche',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                        ),
                                                ),
                                            2 =>
                                                array (
                                                    'id' => '245a4271-3820-5798-af03-b7471433e0fa',
                                                    'name' => 'Vidéo',
                                                    'css' => '',
                                                    'type' => 'answer',
                                                    'exclusive_answer' => 0,
                                                    'complex_answer_mapping_key' => NULL,
                                                    'reusable_user_data_key' => NULL,
                                                    'custom_data' =>
                                                        array (
                                                        ),
                                                    'special_position' => NULL,
                                                    'translations' =>
                                                        array (
                                                            'en' =>
                                                                array (
                                                                    'name' => 'Vidéo',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'fr' =>
                                                                array (
                                                                    'name' => 'Vidéo',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'es' =>
                                                                array (
                                                                    'name' => 'Vidéo',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                        ),
                                                ),
                                            3 =>
                                                array (
                                                    'id' => '1531f1c2-bc66-51ea-8ce3-6cdbcc89f0c0',
                                                    'name' => 'Autres (copy)',
                                                    'css' => '',
                                                    'type' => 'answer',
                                                    'exclusive_answer' => 0,
                                                    'complex_answer_mapping_key' => NULL,
                                                    'reusable_user_data_key' => NULL,
                                                    'custom_data' =>
                                                        array (
                                                        ),
                                                    'special_position' => NULL,
                                                    'translations' =>
                                                        array (
                                                            'en' =>
                                                                array (
                                                                    'name' => 'Autres',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'fr' =>
                                                                array (
                                                                    'name' => 'Autres',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'es' =>
                                                                array (
                                                                    'name' => 'Autres',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                        ),
                                                ),
                                        ),
                                ),
                            3 =>
                                array (
                                    'id' => '5bea74dd-a588-5a22-8cdb-d05bfd8f411d',
                                    'name' => 'Si "Autres", veuillez préciser :',
                                    'css' => '',
                                    'type' => 'question',
                                    'question_type' => 'text',
                                    'required' => 1,
                                    'custom_data' =>
                                        array (
                                            'location_country' => '',
                                        ),
                                    'alpha_sort' => 0,
                                    'min_length' => 1,
                                    'max_length' => NULL,
                                    'validation_type' => NULL,
                                    'validation_mask' => NULL,
                                    'width' => NULL,
                                    'height' => NULL,
                                    'size_limit' => NULL,
                                    'file_types' =>
                                        array (
                                        ),
                                    'translations' =>
                                        array (
                                            'en' =>
                                                array (
                                                    'name' => 'Si "Autres", veuillez préciser :',
                                                    'introduction' => NULL,
                                                ),
                                            'fr' =>
                                                array (
                                                    'name' => 'Si "Autres", veuillez préciser :',
                                                    'introduction' => NULL,
                                                ),
                                            'es' =>
                                                array (
                                                    'name' => 'Si "Autres", veuillez préciser :',
                                                    'introduction' => NULL,
                                                ),
                                        ),
                                    'elements' =>
                                        array (
                                            0 =>
                                                array (
                                                    'id' => 'a560e313-d1d5-5904-b5af-7a5ddc0fbeb2',
                                                    'name' => 'Veuillez préciser : (copy)',
                                                    'css' => NULL,
                                                    'type' => 'answer',
                                                    'exclusive_answer' => 0,
                                                    'complex_answer_mapping_key' => NULL,
                                                    'reusable_user_data_key' => NULL,
                                                    'custom_data' =>
                                                        array (
                                                        ),
                                                    'special_position' => NULL,
                                                    'translations' =>
                                                        array (
                                                            'en' =>
                                                                array (
                                                                    'name' => '',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'fr' =>
                                                                array (
                                                                    'name' => NULL,
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'es' =>
                                                                array (
                                                                    'name' => '',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                        ),
                                                ),
                                        ),
                                ),
                            4 =>
                                array (
                                    'id' => '08d24153-a2f6-57a4-89c7-c0357636bb8b',
                                    'name' => 'Date de remise prévisionnelle des visuels (copy)',
                                    'css' => '',
                                    'type' => 'question',
                                    'question_type' => 'calendar',
                                    'required' => 1,
                                    'custom_data' =>
                                        array (
                                            'location_country' => '',
                                        ),
                                    'alpha_sort' => 0,
                                    'min_length' => 1,
                                    'max_length' => NULL,
                                    'validation_type' => NULL,
                                    'validation_mask' => NULL,
                                    'width' => NULL,
                                    'height' => NULL,
                                    'size_limit' => NULL,
                                    'file_types' =>
                                        array (
                                        ),
                                    'translations' =>
                                        array (
                                            'en' =>
                                                array (
                                                    'name' => 'Date de remise prévisionnelle des visuels',
                                                    'introduction' => NULL,
                                                ),
                                            'fr' =>
                                                array (
                                                    'name' => 'Date de remise prévisionnelle des visuels',
                                                    'introduction' => NULL,
                                                ),
                                            'es' =>
                                                array (
                                                    'name' => 'Date de remise prévisionnelle des visuels',
                                                    'introduction' => NULL,
                                                ),
                                        ),
                                    'elements' =>
                                        array (
                                            0 =>
                                                array (
                                                    'id' => '1937fcc8-0941-50e8-b792-194bb99284aa',
                                                    'name' => 'Date prévisionnelle de réception (copy)',
                                                    'css' => NULL,
                                                    'type' => 'answer',
                                                    'exclusive_answer' => 0,
                                                    'complex_answer_mapping_key' => NULL,
                                                    'reusable_user_data_key' => NULL,
                                                    'custom_data' =>
                                                        array (
                                                        ),
                                                    'special_position' => NULL,
                                                    'translations' =>
                                                        array (
                                                            'en' =>
                                                                array (
                                                                    'name' => '',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'fr' =>
                                                                array (
                                                                    'name' => NULL,
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'es' =>
                                                                array (
                                                                    'name' => '',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                        ),
                                                ),
                                        ),
                                ),
                        ),
                ),
            3 =>
                array (
                    'id' => '8f539168-8472-5501-8156-e4d719e4e188',
                    'name' => 'Visuel n°2',
                    'css' => 'add-remove template-section table-row saisie-des-calendriers--visuels--line',
                    'type' => 'section',
                    'translations' =>
                        array (
                            'en' =>
                                array (
                                    'name' => 'Visuel n°2',
                                    'introduction' => NULL,
                                ),
                            'fr' =>
                                array (
                                    'name' => 'Visuel n°2',
                                    'introduction' => NULL,
                                ),
                            'es' =>
                                array (
                                    'name' => 'Visuel n°2',
                                    'introduction' => NULL,
                                ),
                        ),
                    'elements' =>
                        array (
                            0 =>
                                array (
                                    'id' => '5ead2c49-5706-556c-bf99-6113117e6fae',
                                    'name' => 'N° du lot (copy)',
                                    'css' => '',
                                    'type' => 'question',
                                    'question_type' => 'text',
                                    'required' => 1,
                                    'custom_data' =>
                                        array (
                                            'location_country' => '',
                                        ),
                                    'alpha_sort' => 0,
                                    'min_length' => 1,
                                    'max_length' => NULL,
                                    'validation_type' => NULL,
                                    'validation_mask' => NULL,
                                    'width' => NULL,
                                    'height' => NULL,
                                    'size_limit' => NULL,
                                    'file_types' =>
                                        array (
                                        ),
                                    'translations' =>
                                        array (
                                            'en' =>
                                                array (
                                                    'name' => 'N° du lot',
                                                    'introduction' => NULL,
                                                ),
                                            'fr' =>
                                                array (
                                                    'name' => 'N° du lot',
                                                    'introduction' => NULL,
                                                ),
                                            'es' =>
                                                array (
                                                    'name' => 'N° du lot',
                                                    'introduction' => NULL,
                                                ),
                                        ),
                                    'elements' =>
                                        array (
                                            0 =>
                                                array (
                                                    'id' => '7d6874b0-ffad-5a9a-b123-86c383eb39dd',
                                                    'name' => 'N° du lot (copy)',
                                                    'css' => NULL,
                                                    'type' => 'answer',
                                                    'exclusive_answer' => 0,
                                                    'complex_answer_mapping_key' => NULL,
                                                    'reusable_user_data_key' => NULL,
                                                    'custom_data' =>
                                                        array (
                                                        ),
                                                    'special_position' => NULL,
                                                    'translations' =>
                                                        array (
                                                            'en' =>
                                                                array (
                                                                    'name' => '',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'fr' =>
                                                                array (
                                                                    'name' => NULL,
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'es' =>
                                                                array (
                                                                    'name' => '',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                        ),
                                                ),
                                        ),
                                ),
                            1 =>
                                array (
                                    'id' => '94018e11-b595-52b2-bb4f-fbb5628300b0',
                                    'name' => 'Nombre de visuels prévus (copy)',
                                    'css' => '',
                                    'type' => 'question',
                                    'question_type' => 'text',
                                    'required' => 1,
                                    'custom_data' =>
                                        array (
                                            'location_country' => '',
                                        ),
                                    'alpha_sort' => 0,
                                    'min_length' => 1,
                                    'max_length' => NULL,
                                    'validation_type' => 'digits',
                                    'validation_mask' => NULL,
                                    'width' => NULL,
                                    'height' => NULL,
                                    'size_limit' => NULL,
                                    'file_types' =>
                                        array (
                                        ),
                                    'translations' =>
                                        array (
                                            'en' =>
                                                array (
                                                    'name' => 'Nombre de visuels prévus',
                                                    'introduction' => NULL,
                                                ),
                                            'fr' =>
                                                array (
                                                    'name' => 'Nombre de visuels prévus',
                                                    'introduction' => NULL,
                                                ),
                                            'es' =>
                                                array (
                                                    'name' => 'Nombre de visuels prévus',
                                                    'introduction' => NULL,
                                                ),
                                        ),
                                    'elements' =>
                                        array (
                                            0 =>
                                                array (
                                                    'id' => '7abf5187-3071-5d2c-8689-4e2b263f4c87',
                                                    'name' => 'Nombre de visuels prévus (copy)',
                                                    'css' => NULL,
                                                    'type' => 'answer',
                                                    'exclusive_answer' => 0,
                                                    'complex_answer_mapping_key' => NULL,
                                                    'reusable_user_data_key' => NULL,
                                                    'custom_data' =>
                                                        array (
                                                        ),
                                                    'special_position' => NULL,
                                                    'translations' =>
                                                        array (
                                                            'en' =>
                                                                array (
                                                                    'name' => '',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'fr' =>
                                                                array (
                                                                    'name' => NULL,
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'es' =>
                                                                array (
                                                                    'name' => '',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                        ),
                                                ),
                                        ),
                                ),
                            2 =>
                                array (
                                    'id' => '8d702953-a909-57cf-bee2-5070140894d2',
                                    'name' => 'Type de visuels prévus (copy)',
                                    'css' => '',
                                    'type' => 'question',
                                    'question_type' => 'dropdown',
                                    'required' => 1,
                                    'custom_data' =>
                                        array (
                                            'location_country' => '',
                                        ),
                                    'alpha_sort' => 0,
                                    'min_length' => 1,
                                    'max_length' => NULL,
                                    'validation_type' => NULL,
                                    'validation_mask' => NULL,
                                    'width' => NULL,
                                    'height' => NULL,
                                    'size_limit' => NULL,
                                    'file_types' =>
                                        array (
                                        ),
                                    'translations' =>
                                        array (
                                            'en' =>
                                                array (
                                                    'name' => 'Type de visuels prévus',
                                                    'introduction' => NULL,
                                                ),
                                            'fr' =>
                                                array (
                                                    'name' => 'Type de visuels prévus',
                                                    'introduction' => NULL,
                                                ),
                                            'es' =>
                                                array (
                                                    'name' => 'Type de visuels prévus',
                                                    'introduction' => NULL,
                                                ),
                                        ),
                                    'elements' =>
                                        array (
                                            0 =>
                                                array (
                                                    'id' => '0f29d46d-0aba-5c45-91b0-45ac0cac09d5',
                                                    'name' => 'Photos (copy)',
                                                    'css' => '',
                                                    'type' => 'answer',
                                                    'exclusive_answer' => 0,
                                                    'complex_answer_mapping_key' => NULL,
                                                    'reusable_user_data_key' => NULL,
                                                    'custom_data' =>
                                                        array (
                                                        ),
                                                    'special_position' => NULL,
                                                    'translations' =>
                                                        array (
                                                            'en' =>
                                                                array (
                                                                    'name' => 'Photos',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'fr' =>
                                                                array (
                                                                    'name' => 'Photos',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'es' =>
                                                                array (
                                                                    'name' => 'Photos',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                        ),
                                                ),
                                            1 =>
                                                array (
                                                    'id' => 'd6304e91-4542-577b-890a-794295dc35ea',
                                                    'name' => 'Affiche',
                                                    'css' => '',
                                                    'type' => 'answer',
                                                    'exclusive_answer' => 0,
                                                    'complex_answer_mapping_key' => NULL,
                                                    'reusable_user_data_key' => NULL,
                                                    'custom_data' =>
                                                        array (
                                                        ),
                                                    'special_position' => NULL,
                                                    'translations' =>
                                                        array (
                                                            'en' =>
                                                                array (
                                                                    'name' => 'Affiche',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'fr' =>
                                                                array (
                                                                    'name' => 'Affiche',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'es' =>
                                                                array (
                                                                    'name' => 'Affiche',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                        ),
                                                ),
                                            2 =>
                                                array (
                                                    'id' => '5f914543-4d73-5b84-aa8f-5aeec0794791',
                                                    'name' => 'Vidéo',
                                                    'css' => '',
                                                    'type' => 'answer',
                                                    'exclusive_answer' => 0,
                                                    'complex_answer_mapping_key' => NULL,
                                                    'reusable_user_data_key' => NULL,
                                                    'custom_data' =>
                                                        array (
                                                        ),
                                                    'special_position' => NULL,
                                                    'translations' =>
                                                        array (
                                                            'en' =>
                                                                array (
                                                                    'name' => 'Vidéo',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'fr' =>
                                                                array (
                                                                    'name' => 'Vidéo',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'es' =>
                                                                array (
                                                                    'name' => 'Vidéo',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                        ),
                                                ),
                                            3 =>
                                                array (
                                                    'id' => '75c0358e-6d0f-5cec-9fd7-bb48ad002d68',
                                                    'name' => 'Autres (copy)',
                                                    'css' => '',
                                                    'type' => 'answer',
                                                    'exclusive_answer' => 0,
                                                    'complex_answer_mapping_key' => NULL,
                                                    'reusable_user_data_key' => NULL,
                                                    'custom_data' =>
                                                        array (
                                                        ),
                                                    'special_position' => NULL,
                                                    'translations' =>
                                                        array (
                                                            'en' =>
                                                                array (
                                                                    'name' => 'Autres',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'fr' =>
                                                                array (
                                                                    'name' => 'Autres',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'es' =>
                                                                array (
                                                                    'name' => 'Autres',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                        ),
                                                ),
                                        ),
                                ),
                            3 =>
                                array (
                                    'id' => 'bba06df1-63c3-543a-98b8-e131a84f8600',
                                    'name' => 'Si "Autres", veuillez préciser :',
                                    'css' => '',
                                    'type' => 'question',
                                    'question_type' => 'text',
                                    'required' => 1,
                                    'custom_data' =>
                                        array (
                                            'location_country' => '',
                                        ),
                                    'alpha_sort' => 0,
                                    'min_length' => 1,
                                    'max_length' => NULL,
                                    'validation_type' => NULL,
                                    'validation_mask' => NULL,
                                    'width' => NULL,
                                    'height' => NULL,
                                    'size_limit' => NULL,
                                    'file_types' =>
                                        array (
                                        ),
                                    'translations' =>
                                        array (
                                            'en' =>
                                                array (
                                                    'name' => 'Si "Autres", veuillez préciser :',
                                                    'introduction' => NULL,
                                                ),
                                            'fr' =>
                                                array (
                                                    'name' => 'Si "Autres", veuillez préciser :',
                                                    'introduction' => NULL,
                                                ),
                                            'es' =>
                                                array (
                                                    'name' => 'Si "Autres", veuillez préciser :',
                                                    'introduction' => NULL,
                                                ),
                                        ),
                                    'elements' =>
                                        array (
                                            0 =>
                                                array (
                                                    'id' => '8964d6f8-6644-594d-ba3a-72a808f6fa78',
                                                    'name' => 'Veuillez préciser : (copy)',
                                                    'css' => NULL,
                                                    'type' => 'answer',
                                                    'exclusive_answer' => 0,
                                                    'complex_answer_mapping_key' => NULL,
                                                    'reusable_user_data_key' => NULL,
                                                    'custom_data' =>
                                                        array (
                                                        ),
                                                    'special_position' => NULL,
                                                    'translations' =>
                                                        array (
                                                            'en' =>
                                                                array (
                                                                    'name' => '',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'fr' =>
                                                                array (
                                                                    'name' => NULL,
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'es' =>
                                                                array (
                                                                    'name' => '',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                        ),
                                                ),
                                        ),
                                ),
                            4 =>
                                array (
                                    'id' => 'b9c3a5d0-5702-5840-bbed-6a26ecec75b5',
                                    'name' => 'Date de remise prévisionnelle des visuels (copy)',
                                    'css' => '',
                                    'type' => 'question',
                                    'question_type' => 'calendar',
                                    'required' => 1,
                                    'custom_data' =>
                                        array (
                                            'location_country' => '',
                                        ),
                                    'alpha_sort' => 0,
                                    'min_length' => 1,
                                    'max_length' => NULL,
                                    'validation_type' => NULL,
                                    'validation_mask' => NULL,
                                    'width' => NULL,
                                    'height' => NULL,
                                    'size_limit' => NULL,
                                    'file_types' =>
                                        array (
                                        ),
                                    'translations' =>
                                        array (
                                            'en' =>
                                                array (
                                                    'name' => 'Date de remise prévisionnelle des visuels',
                                                    'introduction' => NULL,
                                                ),
                                            'fr' =>
                                                array (
                                                    'name' => 'Date de remise prévisionnelle des visuels',
                                                    'introduction' => NULL,
                                                ),
                                            'es' =>
                                                array (
                                                    'name' => 'Date de remise prévisionnelle des visuels',
                                                    'introduction' => NULL,
                                                ),
                                        ),
                                    'elements' =>
                                        array (
                                            0 =>
                                                array (
                                                    'id' => '1958c083-dfc1-5f9b-b6eb-a87258fe2af0',
                                                    'name' => 'Date prévisionnelle de réception (copy)',
                                                    'css' => NULL,
                                                    'type' => 'answer',
                                                    'exclusive_answer' => 0,
                                                    'complex_answer_mapping_key' => NULL,
                                                    'reusable_user_data_key' => NULL,
                                                    'custom_data' =>
                                                        array (
                                                        ),
                                                    'special_position' => NULL,
                                                    'translations' =>
                                                        array (
                                                            'en' =>
                                                                array (
                                                                    'name' => '',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'fr' =>
                                                                array (
                                                                    'name' => NULL,
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'es' =>
                                                                array (
                                                                    'name' => '',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                        ),
                                                ),
                                        ),
                                ),
                        ),
                ),
            4 =>
                array (
                    'id' => '8f4888e1-4113-5cc6-891d-9a09602f4cf1',
                    'name' => 'Visuel n°1',
                    'css' => 'add-remove template-section table-row saisie-des-calendriers--visuels--line',
                    'type' => 'section',
                    'translations' =>
                        array (
                            'en' =>
                                array (
                                    'name' => 'Visuel n°1',
                                    'introduction' => NULL,
                                ),
                            'fr' =>
                                array (
                                    'name' => 'Visuel n°1',
                                    'introduction' => NULL,
                                ),
                            'es' =>
                                array (
                                    'name' => 'Visuel n°1',
                                    'introduction' => NULL,
                                ),
                        ),
                    'elements' =>
                        array (
                            0 =>
                                array (
                                    'id' => '28588f5b-1b09-5169-bb90-f94a0dbbd6e8',
                                    'name' => 'N° du lot (copy)',
                                    'css' => '',
                                    'type' => 'question',
                                    'question_type' => 'text',
                                    'required' => 1,
                                    'custom_data' =>
                                        array (
                                            'location_country' => '',
                                        ),
                                    'alpha_sort' => 0,
                                    'min_length' => 1,
                                    'max_length' => NULL,
                                    'validation_type' => NULL,
                                    'validation_mask' => NULL,
                                    'width' => NULL,
                                    'height' => NULL,
                                    'size_limit' => NULL,
                                    'file_types' =>
                                        array (
                                        ),
                                    'translations' =>
                                        array (
                                            'en' =>
                                                array (
                                                    'name' => 'N° du lot',
                                                    'introduction' => NULL,
                                                ),
                                            'fr' =>
                                                array (
                                                    'name' => 'N° du lot',
                                                    'introduction' => NULL,
                                                ),
                                            'es' =>
                                                array (
                                                    'name' => 'N° du lot',
                                                    'introduction' => NULL,
                                                ),
                                        ),
                                    'elements' =>
                                        array (
                                            0 =>
                                                array (
                                                    'id' => '4af06a34-eeb4-5c1b-90d6-28d5097e9451',
                                                    'name' => 'N° du lot (copy)',
                                                    'css' => NULL,
                                                    'type' => 'answer',
                                                    'exclusive_answer' => 0,
                                                    'complex_answer_mapping_key' => NULL,
                                                    'reusable_user_data_key' => NULL,
                                                    'custom_data' =>
                                                        array (
                                                        ),
                                                    'special_position' => NULL,
                                                    'translations' =>
                                                        array (
                                                            'en' =>
                                                                array (
                                                                    'name' => '',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'fr' =>
                                                                array (
                                                                    'name' => NULL,
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'es' =>
                                                                array (
                                                                    'name' => '',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                        ),
                                                ),
                                        ),
                                ),
                            1 =>
                                array (
                                    'id' => '7217c4eb-080c-5224-8c0d-e5285c00e70f',
                                    'name' => 'Nombre de visuels prévus (copy)',
                                    'css' => '',
                                    'type' => 'question',
                                    'question_type' => 'text',
                                    'required' => 1,
                                    'custom_data' =>
                                        array (
                                            'location_country' => '',
                                        ),
                                    'alpha_sort' => 0,
                                    'min_length' => 1,
                                    'max_length' => NULL,
                                    'validation_type' => 'digits',
                                    'validation_mask' => NULL,
                                    'width' => NULL,
                                    'height' => NULL,
                                    'size_limit' => NULL,
                                    'file_types' =>
                                        array (
                                        ),
                                    'translations' =>
                                        array (
                                            'en' =>
                                                array (
                                                    'name' => 'Nombre de visuels prévus',
                                                    'introduction' => NULL,
                                                ),
                                            'fr' =>
                                                array (
                                                    'name' => 'Nombre de visuels prévus',
                                                    'introduction' => NULL,
                                                ),
                                            'es' =>
                                                array (
                                                    'name' => 'Nombre de visuels prévus',
                                                    'introduction' => NULL,
                                                ),
                                        ),
                                    'elements' =>
                                        array (
                                            0 =>
                                                array (
                                                    'id' => '976c73a5-0358-52c6-aefe-7fe9dfcb03f3',
                                                    'name' => 'Nombre de visuels prévus (copy)',
                                                    'css' => NULL,
                                                    'type' => 'answer',
                                                    'exclusive_answer' => 0,
                                                    'complex_answer_mapping_key' => NULL,
                                                    'reusable_user_data_key' => NULL,
                                                    'custom_data' =>
                                                        array (
                                                        ),
                                                    'special_position' => NULL,
                                                    'translations' =>
                                                        array (
                                                            'en' =>
                                                                array (
                                                                    'name' => '',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'fr' =>
                                                                array (
                                                                    'name' => NULL,
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'es' =>
                                                                array (
                                                                    'name' => '',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                        ),
                                                ),
                                        ),
                                ),
                            2 =>
                                array (
                                    'id' => 'aa35f238-f087-5de3-b28f-afff8f9ef170',
                                    'name' => 'Type de visuels prévus (copy)',
                                    'css' => '',
                                    'type' => 'question',
                                    'question_type' => 'dropdown',
                                    'required' => 1,
                                    'custom_data' =>
                                        array (
                                            'location_country' => '',
                                        ),
                                    'alpha_sort' => 0,
                                    'min_length' => 1,
                                    'max_length' => NULL,
                                    'validation_type' => NULL,
                                    'validation_mask' => NULL,
                                    'width' => NULL,
                                    'height' => NULL,
                                    'size_limit' => NULL,
                                    'file_types' =>
                                        array (
                                        ),
                                    'translations' =>
                                        array (
                                            'en' =>
                                                array (
                                                    'name' => 'Type de visuels prévus',
                                                    'introduction' => NULL,
                                                ),
                                            'fr' =>
                                                array (
                                                    'name' => 'Type de visuels prévus',
                                                    'introduction' => NULL,
                                                ),
                                            'es' =>
                                                array (
                                                    'name' => 'Type de visuels prévus',
                                                    'introduction' => NULL,
                                                ),
                                        ),
                                    'elements' =>
                                        array (
                                            0 =>
                                                array (
                                                    'id' => '7498fa52-f6b7-5fa9-96ec-079d2c1d3d81',
                                                    'name' => 'Photos (copy)',
                                                    'css' => '',
                                                    'type' => 'answer',
                                                    'exclusive_answer' => 0,
                                                    'complex_answer_mapping_key' => NULL,
                                                    'reusable_user_data_key' => NULL,
                                                    'custom_data' =>
                                                        array (
                                                        ),
                                                    'special_position' => NULL,
                                                    'translations' =>
                                                        array (
                                                            'en' =>
                                                                array (
                                                                    'name' => 'Photos',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'fr' =>
                                                                array (
                                                                    'name' => 'Photos',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'es' =>
                                                                array (
                                                                    'name' => 'Photos',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                        ),
                                                ),
                                            1 =>
                                                array (
                                                    'id' => '092be5e2-12e2-5f16-b146-7cc5f55db911',
                                                    'name' => 'Affiche',
                                                    'css' => '',
                                                    'type' => 'answer',
                                                    'exclusive_answer' => 0,
                                                    'complex_answer_mapping_key' => NULL,
                                                    'reusable_user_data_key' => NULL,
                                                    'custom_data' =>
                                                        array (
                                                        ),
                                                    'special_position' => NULL,
                                                    'translations' =>
                                                        array (
                                                            'en' =>
                                                                array (
                                                                    'name' => 'Affiche',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'fr' =>
                                                                array (
                                                                    'name' => 'Affiche',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'es' =>
                                                                array (
                                                                    'name' => 'Affiche',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                        ),
                                                ),
                                            2 =>
                                                array (
                                                    'id' => '916963f4-7392-5da5-b263-ab0c5147c710',
                                                    'name' => 'Vidéo',
                                                    'css' => '',
                                                    'type' => 'answer',
                                                    'exclusive_answer' => 0,
                                                    'complex_answer_mapping_key' => NULL,
                                                    'reusable_user_data_key' => NULL,
                                                    'custom_data' =>
                                                        array (
                                                        ),
                                                    'special_position' => NULL,
                                                    'translations' =>
                                                        array (
                                                            'en' =>
                                                                array (
                                                                    'name' => 'Vidéo',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'fr' =>
                                                                array (
                                                                    'name' => 'Vidéo',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'es' =>
                                                                array (
                                                                    'name' => 'Vidéo',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                        ),
                                                ),
                                            3 =>
                                                array (
                                                    'id' => '239293aa-11c8-5048-9aa1-7676b866ebf4',
                                                    'name' => 'Autres (copy)',
                                                    'css' => '',
                                                    'type' => 'answer',
                                                    'exclusive_answer' => 0,
                                                    'complex_answer_mapping_key' => NULL,
                                                    'reusable_user_data_key' => NULL,
                                                    'custom_data' =>
                                                        array (
                                                        ),
                                                    'special_position' => NULL,
                                                    'translations' =>
                                                        array (
                                                            'en' =>
                                                                array (
                                                                    'name' => 'Autres',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'fr' =>
                                                                array (
                                                                    'name' => 'Autres',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'es' =>
                                                                array (
                                                                    'name' => 'Autres',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                        ),
                                                ),
                                        ),
                                ),
                            3 =>
                                array (
                                    'id' => '27b9e3e7-a8f3-5fbc-a8bf-c553a3e5d4c7',
                                    'name' => 'Si "Autres", veuillez préciser :',
                                    'css' => '',
                                    'type' => 'question',
                                    'question_type' => 'text',
                                    'required' => 1,
                                    'custom_data' =>
                                        array (
                                            'location_country' => '',
                                        ),
                                    'alpha_sort' => 0,
                                    'min_length' => 1,
                                    'max_length' => NULL,
                                    'validation_type' => NULL,
                                    'validation_mask' => NULL,
                                    'width' => NULL,
                                    'height' => NULL,
                                    'size_limit' => NULL,
                                    'file_types' =>
                                        array (
                                        ),
                                    'translations' =>
                                        array (
                                            'en' =>
                                                array (
                                                    'name' => 'Si "Autres", veuillez préciser :',
                                                    'introduction' => NULL,
                                                ),
                                            'fr' =>
                                                array (
                                                    'name' => 'Si "Autres", veuillez préciser :',
                                                    'introduction' => NULL,
                                                ),
                                            'es' =>
                                                array (
                                                    'name' => 'Si "Autres", veuillez préciser :',
                                                    'introduction' => NULL,
                                                ),
                                        ),
                                    'elements' =>
                                        array (
                                            0 =>
                                                array (
                                                    'id' => '79a087db-8596-5522-9f4d-15a1e1e4a9b3',
                                                    'name' => 'Veuillez préciser : (copy)',
                                                    'css' => NULL,
                                                    'type' => 'answer',
                                                    'exclusive_answer' => 0,
                                                    'complex_answer_mapping_key' => NULL,
                                                    'reusable_user_data_key' => NULL,
                                                    'custom_data' =>
                                                        array (
                                                        ),
                                                    'special_position' => NULL,
                                                    'translations' =>
                                                        array (
                                                            'en' =>
                                                                array (
                                                                    'name' => '',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'fr' =>
                                                                array (
                                                                    'name' => NULL,
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'es' =>
                                                                array (
                                                                    'name' => '',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                        ),
                                                ),
                                        ),
                                ),
                            4 =>
                                array (
                                    'id' => '65f17d5d-61c2-5bfb-a1a3-c54451b8e7e4',
                                    'name' => 'Date de remise prévisionnelle des visuels (copy)',
                                    'css' => '',
                                    'type' => 'question',
                                    'question_type' => 'calendar',
                                    'required' => 1,
                                    'custom_data' =>
                                        array (
                                            'location_country' => '',
                                        ),
                                    'alpha_sort' => 0,
                                    'min_length' => 1,
                                    'max_length' => NULL,
                                    'validation_type' => NULL,
                                    'validation_mask' => NULL,
                                    'width' => NULL,
                                    'height' => NULL,
                                    'size_limit' => NULL,
                                    'file_types' =>
                                        array (
                                        ),
                                    'translations' =>
                                        array (
                                            'en' =>
                                                array (
                                                    'name' => 'Date de remise prévisionnelle des visuels',
                                                    'introduction' => NULL,
                                                ),
                                            'fr' =>
                                                array (
                                                    'name' => 'Date de remise prévisionnelle des visuels',
                                                    'introduction' => NULL,
                                                ),
                                            'es' =>
                                                array (
                                                    'name' => 'Date de remise prévisionnelle des visuels',
                                                    'introduction' => NULL,
                                                ),
                                        ),
                                    'elements' =>
                                        array (
                                            0 =>
                                                array (
                                                    'id' => 'a3093cf3-20ff-538f-9fab-0b935c3f9f94',
                                                    'name' => 'Date prévisionnelle de réception (copy)',
                                                    'css' => NULL,
                                                    'type' => 'answer',
                                                    'exclusive_answer' => 0,
                                                    'complex_answer_mapping_key' => NULL,
                                                    'reusable_user_data_key' => NULL,
                                                    'custom_data' =>
                                                        array (
                                                        ),
                                                    'special_position' => NULL,
                                                    'translations' =>
                                                        array (
                                                            'en' =>
                                                                array (
                                                                    'name' => '',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'fr' =>
                                                                array (
                                                                    'name' => NULL,
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'es' =>
                                                                array (
                                                                    'name' => '',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                        ),
                                                ),
                                        ),
                                ),
                        ),
                ),
            5 =>
                array (
                    'id' => '4f41e568-3bb6-599b-bc79-c992f27344bf',
                    'name' => 'Visuel n°5',
                    'css' => 'add-remove template-section table-row saisie-des-calendriers--visuels--line',
                    'type' => 'section',
                    'translations' =>
                        array (
                            'en' =>
                                array (
                                    'name' => 'Visuel n°5',
                                    'introduction' => NULL,
                                ),
                            'fr' =>
                                array (
                                    'name' => 'Visuel n°5',
                                    'introduction' => NULL,
                                ),
                            'es' =>
                                array (
                                    'name' => 'Visuel n°5',
                                    'introduction' => NULL,
                                ),
                        ),
                    'elements' =>
                        array (
                            0 =>
                                array (
                                    'id' => '1934d379-1eee-5573-bd9f-4ebe5adf7b8c',
                                    'name' => 'N° du lot',
                                    'css' => '',
                                    'type' => 'question',
                                    'question_type' => 'text',
                                    'required' => 1,
                                    'custom_data' =>
                                        array (
                                            'location_country' => '',
                                        ),
                                    'alpha_sort' => 0,
                                    'min_length' => 1,
                                    'max_length' => NULL,
                                    'validation_type' => NULL,
                                    'validation_mask' => NULL,
                                    'width' => NULL,
                                    'height' => NULL,
                                    'size_limit' => NULL,
                                    'file_types' =>
                                        array (
                                        ),
                                    'translations' =>
                                        array (
                                            'en' =>
                                                array (
                                                    'name' => 'N° du lot',
                                                    'introduction' => NULL,
                                                ),
                                            'fr' =>
                                                array (
                                                    'name' => 'N° du lot',
                                                    'introduction' => NULL,
                                                ),
                                            'es' =>
                                                array (
                                                    'name' => 'N° du lot',
                                                    'introduction' => NULL,
                                                ),
                                        ),
                                    'elements' =>
                                        array (
                                            0 =>
                                                array (
                                                    'id' => '38894a73-715f-5dca-a1e0-11a8fc850bc1',
                                                    'name' => 'N° du lot',
                                                    'css' => NULL,
                                                    'type' => 'answer',
                                                    'exclusive_answer' => 0,
                                                    'complex_answer_mapping_key' => NULL,
                                                    'reusable_user_data_key' => NULL,
                                                    'custom_data' =>
                                                        array (
                                                        ),
                                                    'special_position' => NULL,
                                                    'translations' =>
                                                        array (
                                                            'en' =>
                                                                array (
                                                                    'name' => '',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'fr' =>
                                                                array (
                                                                    'name' => NULL,
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'es' =>
                                                                array (
                                                                    'name' => '',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                        ),
                                                ),
                                        ),
                                ),
                            1 =>
                                array (
                                    'id' => '78ae5080-4cb9-59a1-a3ff-2a6eba862e50',
                                    'name' => 'Nombre de visuels prévus',
                                    'css' => '',
                                    'type' => 'question',
                                    'question_type' => 'text',
                                    'required' => 1,
                                    'custom_data' =>
                                        array (
                                            'location_country' => '',
                                        ),
                                    'alpha_sort' => 0,
                                    'min_length' => 1,
                                    'max_length' => NULL,
                                    'validation_type' => 'digits',
                                    'validation_mask' => NULL,
                                    'width' => NULL,
                                    'height' => NULL,
                                    'size_limit' => NULL,
                                    'file_types' =>
                                        array (
                                        ),
                                    'translations' =>
                                        array (
                                            'en' =>
                                                array (
                                                    'name' => 'Nombre de visuels prévus',
                                                    'introduction' => NULL,
                                                ),
                                            'fr' =>
                                                array (
                                                    'name' => 'Nombre de visuels prévus',
                                                    'introduction' => NULL,
                                                ),
                                            'es' =>
                                                array (
                                                    'name' => 'Nombre de visuels prévus',
                                                    'introduction' => NULL,
                                                ),
                                        ),
                                    'elements' =>
                                        array (
                                            0 =>
                                                array (
                                                    'id' => 'df74840b-4bec-5891-842f-7dcda11dcd62',
                                                    'name' => 'Nombre de visuels prévus',
                                                    'css' => NULL,
                                                    'type' => 'answer',
                                                    'exclusive_answer' => 0,
                                                    'complex_answer_mapping_key' => NULL,
                                                    'reusable_user_data_key' => NULL,
                                                    'custom_data' =>
                                                        array (
                                                        ),
                                                    'special_position' => NULL,
                                                    'translations' =>
                                                        array (
                                                            'en' =>
                                                                array (
                                                                    'name' => '',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'fr' =>
                                                                array (
                                                                    'name' => NULL,
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'es' =>
                                                                array (
                                                                    'name' => '',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                        ),
                                                ),
                                        ),
                                ),
                            2 =>
                                array (
                                    'id' => 'a7e4ae4c-9dba-5f5b-80b9-174aae8f70b7',
                                    'name' => 'Type de visuels prévus',
                                    'css' => '',
                                    'type' => 'question',
                                    'question_type' => 'dropdown',
                                    'required' => 1,
                                    'custom_data' =>
                                        array (
                                            'location_country' => '',
                                        ),
                                    'alpha_sort' => 0,
                                    'min_length' => 1,
                                    'max_length' => NULL,
                                    'validation_type' => NULL,
                                    'validation_mask' => NULL,
                                    'width' => NULL,
                                    'height' => NULL,
                                    'size_limit' => NULL,
                                    'file_types' =>
                                        array (
                                        ),
                                    'translations' =>
                                        array (
                                            'en' =>
                                                array (
                                                    'name' => 'Type de visuels prévus',
                                                    'introduction' => NULL,
                                                ),
                                            'fr' =>
                                                array (
                                                    'name' => 'Type de visuels prévus',
                                                    'introduction' => NULL,
                                                ),
                                            'es' =>
                                                array (
                                                    'name' => 'Type de visuels prévus',
                                                    'introduction' => NULL,
                                                ),
                                        ),
                                    'elements' =>
                                        array (
                                            0 =>
                                                array (
                                                    'id' => '8bce65bb-9dbe-5f77-b98d-2574cd33597f',
                                                    'name' => 'Photos',
                                                    'css' => '',
                                                    'type' => 'answer',
                                                    'exclusive_answer' => 0,
                                                    'complex_answer_mapping_key' => NULL,
                                                    'reusable_user_data_key' => NULL,
                                                    'custom_data' =>
                                                        array (
                                                        ),
                                                    'special_position' => NULL,
                                                    'translations' =>
                                                        array (
                                                            'en' =>
                                                                array (
                                                                    'name' => 'Photos',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'fr' =>
                                                                array (
                                                                    'name' => 'Photos',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'es' =>
                                                                array (
                                                                    'name' => 'Photos',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                        ),
                                                ),
                                            1 =>
                                                array (
                                                    'id' => '3d821b3f-62d4-55c8-a74c-fb9d9cddb075',
                                                    'name' => 'Affiche',
                                                    'css' => '',
                                                    'type' => 'answer',
                                                    'exclusive_answer' => 0,
                                                    'complex_answer_mapping_key' => NULL,
                                                    'reusable_user_data_key' => NULL,
                                                    'custom_data' =>
                                                        array (
                                                        ),
                                                    'special_position' => NULL,
                                                    'translations' =>
                                                        array (
                                                            'en' =>
                                                                array (
                                                                    'name' => 'Affiche',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'fr' =>
                                                                array (
                                                                    'name' => 'Affiche',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'es' =>
                                                                array (
                                                                    'name' => 'Affiche',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                        ),
                                                ),
                                            2 =>
                                                array (
                                                    'id' => '84371536-fed6-5d7d-a909-b08f0af4ac80',
                                                    'name' => 'Vidéo',
                                                    'css' => '',
                                                    'type' => 'answer',
                                                    'exclusive_answer' => 0,
                                                    'complex_answer_mapping_key' => NULL,
                                                    'reusable_user_data_key' => NULL,
                                                    'custom_data' =>
                                                        array (
                                                        ),
                                                    'special_position' => NULL,
                                                    'translations' =>
                                                        array (
                                                            'en' =>
                                                                array (
                                                                    'name' => 'Vidéo',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'fr' =>
                                                                array (
                                                                    'name' => 'Vidéo',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'es' =>
                                                                array (
                                                                    'name' => 'Vidéo',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                        ),
                                                ),
                                            3 =>
                                                array (
                                                    'id' => '1e782c34-9055-5634-a58f-bb9e84a1dcf7',
                                                    'name' => 'Autres',
                                                    'css' => '',
                                                    'type' => 'answer',
                                                    'exclusive_answer' => 0,
                                                    'complex_answer_mapping_key' => NULL,
                                                    'reusable_user_data_key' => NULL,
                                                    'custom_data' =>
                                                        array (
                                                        ),
                                                    'special_position' => NULL,
                                                    'translations' =>
                                                        array (
                                                            'en' =>
                                                                array (
                                                                    'name' => 'Autres',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'fr' =>
                                                                array (
                                                                    'name' => 'Autres',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'es' =>
                                                                array (
                                                                    'name' => 'Autres',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                        ),
                                                ),
                                        ),
                                ),
                            3 =>
                                array (
                                    'id' => '8077c2a3-9068-5eb2-8c76-0d49c54f2627',
                                    'name' => 'Si "Autres", veuillez préciser :',
                                    'css' => '',
                                    'type' => 'question',
                                    'question_type' => 'text',
                                    'required' => 1,
                                    'custom_data' =>
                                        array (
                                            'location_country' => '',
                                        ),
                                    'alpha_sort' => 0,
                                    'min_length' => 1,
                                    'max_length' => NULL,
                                    'validation_type' => NULL,
                                    'validation_mask' => NULL,
                                    'width' => NULL,
                                    'height' => NULL,
                                    'size_limit' => NULL,
                                    'file_types' =>
                                        array (
                                        ),
                                    'translations' =>
                                        array (
                                            'en' =>
                                                array (
                                                    'name' => 'Si "Autres", veuillez préciser :',
                                                    'introduction' => NULL,
                                                ),
                                            'fr' =>
                                                array (
                                                    'name' => 'Si "Autres", veuillez préciser :',
                                                    'introduction' => NULL,
                                                ),
                                            'es' =>
                                                array (
                                                    'name' => 'Si "Autres", veuillez préciser :',
                                                    'introduction' => NULL,
                                                ),
                                        ),
                                    'elements' =>
                                        array (
                                            0 =>
                                                array (
                                                    'id' => '63ccd158-91e7-5f5d-8766-12ba2fd13d13',
                                                    'name' => 'Veuillez préciser :',
                                                    'css' => NULL,
                                                    'type' => 'answer',
                                                    'exclusive_answer' => 0,
                                                    'complex_answer_mapping_key' => NULL,
                                                    'reusable_user_data_key' => NULL,
                                                    'custom_data' =>
                                                        array (
                                                        ),
                                                    'special_position' => NULL,
                                                    'translations' =>
                                                        array (
                                                            'en' =>
                                                                array (
                                                                    'name' => '',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'fr' =>
                                                                array (
                                                                    'name' => NULL,
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'es' =>
                                                                array (
                                                                    'name' => '',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                        ),
                                                ),
                                        ),
                                ),
                            4 =>
                                array (
                                    'id' => '16bb8d55-4cfd-548b-b2ee-463698368eb8',
                                    'name' => 'Date de remise prévisionnelle des visuels',
                                    'css' => '',
                                    'type' => 'question',
                                    'question_type' => 'calendar',
                                    'required' => 1,
                                    'custom_data' =>
                                        array (
                                            'location_country' => '',
                                        ),
                                    'alpha_sort' => 0,
                                    'min_length' => 1,
                                    'max_length' => NULL,
                                    'validation_type' => NULL,
                                    'validation_mask' => NULL,
                                    'width' => NULL,
                                    'height' => NULL,
                                    'size_limit' => NULL,
                                    'file_types' =>
                                        array (
                                        ),
                                    'translations' =>
                                        array (
                                            'en' =>
                                                array (
                                                    'name' => 'Date de remise prévisionnelle des visuels',
                                                    'introduction' => NULL,
                                                ),
                                            'fr' =>
                                                array (
                                                    'name' => 'Date de remise prévisionnelle des visuels',
                                                    'introduction' => NULL,
                                                ),
                                            'es' =>
                                                array (
                                                    'name' => 'Date de remise prévisionnelle des visuels',
                                                    'introduction' => NULL,
                                                ),
                                        ),
                                    'elements' =>
                                        array (
                                            0 =>
                                                array (
                                                    'id' => '78b4a536-d99b-5de8-9bd9-762d2adbfcb7',
                                                    'name' => 'Date prévisionnelle de réception',
                                                    'css' => NULL,
                                                    'type' => 'answer',
                                                    'exclusive_answer' => 0,
                                                    'complex_answer_mapping_key' => NULL,
                                                    'reusable_user_data_key' => NULL,
                                                    'custom_data' =>
                                                        array (
                                                        ),
                                                    'special_position' => NULL,
                                                    'translations' =>
                                                        array (
                                                            'en' =>
                                                                array (
                                                                    'name' => '',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'fr' =>
                                                                array (
                                                                    'name' => NULL,
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                            'es' =>
                                                                array (
                                                                    'name' => '',
                                                                    'suffix' => NULL,
                                                                    'prefix' => NULL,
                                                                ),
                                                        ),
                                                ),
                                        ),
                                ),
                        ),
                ),
        );

        $addRemoveAnswers = [];
        foreach ($addRemoves as $section) {
            $questions = $section['elements'];
            $sectionAnswers = [];

            foreach ($questions as $question) {
                $answers = array_map(
                    function ($answer) {
                        // We take the french translation $answer['translations']['fr']['name']
                        // as the answer for readability/usability reason (mainly in case of dropdowns)
                        return new FormAnswer($answer['id'], $answer['translations']['fr']['name'], $answer['name']);
                    },
                    $question['elements']
                );

                $sectionAnswers = array_merge($sectionAnswers, $answers);
            }

            array_push($addRemoveAnswers, $sectionAnswers);
        }

        return $addRemoveAnswers;
    }
}
