<?php

namespace Optimy\CallbackReceiverLambda\BusinessFlows;


use Optimy\CallbackReceiverLambda\Application\BusinessFlows\BusinessFlowFactory;
use Optimy\CallbackReceiverLambda\Application\BusinessFlows\BusinessFlows;
use Optimy\CallbackReceiverLambda\Application\BusinessFlows\LABBusinessFlow;
use Optimy\Infrastructure\Repositories\EmailTemplateRepository;
use Optimy\Infrastructure\Repositories\FormRepository;
use Optimy\Infrastructure\Repositories\ProjectRepository;
use Optimy\Infrastructure\Repositories\TaskAssigneeRepository;
use Optimy\Infrastructure\Repositories\TaskRepository;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;

class BusinessFlowFactoryTest extends TestCase
{
    private $factory;

    public function setUp(): void
    {
        $logger = $this->createMock(LoggerInterface::class);
        $projectRepository = $this->createMock(ProjectRepository::class);
        $emailTemplateRepository = $this->createMock(EmailTemplateRepository::class);
        $taskRepository = $this->createMock(TaskRepository::class);
        $taskAssigneeRepository = $this->createMock(TaskAssigneeRepository::class);
        $formRepository = $this->createMock(FormRepository::class);

        $this->factory = new BusinessFlowFactory($logger, $projectRepository, $taskRepository, $emailTemplateRepository, $taskAssigneeRepository, $formRepository);
    }

    public function test_create_should_return_instance_of_LABBusinessFlow()
    {
        self::assertInstanceOf(LABBusinessFlow::class, $this->factory->create(BusinessFlows::LAB_TASK));
    }
}
