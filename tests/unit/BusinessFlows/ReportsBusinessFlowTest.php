<?php

namespace Optimy\CallbackReceiverLambda\BusinessFlows;


use Optimy\CallbackReceiverLambda\Application\BusinessFlows\ReportsBusinessFlow;
use Optimy\Domain\EmailTemplate;
use Optimy\Domain\FormAnswer;
use Optimy\Domain\Project;
use Optimy\Domain\ProjectAnswer;
use Optimy\Domain\ProjectPartVersion;
use Optimy\Domain\Task;
use Optimy\Infrastructure\Repositories\EmailTemplateRepository;
use Optimy\Infrastructure\Repositories\FormRepository;
use Optimy\Infrastructure\Repositories\ProjectRepository;
use Optimy\Infrastructure\Repositories\TaskAssigneeRepository;
use Optimy\Infrastructure\Repositories\TaskRepository;
use Optimy\CallbackReceiverLambda\Domain\AssigneeRoles;
use Optimy\CallbackReceiverLambda\Domain\EmailTemplates;
use Optimy\CallbackReceiverLambda\Domain\FormParts;
use Optimy\CallbackReceiverLambda\Domain\Forms;
use Optimy\CallbackReceiverLambda\Domain\FormScreens;
use Optimy\CallbackReceiverLambda\Domain\ProjectStatuses;
use Optimy\CallbackReceiverLambda\Domain\Tasks;
use Optimy\CallbackReceiverLambda\Domain\TaskTags;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Ramsey\Uuid\Uuid;

class ReportsBusinessFlowTest extends TestCase
{
    private $logger;
    private $projectRepository;
    private $taskRepository;
    private $emailTemplateRepository;
    private $taskAssigneeRepository;
    private $formRepository;

    private $flow;

    public function setup()
    {
        $this->logger = $this->createMock(LoggerInterface::class);
        $this->projectRepository = $this->createMock(ProjectRepository::class);
        $this->taskRepository = $this->createMock(TaskRepository::class);
        $this->emailTemplateRepository = $this->createMock(EmailTemplateRepository::class);
        $this->taskAssigneeRepository = $this->createMock(TaskAssigneeRepository::class);
        $this->formRepository = $this->createMock(FormRepository::class);

        $this->flow = new ReportsBusinessFlow($this->logger, $this->projectRepository, $this->taskRepository, $this->emailTemplateRepository, $this->taskAssigneeRepository, $this->formRepository);
    }

    public function test_execute_no_answers_for_the_formScreen_should_end_process()
    {
        $projectId = Uuid::uuid4()->toString();
        $project = $this->getProject($projectId, 'BE-0001', ProjectStatuses::PROJET_SIGNE_DES_PARTIES, Forms::REFERENTIEL_CONVENTIONS);
        $projectVersion = $this->getProjectPartVersion();

        $this->projectRepository
            ->method('getProjectLastPublishedVersionOfFormPart')
            ->with(
                $this->equalTo($projectId),
                $this->equalTo(FormParts::SAISIE_DES_CALENDRIERS)
            )
            ->willReturn($projectVersion);

        $answers = $this->generateAnswers(2, [FormScreens::SAISIE_DES_AF_BUDGET, FormScreens::SAISIE_DES_AF_BUDGET]);

        $this->projectRepository
            ->expects($this->once())
            ->method('getProjectAnswers')
            ->with(
                $this->equalTo($projectId),
                $this->equalTo($projectVersion->getId())
            )
            ->willReturn($answers);

        $this->logger
            ->method('info')
            ->with($this->equalTo("Last published version {$projectVersion->getId()} for project {$projectId} does not have any answers published in screen ". FormScreens::SAISIE_DES_CALENDRIERS .". End of process."));

        self::assertTrue($this->flow->execute($project, $projectVersion));
    }

    public function test_execute_task_exists_for_docType1_but_is_completed_should_log_info_and_create_task_for_docType2()
    {
        $projectId = Uuid::uuid4()->toString();
        $project = $this->getProject($projectId, 'BE-0001', ProjectStatuses::PROJET_SIGNE_DES_PARTIES, Forms::REFERENTIEL_CONVENTIONS);
        $projectVersion = $this->getProjectPartVersion();

        $this->projectRepository
            ->method('getProjectLastPublishedVersionOfFormPart')
            ->with(
                $this->equalTo($projectId),
                $this->equalTo(FormParts::SAISIE_DES_CALENDRIERS)
            )
            ->willReturn($projectVersion);

        // Create Project Answers
        $projectAnswers = [];
        $docType1 = new ProjectAnswer(Uuid::uuid4(), 'FS1', '1', Uuid::uuid4()->toString(), FormScreens::SAISIE_DES_CALENDRIERS);
        $date1 = new ProjectAnswer(Uuid::uuid4(), 'Date prévisionnelle', '2022-01-12', Uuid::uuid4()->toString(), FormScreens::SAISIE_DES_CALENDRIERS);
        $docType2 = new ProjectAnswer(Uuid::uuid4(), 'RI1', '1', Uuid::uuid4()->toString(), FormScreens::SAISIE_DES_CALENDRIERS);
        $date2 = new ProjectAnswer(Uuid::uuid4(), 'Date prévisionnelle', '2022-05-12', Uuid::uuid4()->toString(), FormScreens::SAISIE_DES_CALENDRIERS);

        array_push($projectAnswers, $docType1, $date1, $docType2, $date2);

        // Create Add removes Form Answers
        $addRemoveAnswers = [];
        $addRemove1 = [];
        array_push(
            $addRemove1,
            new FormAnswer($docType1->getId(), 'FS1', 'FS1'),
            new FormAnswer(Uuid::uuid4()->toString(), 'RI1', 'RI1'),
            new FormAnswer($date1->getId(), null, 'Date prévisionnelle')
            );

        $addRemove2 = [];
        array_push(
            $addRemove2,
            new FormAnswer($docType2->getId(), 'RI1', 'RI1'),
            new FormAnswer(Uuid::uuid4()->toString(), 'RI2', 'RI2'),
            new FormAnswer($date2->getId(), null, 'Date prévisionnelle')
        );

        array_push($addRemoveAnswers, $addRemove1, $addRemove2);

        $this->projectRepository
            ->expects($this->once())
            ->method('getProjectAnswers')
            ->with(
                $this->equalTo($projectId),
                $this->equalTo($projectVersion->getId())
            )
            ->willReturn($projectAnswers);

        $this->formRepository
            ->expects($this->once())
            ->method('getAddRemoveAnswersStructure')
            ->with(
                $this->equalTo(Forms::REFERENTIEL_CONVENTIONS),
                $this->equalTo(FormParts::SAISIE_DES_CALENDRIERS),
                $this->equalTo(FormScreens::SAISIE_DES_CALENDRIERS),
                $this->equalTo(ReportsBusinessFlow::ADD_REMOVE)
            )
            ->willReturn($addRemoveAnswers);

        $task = new Task(Tasks::REPORTS['FS'], 123, 123, '2017-08-05', $projectId);
        $task->wasCompletedOn('2017-08-05');
        $this->taskRepository
            ->expects($this->at(0))
            ->method('getProjectTasksByTag')
            ->with(
                $this->equalTo($projectId),
                $this->equalTo(TaskTags::RAPPORTS . $docType1->getName())
            )
            ->willReturn([$task]);

        $this->logger
            ->method('info')
            ->with(
                $this->equalTo("Task '". $task->getName() ."' already exists for project id {$project->getId()} and document type {$docType1->getName()}. Continue.")
            );

        $this->taskRepository
            ->expects($this->at(1))
            ->method('getProjectTasksByTag')
            ->with(
                $this->equalTo($projectId),
                $this->equalTo(TaskTags::RAPPORTS . $docType2->getName())
            )
            ->willReturn([]);

        $this->taskAssigneeRepository
            ->method('getUserIdFromTeamWithGivenRole')
            ->with(
                $this->equalTo($project->getLeaderTeamId()),
                $this->equalTo(AssigneeRoles::CHEF_DE_PROJET)
            )
            ->willReturn(123);

        $emailTemplate = new EmailTemplate(1, EmailTemplates::RAPPORT_NON_REMIS, 'Les documents auraient du nous être remis à la date suivante : {answer.date}.');
        $this->emailTemplateRepository
            ->method('getTemplateById')
            ->with(
                $this->equalTo(EmailTemplates::RAPPORT_NON_REMIS)
            )
            ->willReturn($emailTemplate);

        $this->taskRepository
            ->expects($this->at(2))
            ->method('saveTask')
            ->willReturn(true);

        self::assertTrue($this->flow->execute($project));
    }

    public function test_execute_task_exists_for_docType2_and_is_not_completed_should_update_task()
    {
        $projectId = Uuid::uuid4()->toString();
        $project = $this->getProject($projectId, 'BE-0001', ProjectStatuses::PROJET_SIGNE_DES_PARTIES, Forms::REFERENTIEL_CONVENTIONS);
        $projectVersion = $this->getProjectPartVersion();

        $this->projectRepository
            ->method('getProjectLastPublishedVersionOfFormPart')
            ->with(
                $this->equalTo($projectId),
                $this->equalTo(FormParts::SAISIE_DES_CALENDRIERS)
            )
            ->willReturn($projectVersion);

        // Create Project Answers
        $projectAnswers = [];
        $docType1 = new ProjectAnswer(Uuid::uuid4(), 'FS1', '1', Uuid::uuid4()->toString(), FormScreens::SAISIE_DES_CALENDRIERS);
        $date1 = new ProjectAnswer(Uuid::uuid4(), 'Date prévisionnelle', '2022-01-12', Uuid::uuid4()->toString(), FormScreens::SAISIE_DES_CALENDRIERS);
        $docType2 = new ProjectAnswer(Uuid::uuid4(), 'RI1', '1', Uuid::uuid4()->toString(), FormScreens::SAISIE_DES_CALENDRIERS);
        $date2 = new ProjectAnswer(Uuid::uuid4(), 'Date prévisionnelle', '2022-05-12', Uuid::uuid4()->toString(), FormScreens::SAISIE_DES_CALENDRIERS);

        array_push($projectAnswers, $docType1, $date1, $docType2, $date2);

        // Create Add removes Form Answers
        $addRemoveAnswers = [];
        $addRemove1 = [];
        array_push(
            $addRemove1,
            new FormAnswer($docType1->getId(), 'FS1', 'FS1'),
            new FormAnswer(Uuid::uuid4()->toString(), 'RI1', 'RI1'),
            new FormAnswer($date1->getId(), null, 'Date prévisionnelle')
        );

        $addRemove2 = [];
        array_push(
            $addRemove2,
            new FormAnswer($docType2->getId(), 'RI1', 'RI1'),
            new FormAnswer(Uuid::uuid4()->toString(), 'RI2', 'RI2'),
            new FormAnswer($date2->getId(), null, 'Date prévisionnelle')
        );

        array_push($addRemoveAnswers, $addRemove1, $addRemove2);

        $this->projectRepository
            ->expects($this->once())
            ->method('getProjectAnswers')
            ->with(
                $this->equalTo($projectId),
                $this->equalTo($projectVersion->getId())
            )
            ->willReturn($projectAnswers);

        $this->formRepository
            ->expects($this->once())
            ->method('getAddRemoveAnswersStructure')
            ->with(
                $this->equalTo(Forms::REFERENTIEL_CONVENTIONS),
                $this->equalTo(FormParts::SAISIE_DES_CALENDRIERS),
                $this->equalTo(FormScreens::SAISIE_DES_CALENDRIERS),
                $this->equalTo(ReportsBusinessFlow::ADD_REMOVE)
            )
            ->willReturn($addRemoveAnswers);

        $task = new Task(Tasks::REPORTS['FS'], 123, 123, '2017-08-05', $projectId);
        $task->wasCompletedOn('2017-08-05');
        $this->taskRepository
            ->expects($this->at(0))
            ->method('getProjectTasksByTag')
            ->with(
                $this->equalTo($projectId),
                $this->equalTo(TaskTags::RAPPORTS . $docType1->getName())
            )
            ->willReturn([$task]);

        $this->logger
            ->expects($this->at(0))
            ->method('info')
            ->with(
                $this->equalTo("Task '". $task->getName() ."' already exists for project id {$project->getId()} and document type {$docType1->getName()}. Continue.")
            );

        $task2 = new Task(Tasks::REPORTS['RI'], 123, 123, '2022-08-05', $projectId);
        $this->taskRepository
            ->expects($this->at(1))
            ->method('getProjectTasksByTag')
            ->with(
                $this->equalTo($projectId),
                $this->equalTo(TaskTags::RAPPORTS . $docType2->getName())
            )
            ->willReturn([$task2]);

        $this->logger
            ->expects($this->at(1))
            ->method('info')
            ->with(
                $this->equalTo("Updating due date (new value: {$date2->getValue()}) in task '". $task2->getName() ."' for project id {$project->getId()} and document type {$docType2->getName()}.")
            );

        $this->emailTemplateRepository
            ->expects($this->never())
            ->method('getTemplateById');

        $this->taskRepository
            ->expects($this->at(2))
            ->method('saveTask')
            ->willReturn(true);

        self::assertTrue($this->flow->execute($project, $projectVersion));
    }

    private function generateAnswers(int $amount, array $formScreens, array $names = [], array $values = [])
    {
        $answers = [];
        for ($i = 0; $i < $amount; $i++) {
            array_push(
                $answers,
                new ProjectAnswer(
                    Uuid::uuid4()->toString(),
                    empty($names) ? 'Answer name' : $names[$i],
                    empty($names) ? 'Answer value' : $values[$i],
                    Uuid::uuid4()->toString(),
                    $formScreens[$i]
                )
            );
        }

        return $answers;
    }

    private function getProject(string $projectId, string $toolReference, string $status, string $form): Project
    {
        return new Project(
            $projectId,
            $toolReference,
            'Project/Partner name',
            $status,
            $form,
            1235
        );
    }

    private function getProjectPartVersion(): ProjectPartVersion
    {
        return new ProjectPartVersion(
            Uuid::uuid4()->toString(),
            FormParts::SAISIE_DES_CALENDRIERS,
            1,
            2,
            (new \DateTime('now - 1 week'))->format('Y-m-d')
        );
    }
}
