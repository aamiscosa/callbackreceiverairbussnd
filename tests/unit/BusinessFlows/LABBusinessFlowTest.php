<?php

namespace Optimy\CallbackReceiverLambda\BusinessFlows;


use Optimy\CallbackReceiverLambda\Application\BusinessFlows\LABBusinessFlow;
use Optimy\Domain\EmailTemplate;
use Optimy\Domain\Project;
use Optimy\Domain\ProjectAnswer;
use Optimy\Domain\ProjectPartVersion;
use Optimy\Domain\Task;
use Optimy\Infrastructure\Repositories\EmailTemplateRepository;
use Optimy\Infrastructure\Repositories\FormRepository;
use Optimy\Infrastructure\Repositories\ProjectRepository;
use Optimy\Infrastructure\Repositories\TaskAssigneeRepository;
use Optimy\Infrastructure\Repositories\TaskRepository;
use Optimy\CallbackReceiverLambda\Domain\AssigneeRoles;
use Optimy\CallbackReceiverLambda\Domain\EmailTemplates;
use Optimy\CallbackReceiverLambda\Domain\FormParts;
use Optimy\CallbackReceiverLambda\Domain\Forms;
use Optimy\CallbackReceiverLambda\Domain\FormScreens;
use Optimy\CallbackReceiverLambda\Domain\ProjectStatuses;
use Optimy\CallbackReceiverLambda\Domain\Tasks;
use Optimy\CallbackReceiverLambda\Domain\TaskTags;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Ramsey\Uuid\Uuid;

class LABBusinessFlowTest extends TestCase
{
    private $logger;
    private $projectRepository;
    private $taskRepository;
    private $emailTemplateRepository;
    private $taskAssigneeRepository;

    private $flow;

    public function setup()
    {
        $this->logger = $this->createMock(LoggerInterface::class);
        $this->projectRepository = $this->createMock(ProjectRepository::class);
        $this->taskRepository = $this->createMock(TaskRepository::class);
        $this->emailTemplateRepository = $this->createMock(EmailTemplateRepository::class);
        $this->taskAssigneeRepository = $this->createMock(TaskAssigneeRepository::class);
        $formRepository = $this->createMock(FormRepository::class);

        $this->flow = new LABBusinessFlow($this->logger, $this->projectRepository, $this->taskRepository, $this->emailTemplateRepository, $this->taskAssigneeRepository, $formRepository);
    }

    public function test_execute_no_related_projects_should_end_of_process()
    {
        $projectId = Uuid::uuid4()->toString();
        $project = $this->getProject($projectId, 'BE-0001', ProjectStatuses::PROJET_CONVENTION_VALIDE, Forms::REFERENTIEL_CONVENTIONS);
        $projectVersion = $this->getProjectPartVersion();

        $this->projectRepository
            ->method('getProjectLastPublishedVersionOfFormPart')
            ->with(
                $this->equalTo($projectId),
                $this->equalTo(FormParts::SAISIE_DES_AF_BUDGET)
            )
            ->willReturn($projectVersion);

        $this->projectRepository
            ->method('getRelatedProjects')
            ->with(
                $this->equalTo($projectId)
            )
            ->willReturn([]);

        $this->logger
            ->method('info')
            ->with($this->equalTo("No related projects for project id {$projectId}. End of process."));

        self::assertTrue($this->flow->execute($project));
    }


    public function test_execute_no_project_answers_of_type_code_du_partenaire_should_end_of_process()
    {
        $projectId = Uuid::uuid4()->toString();
        $project = $this->getProject($projectId, 'BE-0001', ProjectStatuses::PROJET_CONVENTION_VALIDE, Forms::REFERENTIEL_CONVENTIONS);
        $projectVersion = $this->getProjectPartVersion();

        $this->projectRepository
            ->method('getProjectLastPublishedVersionOfFormPart')
            ->with(
                $this->equalTo($projectId),
                $this->equalTo(FormParts::SAISIE_DES_AF_BUDGET)
            )
            ->willReturn($projectVersion);

        $projects = [
            $this->getProject(Uuid::uuid4()->toString(), 'BE-01586', ProjectStatuses::LAB_OK, Forms::REFERENTIEL_PARTENAIRES),
            $this->getProject(Uuid::uuid4()->toString(), 'BE-00478', ProjectStatuses::LAB_NA, Forms::REFERENTIEL_CONVENTIONS),
            $this->getProject(Uuid::uuid4()->toString(), 'BE-08010', ProjectStatuses::PROJET_CONVENTION_VALIDE, Forms::REFERENTIEL_PARTENAIRES)
        ];

        $this->projectRepository
            ->method('getRelatedProjects')
            ->with(
                $this->equalTo($projectId)
            )
            ->willReturn($projects);

        $projectAnswers = [
            $this->getProjectAnswer(Uuid::uuid4()->toString(), '2020-01-01'),
            $this->getProjectAnswer(Uuid::uuid4()->toString(), '2020-02-01'),
            $this->getProjectAnswer(Uuid::uuid4()->toString(), '2020-03-01')
        ];

        $this->projectRepository
            ->method('getProjectAnswers')
            ->with(
                $this->equalTo($project->getId()),
                $this->equalTo($projectVersion->getId())
            )
            ->willReturn($projectAnswers);

        $this->logger
            ->method('info')
            ->with($this->equalTo("Last published version {$projectVersion->getId()} for project {$projectId} does not contain any answer of type " . LABBusinessFlow::ADD_REMOVE_ANSWER . ". End of process."));

        self::assertTrue($this->flow->execute($project));
    }

    public function test_execute_no_project_candidate_to_task_creation_should_end_of_process()
    {
        $projectId = Uuid::uuid4()->toString();
        $project = $this->getProject($projectId, 'BE-0001', ProjectStatuses::PROJET_CONVENTION_VALIDE, Forms::REFERENTIEL_CONVENTIONS);
        $projectVersion = $this->getProjectPartVersion();

        $this->projectRepository
            ->method('getProjectLastPublishedVersionOfFormPart')
            ->with(
                $this->equalTo($projectId),
                $this->equalTo(FormParts::SAISIE_DES_AF_BUDGET)
            )
            ->willReturn($projectVersion);

        $projects = [
            $this->getProject(Uuid::uuid4()->toString(), 'BE-0011', ProjectStatuses::LAB_OK, Forms::REFERENTIEL_PARTENAIRES),
            $this->getProject(Uuid::uuid4()->toString(), 'BE-0121', ProjectStatuses::LAB_NA, Forms::REFERENTIEL_CONVENTIONS),
            $this->getProject(Uuid::uuid4()->toString(), 'BE-0201', ProjectStatuses::PROJET_CONVENTION_VALIDE, Forms::REFERENTIEL_PARTENAIRES)
        ];

        $this->projectRepository
            ->method('getRelatedProjects')
            ->with(
                $this->equalTo($projectId)
            )
            ->willReturn($projects);

        $projectAnswers = [
            $this->getProjectAnswer(LABBusinessFlow::ADD_REMOVE_ANSWER, 'BE-002'),
            $this->getProjectAnswer(Uuid::uuid4()->toString(), '2020-02-01'),
            $this->getProjectAnswer(Uuid::uuid4()->toString(), '2020-03-01')
        ];
        $this->projectRepository
            ->method('getProjectAnswers')
            ->with(
                $this->equalTo($project->getId()),
                $this->equalTo($projectVersion->getId())
            )
            ->willReturn($projectAnswers);

        $this->logger
            ->method('info')
            ->with($this->equalTo("No match found between related project(s) of project id {$project->getId()} and 'Code du partenaire'. End of process."));

        self::assertTrue($this->flow->execute($project));
    }

    public function test_execute_task_already_exists_for_project_should_return_true()
    {
        $projectId = Uuid::uuid4()->toString();
        $project = $this->getProject($projectId, 'BE-0001', ProjectStatuses::PROJET_CONVENTION_VALIDE, Forms::REFERENTIEL_CONVENTIONS);
        $projectVersion = $this->getProjectPartVersion();

        $this->projectRepository
            ->method('getProjectLastPublishedVersionOfFormPart')
            ->with(
                $this->equalTo($projectId),
                $this->equalTo(FormParts::SAISIE_DES_AF_BUDGET)
            )
            ->willReturn($projectVersion);

        $projects = [
            $this->getProject(Uuid::uuid4()->toString(), 'BE-0401', ProjectStatuses::LAB_OK, Forms::REFERENTIEL_PARTENAIRES),
            $this->getProject(Uuid::uuid4()->toString(), 'BE-4501', ProjectStatuses::LAB_NA, Forms::REFERENTIEL_CONVENTIONS),
            $this->getProject(Uuid::uuid4()->toString(), 'BE-0101', ProjectStatuses::PROJET_CONVENTION_VALIDE, Forms::REFERENTIEL_PARTENAIRES)
        ];

        $this->projectRepository
            ->method('getRelatedProjects')
            ->with(
                $this->equalTo($projectId)
            )
            ->willReturn($projects);

        $projectAnswers = [
            $this->getProjectAnswer(LABBusinessFlow::ADD_REMOVE_ANSWER, 'BE-0101'),
            $this->getProjectAnswer(Uuid::uuid4()->toString(), '2020-02-01'),
            $this->getProjectAnswer(Uuid::uuid4()->toString(), '2020-03-01')
        ];
        $this->projectRepository
            ->method('getProjectAnswers')
            ->with(
                $this->equalTo($project->getId()),
                $this->equalTo($projectVersion->getId())
            )
            ->willReturn($projectAnswers);

        $task = new Task(Tasks::LAB, 123, 123, '2019-02-12', $projectId);
        $task->addDescription('Please check this partner: Project/Partner name. Missing documents.');

        $this->taskRepository
            ->method('getProjectTasksByTag')
            ->with(
                $this->equalTo($projectId),
                $this->equalTo(TaskTags::LAB)
            )
            ->willReturn([$task]);

        $this->logger
            ->method('info')
            ->with(
                $this->equalTo("Task '". Tasks::LAB ."' already exists for project id {$projectId} and partner {$projects[0]->getName()}. Continue.")
            );

        self::assertTrue($this->flow->execute($project, $projectVersion));
    }

    public function test_execute_task_does_not_exist_should_create_one_and_return_true()
    {
        $projectId = Uuid::uuid4()->toString();
        $project = $this->getProject($projectId, 'BE-0001', ProjectStatuses::PROJET_CONVENTION_VALIDE, Forms::REFERENTIEL_CONVENTIONS);
        $projectVersion = $this->getProjectPartVersion();

        $this->projectRepository
            ->method('getProjectLastPublishedVersionOfFormPart')
            ->with(
                $this->equalTo($projectId),
                $this->equalTo(FormParts::SAISIE_DES_AF_BUDGET)
            )
            ->willReturn($projectVersion);

        $projects = [
            $this->getProject(Uuid::uuid4()->toString(), 'BE-0401', ProjectStatuses::LAB_OK, Forms::REFERENTIEL_PARTENAIRES),
            $this->getProject(Uuid::uuid4()->toString(), 'BE-4501', ProjectStatuses::LAB_NA, Forms::REFERENTIEL_CONVENTIONS),
            $this->getProject(Uuid::uuid4()->toString(), 'BE-0101', ProjectStatuses::PROJET_CONVENTION_VALIDE, Forms::REFERENTIEL_PARTENAIRES)
        ];

        $this->projectRepository
            ->method('getRelatedProjects')
            ->with(
                $this->equalTo($projectId)
            )
            ->willReturn($projects);

        $projectAnswers = [
            $this->getProjectAnswer(LABBusinessFlow::ADD_REMOVE_ANSWER, 'BE-0101'),
            $this->getProjectAnswer(Uuid::uuid4()->toString(), '2020-02-01'),
            $this->getProjectAnswer(Uuid::uuid4()->toString(), '2020-03-01')
        ];
        $this->projectRepository
            ->method('getProjectAnswers')
            ->with(
                $this->equalTo($project->getId()),
                $this->equalTo($projectVersion->getId())
            )
            ->willReturn($projectAnswers);

        $this->taskRepository
            ->method('getProjectTasksByTag')
            ->with(
                $this->equalTo($projectId),
                $this->equalTo(TaskTags::LAB)
            )
            ->willReturn([]);

        $this->taskAssigneeRepository
            ->method('getUserIdFromTeamWithGivenRole')
            ->with(
                $this->equalTo($projects[2]->getLeaderTeamId()),
                $this->equalTo(AssigneeRoles::RESPONSABLE_DE_PROGRAMME)
            )
            ->willReturn(123);

        $emailTemplate = new EmailTemplate(1, Tasks::LAB, 'Concerne le Partenaire suivant : {partenaireName}.');
        $this->emailTemplateRepository
            ->method('getTemplateById')
            ->with(
                $this->equalTo(EmailTemplates::LAB)
            )
            ->willReturn($emailTemplate);

        $this->taskRepository
            ->method('saveTask')
            ->willReturn(true);


        self::assertTrue($this->flow->execute($project, $projectVersion));
    }

    public function test_execute_task_already_exist_for_other_projects_should_create_one_for_project_and_return_true()
    {
        $projectId = Uuid::uuid4()->toString();
        $project = $this->getProject($projectId, 'BE-0001', ProjectStatuses::PROJET_CONVENTION_VALIDE, Forms::REFERENTIEL_CONVENTIONS);
        $projectVersion = $this->getProjectPartVersion();

        $this->projectRepository
            ->method('getProjectLastPublishedVersionOfFormPart')
            ->with(
                $this->equalTo($projectId),
                $this->equalTo(FormParts::SAISIE_DES_AF_BUDGET)
            )
            ->willReturn($projectVersion);

        $projects = [
            $this->getProject(Uuid::uuid4()->toString(), 'BE-0401', ProjectStatuses::LAB_OK, Forms::REFERENTIEL_PARTENAIRES),
            $this->getProject(Uuid::uuid4()->toString(), 'BE-4501', ProjectStatuses::LAB_NA, Forms::REFERENTIEL_CONVENTIONS),
            $this->getProject(Uuid::uuid4()->toString(), 'BE-0101', ProjectStatuses::PROJET_CONVENTION_VALIDE, Forms::REFERENTIEL_PARTENAIRES)
        ];

        $this->projectRepository
            ->method('getRelatedProjects')
            ->with(
                $this->equalTo($projectId)
            )
            ->willReturn($projects);

        $projectAnswers = [
            $this->getProjectAnswer(LABBusinessFlow::ADD_REMOVE_ANSWER, 'BE-0101'),
            $this->getProjectAnswer(Uuid::uuid4()->toString(), '2020-02-01'),
            $this->getProjectAnswer(Uuid::uuid4()->toString(), '2020-03-01')
        ];
        $this->projectRepository
            ->method('getProjectAnswers')
            ->with(
                $this->equalTo($project->getId()),
                $this->equalTo($projectVersion->getId())
            )
            ->willReturn($projectAnswers);

        $task = new Task(Tasks::LAB, 123, 123, '2019-02-12', $projectId);
        $task->addDescription('Please check this partner: L\'amicale des testeurs. Missing documents.');

        $this->taskRepository
            ->method('getProjectTasksByTag')
            ->with(
                $this->equalTo($projectId),
                $this->equalTo(TaskTags::LAB)
            )
            ->willReturn([$task]);

        $this->taskAssigneeRepository
            ->method('getUserIdFromTeamWithGivenRole')
            ->with(
                $this->equalTo($projects[2]->getLeaderTeamId()),
                $this->equalTo(AssigneeRoles::RESPONSABLE_DE_PROGRAMME)
            )
            ->willReturn(123);

        $emailTemplate = new EmailTemplate(1, Tasks::LAB, 'Concerne le Partenaire suivant : {partenaireName}.');
        $this->emailTemplateRepository
            ->method('getTemplateById')
            ->with(
                $this->equalTo(EmailTemplates::LAB)
            )
            ->willReturn($emailTemplate);

        $this->taskRepository
            ->method('saveTask')
            ->willReturn(true);

        self::assertTrue($this->flow->execute($project, $projectVersion));
    }

    public function test_execute_task_does_not_exist_should_create_one_but_error_in_the_process_should_log_warning_and_return_false()
    {
        $projectId = Uuid::uuid4()->toString();
        $project = $this->getProject($projectId, 'BE-0001', ProjectStatuses::PROJET_CONVENTION_VALIDE, Forms::REFERENTIEL_CONVENTIONS);
        $projectVersion = $this->getProjectPartVersion();

        $this->projectRepository
            ->method('getProjectLastPublishedVersionOfFormPart')
            ->with(
                $this->equalTo($projectId),
                $this->equalTo(FormParts::SAISIE_DES_AF_BUDGET)
            )
            ->willReturn($projectVersion);

        $projects = [
            $this->getProject(Uuid::uuid4()->toString(), 'BE-0401', ProjectStatuses::LAB_OK, Forms::REFERENTIEL_PARTENAIRES),
            $this->getProject(Uuid::uuid4()->toString(), 'BE-4501', ProjectStatuses::LAB_NA, Forms::REFERENTIEL_CONVENTIONS),
            $this->getProject(Uuid::uuid4()->toString(), 'BE-0101', ProjectStatuses::PROJET_CONVENTION_VALIDE, Forms::REFERENTIEL_PARTENAIRES)
        ];

        $this->projectRepository
            ->method('getRelatedProjects')
            ->with(
                $this->equalTo($projectId)
            )
            ->willReturn($projects);

        $projectAnswers = [
            $this->getProjectAnswer(LABBusinessFlow::ADD_REMOVE_ANSWER, 'BE-0101'),
            $this->getProjectAnswer(Uuid::uuid4()->toString(), '2020-02-01'),
            $this->getProjectAnswer(Uuid::uuid4()->toString(), '2020-03-01')
        ];
        $this->projectRepository
            ->method('getProjectAnswers')
            ->with(
                $this->equalTo($project->getId()),
                $this->equalTo($projectVersion->getId())
            )
            ->willReturn($projectAnswers);

        $this->taskRepository
            ->method('getProjectTasksByTag')
            ->with(
                $this->equalTo($projectId),
                $this->equalTo(TaskTags::LAB)
            )
            ->willReturn([]);

        $this->taskAssigneeRepository
            ->method('getUserIdFromTeamWithGivenRole')
            ->with(
                $this->equalTo($projects[2]->getLeaderTeamId()),
                $this->equalTo(AssigneeRoles::RESPONSABLE_DE_PROGRAMME)
            )
            ->willReturn(123);

        $emailTemplate = new EmailTemplate(1, Tasks::LAB, 'Concerne le Partenaire suivant : {partenaireName}.');
        $this->emailTemplateRepository
            ->method('getTemplateById')
            ->with(
                $this->equalTo(EmailTemplates::LAB)
            )
            ->willReturn($emailTemplate);

        $this->taskRepository
            ->method('saveTask')
            ->willReturn(false);

        $this->logger
            ->method('warning')
            ->with(
                $this->equalTo("Task " . Tasks::LAB . " was not created for project id {$projectId}. Please check logs.")
            );


        self::assertFalse($this->flow->execute($project, $projectVersion));
    }

    private function getProjectAnswer(string $type, $value)
    {
        return new ProjectAnswer(Uuid::uuid4()->toString(), $type, $value, Uuid::uuid4()->toString(), FormScreens::SAISIE_DES_AF_BUDGET);
    }

    private function getProject(string $projectId, string $toolReference, string $status, string $form): Project
    {
        return new Project(
            $projectId,
            $toolReference,
            'Project/Partner name',
            $status,
            $form,
            '1235',
            ''
        );
    }

    private function getProjectPartVersion(): ProjectPartVersion
    {
        return new ProjectPartVersion(
            Uuid::uuid4()->toString(),
            FormParts::SAISIE_DES_AF_BUDGET,
            1,
            2,
            (new \DateTime('now - 1 week'))->format('Y-m-d')
            );
    }
}
