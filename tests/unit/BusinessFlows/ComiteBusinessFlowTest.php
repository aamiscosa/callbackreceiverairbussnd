<?php

namespace Optimy\CallbackReceiverLambda\BusinessFlows;


use Optimy\CallbackReceiverLambda\Application\BusinessFlows\ComiteBusinessFlow;
use PHPUnit\Framework\TestCase;
use Optimy\Domain\EmailTemplate;
use Optimy\Domain\FormAnswer;
use Optimy\Domain\Project;
use Optimy\Domain\ProjectAnswer;
use Optimy\Domain\ProjectPartVersion;
use Optimy\Domain\Task;
use Optimy\Infrastructure\Repositories\EmailTemplateRepository;
use Optimy\Infrastructure\Repositories\FormRepository;
use Optimy\Infrastructure\Repositories\ProjectRepository;
use Optimy\Infrastructure\Repositories\TaskAssigneeRepository;
use Optimy\Infrastructure\Repositories\TaskRepository;
use Optimy\CallbackReceiverLambda\Domain\AssigneeRoles;
use Optimy\CallbackReceiverLambda\Domain\EmailTemplates;
use Optimy\CallbackReceiverLambda\Domain\FormParts;
use Optimy\CallbackReceiverLambda\Domain\Forms;
use Optimy\CallbackReceiverLambda\Domain\FormScreens;
use Optimy\CallbackReceiverLambda\Domain\ProjectStatuses;
use Optimy\CallbackReceiverLambda\Domain\Tasks;
use Optimy\CallbackReceiverLambda\Domain\TaskTags;
use Psr\Log\LoggerInterface;
use Ramsey\Uuid\Uuid;

class ComiteBusinessFlowTest extends TestCase
{
    private $logger;
    private $projectRepository;
    private $taskRepository;
    private $emailTemplateRepository;
    private $taskAssigneeRepository;
    private $formRepository;

    private $flow;

    // Question id of the question "Exception de territorialité"
    private const EXCEPTION_TERRITORIALITE_Q = 'b3a6cc9e-8ead-5300-a456-f09713d0b021';
    // Answer id of the answer "NON" to the question "Exception de territorialité"
    private const EXCEPTION_TERRITORIALITE_A = 'cf1455d8-9760-5696-b60b-a823bdd87588';
    // Add remove section id "Réunions de suivi"
    private const ADD_REMOVE = 'efb06a5d-2037-5520-8e06-8404f4310488';

    public function setup()
    {
        $this->logger = $this->createMock(LoggerInterface::class);
        $this->projectRepository = $this->createMock(ProjectRepository::class);
        $this->taskRepository = $this->createMock(TaskRepository::class);
        $this->emailTemplateRepository = $this->createMock(EmailTemplateRepository::class);
        $this->taskAssigneeRepository = $this->createMock(TaskAssigneeRepository::class);
        $this->formRepository = $this->createMock(FormRepository::class);

        $this->flow = new ComiteBusinessFlow($this->logger, $this->projectRepository, $this->taskRepository, $this->emailTemplateRepository, $this->taskAssigneeRepository, $this->formRepository);
    }

    public function test_execute_no_published_part_of_infos_juridiques_should_end_process()
    {
        $projectId = Uuid::uuid4()->toString();
        $project = $this->getProject($projectId, 'BE-0001', ProjectStatuses::PROJET_SIGNE_DES_PARTIES, Forms::REFERENTIEL_CONVENTIONS);

        $this->projectRepository
            ->expects($this->at(0))
            ->method('getProjectLastPublishedVersionOfFormPart')
            ->with(
                $this->equalTo($projectId),
                $this->equalTo(FormParts::INFORMATIONS_JURIDIQUES)
            )
            ->willReturn(null);

        $this->logger
            ->method('info')
            ->with($this->equalTo("No last published version of form part ". FormParts::INFORMATIONS_JURIDIQUES ." for project id {$projectId}. End of process."));

        self::assertTrue($this->flow->execute($project));
    }

    public function test_execute_exception_territoriale_is_non_should_end_process()
    {
        $projectId = Uuid::uuid4()->toString();
        $project = $this->getProject($projectId, 'BE-0001', ProjectStatuses::PROJET_SIGNE_DES_PARTIES, Forms::REFERENTIEL_CONVENTIONS);
        $projectVersion = $this->getProjectPartVersion();

        $this->projectRepository
            ->expects($this->at(0))
            ->method('getProjectLastPublishedVersionOfFormPart')
            ->with(
                $this->equalTo($projectId),
                $this->equalTo(FormParts::INFORMATIONS_JURIDIQUES)
            )
            ->willReturn($projectVersion);

        $answer =  new ProjectAnswer(self::EXCEPTION_TERRITORIALITE_A, 'Non', '1', self::EXCEPTION_TERRITORIALITE_Q, FormScreens::SAISIE_DES_CALENDRIERS);
        $this->projectRepository
            ->expects($this->at(1))
            ->method('getProjectAnswers')
            ->with(
                $this->equalTo($projectId),
                $this->equalTo($projectVersion->getId()),
                $this->equalTo(self::EXCEPTION_TERRITORIALITE_Q)
            )
            ->willReturn([$answer]);

        $this->logger
            ->method('info')
            ->with($this->equalTo("Question 'Exception de territorialité' is NON. End of process."));

        self::assertTrue($this->flow->execute($project));
    }

    public function test_execute_no_last_version_saisie_des_calendriers_should_end_process()
    {
        $projectId = Uuid::uuid4()->toString();
        $project = $this->getProject($projectId, 'BE-0001', ProjectStatuses::PROJET_SIGNE_DES_PARTIES, Forms::REFERENTIEL_CONVENTIONS);
        $projectVersion = $this->getProjectPartVersion();

        $this->projectRepository
            ->expects($this->at(0))
            ->method('getProjectLastPublishedVersionOfFormPart')
            ->with(
                $this->equalTo($projectId),
                $this->equalTo(FormParts::INFORMATIONS_JURIDIQUES)
            )
            ->willReturn($projectVersion);

        $answer =  new ProjectAnswer(Uuid::uuid4()->toString(), 'Non', '1', self::EXCEPTION_TERRITORIALITE_Q, FormScreens::SAISIE_DES_CALENDRIERS);
        $this->projectRepository
            ->expects($this->at(1))
            ->method('getProjectAnswers')
            ->with(
                $this->equalTo($projectId),
                $this->equalTo($projectVersion->getId()),
                $this->equalTo(self::EXCEPTION_TERRITORIALITE_Q)
            )
            ->willReturn([$answer]);

        $this->projectRepository
            ->expects($this->at(2))
            ->method('getProjectLastPublishedVersionOfFormPart')
            ->with(
                $this->equalTo($projectId),
                $this->equalTo(FormParts::SAISIE_DES_CALENDRIERS)
            )
            ->willReturn(null);

        $this->logger
            ->method('info')
            ->with($this->equalTo("No last published version of form part ". FormParts::SAISIE_DES_CALENDRIERS ." for project id {$projectId}. End of process."));

        self::assertTrue($this->flow->execute($project));
    }

    public function test_execute_no_formScreen_answers_in_last_version_saisie_des_calendriers_should_end_process()
    {
        $projectId = Uuid::uuid4()->toString();
        $project = $this->getProject($projectId, 'BE-0001', ProjectStatuses::PROJET_SIGNE_DES_PARTIES, Forms::REFERENTIEL_CONVENTIONS);
        $projectVersion = $this->getProjectPartVersion();

        $this->projectRepository
            ->expects($this->at(0))
            ->method('getProjectLastPublishedVersionOfFormPart')
            ->with(
                $this->equalTo($projectId),
                $this->equalTo(FormParts::INFORMATIONS_JURIDIQUES)
            )
            ->willReturn($projectVersion);

        $answer =  new ProjectAnswer(Uuid::uuid4()->toString(), 'Non', '1', self::EXCEPTION_TERRITORIALITE_Q, FormScreens::SAISIE_DES_CALENDRIERS);
        $this->projectRepository
            ->expects($this->at(1))
            ->method('getProjectAnswers')
            ->with(
                $this->equalTo($projectId),
                $this->equalTo($projectVersion->getId()),
                $this->equalTo(self::EXCEPTION_TERRITORIALITE_Q)
            )
            ->willReturn([$answer]);

        $version = $this->getProjectPartVersion();
        $this->projectRepository
            ->expects($this->at(2))
            ->method('getProjectLastPublishedVersionOfFormPart')
            ->with(
                $this->equalTo($projectId),
                $this->equalTo(FormParts::SAISIE_DES_CALENDRIERS)
            )
            ->willReturn($version);

        $this->projectRepository
            ->expects($this->at(3))
            ->method('getProjectAnswers')
            ->with(
                $this->equalTo($projectId),
                $this->equalTo($version->getId())
            )
            ->willReturn([]);

        $this->logger
            ->method('info')
            ->with($this->equalTo("Last published version {$version->getId()} for project {$projectId} does not have any answers published in screen ". FormScreens::SAISIE_DES_CALENDRIERS .". End of process."));

        self::assertTrue($this->flow->execute($project));
    }

    public function test_execute_first_date_has_already_completed_task_second_date_needs_new_task_should_return_true()
    {
        $projectId = Uuid::uuid4()->toString();
        $project = $this->getProject($projectId, 'BE-0001', ProjectStatuses::PROJET_SIGNE_DES_PARTIES, Forms::REFERENTIEL_CONVENTIONS);
        $projectVersion = $this->getProjectPartVersion();

        $this->projectRepository
            ->expects($this->at(0))
            ->method('getProjectLastPublishedVersionOfFormPart')
            ->with(
                $this->equalTo($projectId),
                $this->equalTo(FormParts::INFORMATIONS_JURIDIQUES)
            )
            ->willReturn($projectVersion);

        $answer =  new ProjectAnswer(Uuid::uuid4()->toString(), 'Non', '1', self::EXCEPTION_TERRITORIALITE_Q, FormScreens::SAISIE_DES_CALENDRIERS);
        $this->projectRepository
            ->expects($this->at(1))
            ->method('getProjectAnswers')
            ->with(
                $this->equalTo($projectId),
                $this->equalTo($projectVersion->getId()),
                $this->equalTo(self::EXCEPTION_TERRITORIALITE_Q)
            )
            ->willReturn([$answer]);

        $version = $this->getProjectPartVersion();
        $this->projectRepository
            ->expects($this->at(2))
            ->method('getProjectLastPublishedVersionOfFormPart')
            ->with(
                $this->equalTo($projectId),
                $this->equalTo(FormParts::SAISIE_DES_CALENDRIERS)
            )
            ->willReturn($version);

        // Create Project Answers
        $projectAnswers = [];
        $meeting1 = new ProjectAnswer(Uuid::uuid4(), 'Meeting number', '1', Uuid::uuid4()->toString(), FormScreens::SAISIE_DES_CALENDRIERS);
        $date1 = new ProjectAnswer(Uuid::uuid4(), 'Date prévisionnelle', '2022-01-12', Uuid::uuid4()->toString(), FormScreens::SAISIE_DES_CALENDRIERS);
        $meeting2 = new ProjectAnswer(Uuid::uuid4(), 'Meeting number', '2', Uuid::uuid4()->toString(), FormScreens::SAISIE_DES_CALENDRIERS);
        $date2 = new ProjectAnswer(Uuid::uuid4(), 'Date prévisionnelle', '2022-05-12', Uuid::uuid4()->toString(), FormScreens::SAISIE_DES_CALENDRIERS);

        array_push($projectAnswers, $meeting1, $date1, $meeting2, $date2);

        $this->projectRepository
            ->expects($this->at(3))
            ->method('getProjectAnswers')
            ->with(
                $this->equalTo($projectId),
                $this->equalTo($version->getId())
            )
            ->willReturn($projectAnswers);

        // Create Add removes Form Answers
        $addRemoveAnswers = [];
        $addRemove1 = [];
        array_push(
            $addRemove1,
            new FormAnswer($meeting1->getId(), null, 'Meeting number'),
            new FormAnswer($date1->getId(), null, 'Date prévisionnelle')
        );

        $addRemove2 = [];
        array_push(
            $addRemove2,
            new FormAnswer($meeting1->getId(), null, 'Meeting number'),
            new FormAnswer($date2->getId(), null, 'Date prévisionnelle')
        );

        array_push($addRemoveAnswers, $addRemove1, $addRemove2);

        $this->formRepository
            ->expects($this->once())
            ->method('getAddRemoveAnswersStructure')
            ->with(
                $this->equalTo(Forms::REFERENTIEL_CONVENTIONS),
                $this->equalTo(FormParts::SAISIE_DES_CALENDRIERS),
                $this->equalTo(FormScreens::SAISIE_DES_CALENDRIERS),
                $this->equalTo(self::ADD_REMOVE)
            )
            ->willReturn($addRemoveAnswers);

        $task = new Task(Tasks::COMITE, 123, 123, $date1->getValue(), $projectId);
        $task->wasCompletedOn($date1->getValue());
        $this->taskRepository
            ->expects($this->at(0))
            ->method('getProjectTasksByTag')
            ->with(
                $this->equalTo($projectId),
                $this->equalTo(TaskTags::COMITE . $date1->getId())
            )
            ->willReturn([$task]);

        $this->logger
            ->method('info')
            ->with($this->equalTo("Task '". $task->getName() ."' already exists for project id {$project->getId()} and date answer id {$date1->getId()}. Continue."));

        $this->taskRepository
            ->expects($this->at(1))
            ->method('getProjectTasksByTag')
            ->with(
                $this->equalTo($projectId),
                $this->equalTo(TaskTags::COMITE . $date2->getId())
            )
            ->willReturn([]);

        $this->taskAssigneeRepository
            ->method('getUserIdFromTeamWithGivenRole')
            ->with(
                $this->equalTo($project->getLeaderTeamId()),
                $this->equalTo(AssigneeRoles::RESPONSABLE_DE_PROGRAMME)
            )
            ->willReturn(1548);

        $emailTemplate = new EmailTemplate(1, EmailTemplates::COMITE_NON_TENU, 'Comité non tenu');
        $this->emailTemplateRepository
            ->method('getTemplateById')
            ->with(
                $this->equalTo(EmailTemplates::COMITE_NON_TENU)
            )
            ->willReturn($emailTemplate);

        $this->taskRepository
            ->expects($this->at(2))
            ->method('saveTask')
            ->willReturn(true);

        self::assertTrue($this->flow->execute($project));
    }

    public function test_execute_first_date_has_already_completed_task_second_date_needs_to_be_updated_should_return_true()
    {
        $projectId = Uuid::uuid4()->toString();
        $project = $this->getProject($projectId, 'BE-0001', ProjectStatuses::PROJET_SIGNE_DES_PARTIES, Forms::REFERENTIEL_CONVENTIONS);
        $projectVersion = $this->getProjectPartVersion();

        $this->projectRepository
            ->expects($this->at(0))
            ->method('getProjectLastPublishedVersionOfFormPart')
            ->with(
                $this->equalTo($projectId),
                $this->equalTo(FormParts::INFORMATIONS_JURIDIQUES)
            )
            ->willReturn($projectVersion);

        $answer =  new ProjectAnswer(Uuid::uuid4()->toString(), 'Non', '1', self::EXCEPTION_TERRITORIALITE_Q, FormScreens::SAISIE_DES_CALENDRIERS);
        $this->projectRepository
            ->expects($this->at(1))
            ->method('getProjectAnswers')
            ->with(
                $this->equalTo($projectId),
                $this->equalTo($projectVersion->getId()),
                $this->equalTo(self::EXCEPTION_TERRITORIALITE_Q)
            )
            ->willReturn([$answer]);

        $version = $this->getProjectPartVersion();
        $this->projectRepository
            ->expects($this->at(2))
            ->method('getProjectLastPublishedVersionOfFormPart')
            ->with(
                $this->equalTo($projectId),
                $this->equalTo(FormParts::SAISIE_DES_CALENDRIERS)
            )
            ->willReturn($version);

        // Create Project Answers
        $projectAnswers = [];
        $meeting1 = new ProjectAnswer(Uuid::uuid4(), 'Meeting number', '1', Uuid::uuid4()->toString(), FormScreens::SAISIE_DES_CALENDRIERS);
        $date1 = new ProjectAnswer(Uuid::uuid4(), 'Date prévisionnelle', '2022-01-12', Uuid::uuid4()->toString(), FormScreens::SAISIE_DES_CALENDRIERS);
        $meeting2 = new ProjectAnswer(Uuid::uuid4(), 'Meeting number', '2', Uuid::uuid4()->toString(), FormScreens::SAISIE_DES_CALENDRIERS);
        $date2 = new ProjectAnswer(Uuid::uuid4(), 'Date prévisionnelle', '2022-05-12', Uuid::uuid4()->toString(), FormScreens::SAISIE_DES_CALENDRIERS);

        array_push($projectAnswers, $meeting1, $date1, $meeting2, $date2);

        $this->projectRepository
            ->expects($this->at(3))
            ->method('getProjectAnswers')
            ->with(
                $this->equalTo($projectId),
                $this->equalTo($version->getId())
            )
            ->willReturn($projectAnswers);

        // Create Add removes Form Answers
        $addRemoveAnswers = [];
        $addRemove1 = [];
        array_push(
            $addRemove1,
            new FormAnswer($meeting1->getId(), null, 'Meeting number'),
            new FormAnswer($date1->getId(), null, 'Date prévisionnelle')
        );

        $addRemove2 = [];
        array_push(
            $addRemove2,
            new FormAnswer($meeting1->getId(), null, 'Meeting number'),
            new FormAnswer($date2->getId(), null, 'Date prévisionnelle')
        );

        array_push($addRemoveAnswers, $addRemove1, $addRemove2);

        $this->formRepository
            ->expects($this->once())
            ->method('getAddRemoveAnswersStructure')
            ->with(
                $this->equalTo(Forms::REFERENTIEL_CONVENTIONS),
                $this->equalTo(FormParts::SAISIE_DES_CALENDRIERS),
                $this->equalTo(FormScreens::SAISIE_DES_CALENDRIERS),
                $this->equalTo(self::ADD_REMOVE)
            )
            ->willReturn($addRemoveAnswers);

        $task = new Task(Tasks::COMITE, 123, 123, $date1->getValue(), $projectId);
        $task->wasCompletedOn($date1->getValue());
        $this->taskRepository
            ->expects($this->at(0))
            ->method('getProjectTasksByTag')
            ->with(
                $this->equalTo($projectId),
                $this->equalTo(TaskTags::COMITE . $date1->getId())
            )
            ->willReturn([$task]);

        $this->logger
            ->expects($this->at(0))
            ->method('info')
            ->with($this->equalTo("Task '". $task->getName() ."' already exists for project id {$project->getId()} and date answer id {$date1->getId()}. Continue."));

        $task2 = new Task(Tasks::COMITE, 123, 123, '2050-03-12', $projectId);
        $this->taskRepository
            ->expects($this->at(1))
            ->method('getProjectTasksByTag')
            ->with(
                $this->equalTo($projectId),
                $this->equalTo(TaskTags::COMITE . $date2->getId())
            )
            ->willReturn([$task2]);

        $this->taskAssigneeRepository
            ->method('getUserIdFromTeamWithGivenRole')
            ->with(
                $this->equalTo($project->getLeaderTeamId()),
                $this->equalTo(AssigneeRoles::RESPONSABLE_DE_PROGRAMME)
            )
            ->willReturn(1548);

        $this->logger
            ->expects($this->at(1))
            ->method('info')
            ->with(
                $this->equalTo("Updating due date (new value: {$date2->getValue()}) in task '". $task2->getName() ."' for project id {$project->getId()} and date answer id {$date2->getId()}.")
            );

        $this->taskRepository
            ->expects($this->at(2))
            ->method('saveTask')
            ->willReturn(true);

        self::assertTrue($this->flow->execute($project));
    }

    private function getProject(string $projectId, string $toolReference, string $status, string $form): Project
    {
        return new Project(
            $projectId,
            $toolReference,
            'Project/Partner name',
            $status,
            $form,
            '1235',
            ''
        );
    }

    private function getProjectPartVersion(): ProjectPartVersion
    {
        return new ProjectPartVersion(
            Uuid::uuid4()->toString(),
            FormParts::SAISIE_DES_CALENDRIERS,
            1,
            2,
            (new \DateTime('now - 1 week'))->format('Y-m-d')
        );
    }
}
