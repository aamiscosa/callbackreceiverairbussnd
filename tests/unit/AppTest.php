<?php

namespace Optimy\CallbackReceiverLambda;


use Optimy\Infrastructure\Events\EventTypes;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Ramsey\Uuid\Uuid;

class AppTest extends TestCase
{
    private $app;
    private $logger;
    private $config;

    public function setUp()
    {
        $this->logger = $this->createMock(LoggerInterface::class);
        $this->config = [
            'apiEndpoint' => 'http://www.optimy.com/api/v1.3/',
            'apiKey' => 'apiKey',
            'apiSecret' => 'apiSecret'
        ];
        $this->app = new App($this->logger, $this->config);
    }

    public function test_run_with_no_api_key_should_log_error_of_type_wrong_payload()
    {
        $payload = [
            'event_name' => EventTypes::PROJECT_PART_VERSION_SUBMITTED
        ];

        $this->logger
            ->expects($this->at(0))
            ->method('debug')
            ->with(
                $this->equalTo('Processing request'),
                $payload
                );

        $this->logger
            ->expects($this->at(1))
            ->method('error')
            ->with(
                $this->equalTo('Wrong payload received: Missing api_key'),
                $payload
            );

        $result = $this->app->run($payload);
        $this->assertEquals(App::RETURN_ERROR, $result);
    }

    public function test_run_with_no_event_name_should_log_error_of_type_wrong_payload()
    {
        $payload = [
            'api_key' => 'apiKey'
        ];

        $this->logger
            ->expects($this->at(0))
            ->method('debug')
            ->with(
                $this->equalTo('Processing request'),
                $payload
            );

        $this->logger
            ->expects($this->at(1))
            ->method('error')
            ->with(
                $this->equalTo('Wrong payload received: Missing event_name'),
                $payload
            );

        $result = $this->app->run($payload);
        $this->assertEquals(App::RETURN_ERROR, $result);
    }

    public function test_run_with_invalid_api_key_should_log_error_of_type_api_key_mismatch()
    {
        $payload = [
            'api_key' => 'invalid_key',
            'event_name' => EventTypes::PROJECT_PART_VERSION_SUBMITTED
        ];

        $this->logger
            ->expects($this->at(0))
            ->method('debug')
            ->with(
                $this->equalTo('Processing request'),
                $payload
            );

        $this->logger
            ->expects($this->at(1))
            ->method('error')
            ->with(
                $this->equalTo('Wrong payload received: Invalid api_key')
            );

        $result = $this->app->run($payload);
        $this->assertEquals(App::RETURN_ERROR, $result);
    }

    public function test_run_with_non_actionable_event_name_should_log_info_and_end_process()
    {
        $payload = [
            'api_key' => 'apiKey',
            'event_name' => 'non.actionable.event'
        ];

        $this->logger
            ->expects($this->at(0))
            ->method('debug')
            ->with(
                $this->equalTo('Processing request'),
                $payload
            );

        $this->logger
            ->expects($this->at(1))
            ->method('info')
            ->with(
                $this->equalTo("The following value: {$payload['event_name']} is not actionable by this service. End of process.")
            );

        $result = $this->app->run($payload);
        $this->assertEquals(App::RETURN_SUCCESS, $result);
    }
}
