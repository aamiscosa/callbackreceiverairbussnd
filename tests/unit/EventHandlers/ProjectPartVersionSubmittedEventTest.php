<?php

namespace Optimy\CallbackReceiverLambda\EventHandlers;


use Optimy\Infrastructure\Events\EventTypes;
use Optimy\Infrastructure\Events\ObjectTypes;
use Optimy\Infrastructure\Events\ProjectPartVersionSubmittedEvent;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;

class ProjectPartVersionSubmittedEventTest extends TestCase
{
    private $event;
    private $projectPartVersionId;
    private $objectId;

    public function setUp()
    {
        $this->objectId = Uuid::uuid4()->toString();
        $this->projectPartVersionId = Uuid::uuid4()->toString();
        $eventData = [
            'project_part_version_id' => $this->projectPartVersionId
        ];

        $this->event = new ProjectPartVersionSubmittedEvent($this->objectId, $eventData);
    }

    public function test_getProjectStatusId()
    {
        self::assertEquals($this->projectPartVersionId, $this->event->getProjectPartVersionId());
    }

    public function test_getObjectId()
    {
        self::assertEquals($this->objectId, $this->event->getObjectId());
    }

    public function test_getType()
    {
        self::assertEquals(EventTypes::PROJECT_PART_VERSION_SUBMITTED, $this->event->getType());
    }

    public function test_getObjectType()
    {
        self::assertEquals(ObjectTypes::PROJECT, $this->event->getObjectType());
    }

    public function test_toArray()
    {
        $expected = [
            'type' => EventTypes::PROJECT_PART_VERSION_SUBMITTED,
            ObjectTypes::PROJECT => $this->objectId,
            'project_part_version_id' => $this->projectPartVersionId
        ];

        self::assertEquals($expected, $this->event->toArray());
    }
}
