<?php

namespace Optimy\CallbackReceiverLambda\EventHandlers;


use Optimy\CallbackReceiverLambda\Application\BusinessFlows\BusinessFlowFactory;
use Optimy\CallbackReceiverLambda\Application\BusinessFlows\BusinessFlows;
use Optimy\CallbackReceiverLambda\Application\BusinessFlows\LABBusinessFlow;
use Optimy\CallbackReceiverLambda\Infrastructure\EventHandlers\ProjectStatusChangedEventHandler;
use Optimy\Infrastructure\Events\ProjectStatusChangedEvent;
use Optimy\Domain\Project;
use Optimy\Domain\ProjectPartVersion;
use Optimy\Infrastructure\Repositories\EmailTemplateRepository;
use Optimy\Infrastructure\Repositories\ProjectRepository;
use Optimy\Infrastructure\Repositories\TaskRepository;
use Optimy\CallbackReceiverLambda\Domain\FormParts;
use Optimy\CallbackReceiverLambda\Domain\Forms;
use Optimy\CallbackReceiverLambda\Domain\ProjectStatuses;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Ramsey\Uuid\Uuid;

class ProjectStatusChangedEventHandlerTest extends TestCase
{
    private $eventHandler;
    private $logger;
    private $businessFlowFactory;
    private $projectRepository;

    public function setUp()
    {
        $this->projectRepository = $this->createMock(ProjectRepository::class);
        $this->logger = $this->createMock(LoggerInterface::class);

        $this->businessFlowFactory = $this->createMock(BusinessFlowFactory::class);
        $this->eventHandler = new ProjectStatusChangedEventHandler($this->logger, $this->businessFlowFactory, $this->projectRepository);
    }

    /**
     * @expectedException \Optimy\Domain\EndOfProcessException
     */
    public function test_status_is_not_actionable_should_end_process()
    {
        $event = new ProjectStatusChangedEvent(Uuid::uuid4()->toString(), ['project_status_id' => Uuid::uuid4()->toString()]);
        $this->projectRepository
            ->expects($this->at(0))
            ->method('getProject')
            ->with(
                $this->equalTo($event->getObjectId())
            )
            ->willReturn(new Project($event->getObjectId(), 1010, 'Project for curing cancer', $event->getProjectStatusId(), Forms::REFERENTIEL_PARTENAIRES, '12',''));

        $this->projectRepository
            ->expects($this->at(0))
            ->method('getProject')
            ->with(
                $this->equalTo($event->getObjectId())
            )
            ->willReturn(new ProjectPartVersion(Uuid::uuid4()->toString(), Uuid::uuid4()->toString(), 2, 2, '2018-05-31'));

        $this->logger
            ->expects($this->at(0))
            ->method('info')
            ->with(
                $this->equalTo("Executing {$event->getType()} with " . join(',', $event->toArray()))
            );

        $this->eventHandler->execute($event);
    }

    public function test_status_is_projet_convention_valide_project_is_null_should_return_false()
    {
        $event = new ProjectStatusChangedEvent(Uuid::uuid4()->toString(), ['project_status_id' => ProjectStatuses::PROJET_CONVENTION_VALIDE]);

        $this->projectRepository
            ->expects($this->once())
            ->method('getProject')
            ->with(
                $this->equalTo($event->getObjectId())
            )
            ->willReturn(null);

        $this->logger
            ->expects($this->at(1))
            ->method('error')
            ->with(
                $this->equalTo("Failed: {$event->getType()} with " . join(',', $event->toArray())),
                $this->equalTo(['Project does not exist or service was unreachable.'])
            );

        self::assertFalse($this->eventHandler->execute($event));
    }

    /**
     * @expectedException \Optimy\Domain\EndOfProcessException
     */
    public function test_status_is_projet_contention_valide_project_has_wrong_form_should_end_process()
    {
        $event = new ProjectStatusChangedEvent(Uuid::uuid4()->toString(), ['project_status_id' => ProjectStatuses::PROJET_CONVENTION_VALIDE]);
        $project = new Project(Uuid::uuid4()->toString(), 1, 'Project name', $event->getProjectStatusId(), Uuid::uuid4()->toString(), '1','');

        $this->projectRepository
            ->expects($this->once())
            ->method('getProject')
            ->with(
                $this->equalTo($event->getObjectId())
            )
            ->willReturn($project);

        $this->eventHandler->execute($event);
    }

//    /**
//     * @expectedException \Optimy\Domain\EndOfProcessException
//     */
//    public function test_status_is_projet_contention_valide_project_has_no_last_published_version_should_end_process()
//    {
//        $event = new ProjectStatusChangedEvent(Uuid::uuid4()->toString(), ['project_status_id' => ProjectStatuses::PROJET_CONVENTION_VALIDE]);
//        $project = new Project(Uuid::uuid4()->toString(), 1, 'Project name', $event->getProjectStatusId(), Forms::REFERENTIEL_CONVENTIONS, 1);
//
//        $this->projectRepository
//            ->expects($this->at(0))
//            ->method('getProject')
//            ->with(
//                $this->equalTo($event->getObjectId())
//            )
//            ->willReturn($project);
//
//        $this->projectRepository
//            ->expects($this->at(1))
//            ->method('getProjectLastPublishedVersionOfFormPart')
//            ->with(
//                $this->equalTo($event->getObjectId()),
//                FormParts::SAISIE_DES_AF_BUDGET
//            )
//            ->willReturn(null);
//
//        $this->eventHandler->execute($event);
//    }

    public function test_status_is_project_convention_valide_should_create_LAB_business_flow()
    {
        $event = new ProjectStatusChangedEvent(Uuid::uuid4()->toString(), ['project_status_id' => ProjectStatuses::PROJET_CONVENTION_VALIDE]);
        $project = new Project(Uuid::uuid4()->toString(), 1, 'Project name', $event->getProjectStatusId(), Forms::REFERENTIEL_CONVENTIONS, '1','');

        $this->projectRepository
            ->expects($this->at(0))
            ->method('getProject')
            ->with(
                $this->equalTo($event->getObjectId())
            )
            ->willReturn($project);

        $businessFlowMock = $this->createMock(LABBusinessFlow::class);
        $this->businessFlowFactory
            ->expects($this->once())
            ->method('create')
            ->with(
                $this->equalTo(BusinessFlows::LAB_TASK)
            )
            ->willReturn($businessFlowMock);

        $businessFlowMock
            ->method('execute')
            ->willReturn(true);

        $this->eventHandler->execute($event);
    }
}
