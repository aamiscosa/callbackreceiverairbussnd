<?php

namespace Optimy\CallbackReceiverLambda\EventHandlers;


use Optimy\Infrastructure\Events\EventTypes;
use Optimy\Infrastructure\Events\ObjectTypes;
use Optimy\Infrastructure\Events\ProjectStatusChangedEvent;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;

class ProjectStatusChangedEventTest extends TestCase
{
    private $event;
    private $projectStatusId;
    private $objectId;

    public function setUp()
    {
        $this->objectId = Uuid::uuid4()->toString();
        $this->projectStatusId = Uuid::uuid4()->toString();
        $eventData = [
            'project_status_id' => $this->projectStatusId
        ];

        $this->event = new ProjectStatusChangedEvent($this->objectId, $eventData);
    }

    public function test_getProjectStatusId()
    {
        self::assertEquals($this->projectStatusId, $this->event->getProjectStatusId());
    }

    public function test_getObjectId()
    {
        self::assertEquals($this->objectId, $this->event->getObjectId());
    }

    public function test_getType()
    {
        self::assertEquals(EventTypes::PROJECT_STATUS_CHANGED, $this->event->getType());
    }

    public function test_getObjectType()
    {
        self::assertEquals(ObjectTypes::PROJECT, $this->event->getObjectType());
    }

    public function test_toArray()
    {
        $expected = [
            'type' => EventTypes::PROJECT_STATUS_CHANGED,
            ObjectTypes::PROJECT => $this->objectId,
            'project_status_id' => $this->projectStatusId
        ];

        self::assertEquals($expected, $this->event->toArray());
    }
}
