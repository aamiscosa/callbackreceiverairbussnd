<?php
/**
 * Loading dependencies
 */
require_once(__DIR__ . '/vendor/autoload.php');

/**
 * Get Config
 */
$config = require_once(__DIR__ . '/config.php');

use Monolog\Logger;
use Monolog\Handler\StreamHandler;


/**
 * Setting up dependencies for injection
 */
$logger = (new Logger('php'))
	->pushHandler(new StreamHandler('php://stdout', Logger::DEBUG))
	->pushHandler(new StreamHandler('php://stderr', Logger::ERROR, false)); // false = no bubbling to our stdout handler


// running the app
try {
    // reading/decoding our input
    //$input = file_get_contents('php://stdin');
    $input = file_get_contents('php://input');

    $logger->info($input);
    $json = json_decode($input, true);
    if ($json === null) {
        $logger->error('Could not JSON decode input [' . $input . ']');
        exit(9);
    }

	$app = new \Optimy\CallbackReceiverLambda\App($logger, $config);
	exit($app->run($json));	
} catch (Exception $ex) {
	$logger->error('Exception occurred [' . $ex->getMessage() . ']', ['exception' => $ex]);
}
