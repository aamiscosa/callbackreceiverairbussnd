FROM composer:latest AS vendoring-image

WORKDIR /tmp
COPY composer.* /tmp/
RUN composer install --no-dev --no-interaction --optimize-autoloader --ignore-platform-reqs

# ---

FROM amazonlinux:2017.03.1.20170812

WORKDIR /root

ARG PHP_VERSION=7.2.2

RUN sed -i 's;^releasever.*;releasever=2017.03;;' /etc/yum.conf \
	&& yum clean all \
	&& yum -y downgrade openssl-1.0.1k

RUN yum install gcc gcc-c++ libxml2-devel wget tar bzip2 zip libzip-devel openssl-devel -y \
	&& mkdir php-7-bin \
	&& wget http://de2.php.net/get/php-${PHP_VERSION}.tar.bz2/from/this/mirror \
	&& tar -jxvf mirror \
	&& rm mirror \
	&& cd php-${PHP_VERSION} \ 
	&& ./configure --prefix=/root/php-7-bin/ --with-openssl --without-pear --disable-cgi --enable-mbstring --enable-zip \
	&& make -j2 \
	&& make install \
	&& cd /root \
	&& mv php-7-bin/bin/php . \
	&& rm -rf php-7-bin \
	&& rm -rf php-${PHP_VERSION} \
	&& yum erase gcc gcc-c++ libxml2-devel wget tar bzip2 libzip-devel openssl-devel -y \
	&& yum autoremove -y \
	&& yum clean all -y
	
COPY --from=vendoring-image /tmp/vendor /tmp/vendor
	
COPY src/builder/entrypoint.sh /tmp/entrypoint.sh
RUN chmod +x /tmp/entrypoint.sh

ENTRYPOINT ["/tmp/entrypoint.sh"]