#!/bin/bash
mkdir -p /tmp/dist/bin
cd /tmp/dist

cp -R /root/php bin
cp -R /build-src/data .

mkdir src
cp -R /build-src/src/app src
cp -R /build-src/optimy optimy

cp -R /tmp/vendor .
cp /build-src/src/lambda/php.js .

cp /build-src/run.php .
cp /build-src/config.php .

DATE=`date +%Y%m%d%H%M%S`
zip -9rv /build-src/dist/lambda-${DATE}.zip *

echo
echo Your Lambda package is available at: "dist/lambda-${DATE}.zip"
echo 