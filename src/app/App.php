<?php
//declare(strict_types=1);
namespace Optimy\CallbackReceiverLambda;

use Optimy\Application\ProjectAnswerService;
use Optimy\CallbackReceiverLambda\Application\BusinessFlows\BusinessFlowFactory;
use Optimy\CallbackReceiverLambda\Application\LoggerService;
use Optimy\CallbackReceiverLambda\Application\TaskService;
use Optimy\Infrastructure\Events\EventTypes;
use Optimy\Infrastructure\Events\ProjectPartVersionSubmittedEvent;
use Optimy\CallbackReceiverLambda\Infrastructure\EventHandlers\ProjectPartVersionSubmittedEventHandler;
use Optimy\Infrastructure\Events\ProjectStatusChangedEvent;
use Optimy\CallbackReceiverLambda\Infrastructure\EventHandlers\ProjectStatusChangedEventHandler;
use Optimy\Infrastructure\Repositories\Client;
use Optimy\Infrastructure\Repositories\EmailTemplateRepository;
use Optimy\Infrastructure\Repositories\FormRepository;
use Optimy\Infrastructure\Repositories\ProjectRepository;
use Optimy\Infrastructure\Repositories\TaskAssigneeRepository;
use Optimy\Infrastructure\Repositories\TaskRepository;
use Optimy\Domain\EndOfProcessException;
use Optimy\CallbackReceiverLambda\Domain\ProjectStatuses;
use Psr\Log\LoggerInterface;


/**
 * Configs:
 * - apiEndpoint: Optimy API endpoint (eg: https://api.optimytool.com/v1.3/)
 * - apiKey: Optimy API access key
 * - apiSecret: Optimy API secret key
 * - apiTimeout: Client timeout in seconds [default = 30]
 */
class App {

    /**
     * @var LoggerInterface 
     */
    private $logger;

    /**
     * @var ProjectRepository
     */
    private $projectRepository;

    /**
     * @var TaskRepository
     */
    private $taskRepository;

    /**
     * @var EmailTemplateRepository
     */
    private $emailTemplateRepository;

    /**
     * @var TaskAssigneeRepository
     */
    private $taskAssigneeRepository;

    /**
     * @var FormRepository
     */
    private $formRepository;


    /**
     * @var array 
     */
    private $config = [
		'apiTimeout' => 30,
	];

	const RETURN_SUCCESS = 0;
	const RETURN_ERROR = 9;

	public function __construct(LoggerInterface $logger, array $config) 
    {
		$this->logger = $logger;
		$this->config = array_merge($this->config, $config);
        $this->projectRepository = $this->getProjectRepository();
        $this->taskRepository = $this->getTaskRepository();
        $this->emailTemplateRepository = $this->getEmailTemplateRepository();
        $this->taskAssigneeRepository = $this->getTaskAssigneeRepository();
        $this->formRepository = $this->getFormRepository();
	}

    /**
     * @param array $payload
     * the JSON is structured as follow:
     * {
            "api_key":"OI3JC92V7N1688F0PVKH",
            "event_name":"project.partVersionSubmitted",
            "event_data":{"project_part_version_id":"621a74bd-0c41-5ad1-b6f9-f4635e11bcae"},
            "event_time":"2018-08-14 13:35:25",
            "object_type":"project",
            "object_id":"1710cfc1-6281-5f3f-9897-df7f2084376e",
            "callback_try_counter":"1",
            "callback_maximum_tries":3
        }
     * @return int
     */
	public function run(array $payload): int
    {
		// init project
		$this->logger->debug('Processing request', $payload);

		try {
            $this->validateApiKey($payload);
            $this->validateEventName($payload);
            $this->validateActionable($payload['event_name'], EventTypes::getValues());

            switch ($payload['event_name']) {
                case EventTypes::PROJECT_STATUS_CHANGED:
                    $event = new ProjectStatusChangedEvent($payload['object_id'], $payload['event_data']);
                    $this->validateActionable($event->getProjectStatusId(), ProjectStatuses::getValues());
                    $eventHandler = new ProjectStatusChangedEventHandler(
                        $this->logger, $this->getBusinessFlowFactory(), 
                        $this->projectRepository
                    );
                    $eventHandler->handleStatusChangeEvent($payload);
                    break;

                case EventTypes::PROJECT_PART_VERSION_SUBMITTED:
                    $event = new ProjectPartVersionSubmittedEvent($payload['object_id'], $payload['event_data']);
                    $eventHandler = new ProjectPartVersionSubmittedEventHandler(
                        $this->logger, $this->getBusinessFlowFactory(), 
                        $this->projectRepository
                    );
                    break;

//                case EventTypes::WORKFLOW_HTTP_CALL:
//                    $event = new WorkflowHttpCallEvent($payload['object_id'], $payload['event_data']);
//                    return (new WorkflowHttpCallEventHandler($this->logger))->execute($event);
//                    break;
//
//                case EventTypes::TASK_COMPLETE:
//                    $event = new TaskCompleteEvent($payload['object_id'], $payload['event_data']);
//                    return (new TaskCompleteEventHandler($this->logger))->execute($event);
//                    break;
                default:
            }

            $eventHandler->execute($event);
            return static::RETURN_SUCCESS;

        } catch (EndOfProcessException $exception) {
		    // This exception is thrown when the event $payload
            // is not actionable by the service.
            // The process should then end without error.
		    $this->logger->info($exception->getMessage(), $payload);
		    return self::RETURN_SUCCESS;

		} catch (\InvalidArgumentException $e) {
		    $this->logger->error($e->getMessage(), $payload);
		    return self::RETURN_ERROR;
        }
    }

    /**
     * @param array $payload
     * @throws \InvalidArgumentException
     */
    private function validateApiKey(array $payload)
    {
        if (!array_key_exists('api_key', $payload)) {
            throw new \InvalidArgumentException('Wrong payload received: Missing api_key');
        }

        if ($payload['api_key'] !== $this->config['apiKey']) {
            throw new \InvalidArgumentException('Wrong payload received: Invalid api_key');
        }
    }

    /**
     * @param array $payload
     * @throws \InvalidArgumentException
     */
    private function validateEventName(array $payload)
    {
        if (!isset($payload['event_name'])) {
            throw new \InvalidArgumentException('Wrong payload received: Missing event_name');
        }
    }

    /**
     * @param string $value
     * @param array $types
     * @throws \Optimy\Domain\EndOfProcessException
     */
    private function validateActionable(string $value, array $types)
    {
        if (!in_array($value, $types)) {
            throw new EndOfProcessException("The following value: {$value} is not actionable by this service. End of process.");
        }
    }

    /**
     * @return BusinessFlowFactory
     */
	private function getBusinessFlowFactory(): BusinessFlowFactory
    {
        $projectAnswerService = new ProjectAnswerService($this->projectRepository, $this->formRepository);
        $taskService = new TaskService($this->taskRepository, $this->taskAssigneeRepository, $this->emailTemplateRepository);
        $loggerService = new LoggerService($this->logger);

        return new BusinessFlowFactory(
            $this->projectRepository,
            $this->formRepository,
            $projectAnswerService,
            $taskService,
            $loggerService
        );
    }

    /**
     * @return ProjectRepository
     */
	private function getProjectRepository()
    {
	    return new ProjectRepository($this->getClient());
    }

    /**
     * @return TaskRepository
     */
    private function getTaskRepository(): TaskRepository
    {
        return new TaskRepository($this->getClient());
    }

    /**
     * @return EmailTemplateRepository
     */
    private function getEmailTemplateRepository(): EmailTemplateRepository
    {
        return new EmailTemplateRepository($this->getClient());
    }

    /**
     * @return TaskAssigneeRepository
     */
    private function getTaskAssigneeRepository(): TaskAssigneeRepository
    {
        return new TaskAssigneeRepository($this->getClient());
    }

    /**
     * @return FormRepository
     */
    private function getFormRepository(): FormRepository
    {
        return new FormRepository($this->getClient());
    }

    /**
     * @return Client
     */
	private function getClient(): Client
    {
        $client = new \GuzzleHttp\Client(
            [
                'base_uri' => $this->config['apiEndpoint'],
                'timeout' => $this->config['apiTimeout'],
                'auth' => [$this->config['apiKey'], $this->config['apiSecret']]
            ]
        );

        return new Client($this->logger, $client);
    }
}