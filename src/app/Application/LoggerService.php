<?php

namespace Optimy\CallbackReceiverLambda\Application;


use Psr\Log\LoggerInterface;

class LoggerService
{
    private $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function logTaskUpdateDueDate(string $projectId, string $taskName, string $answerId, string $newValue)
    {
        $this->logger->info("Updating due date (new value: {$newValue}) in task '{$taskName}' for project id {$projectId} and ppva id {$answerId}");
    }

    public function logTaskExists(string $projectId, string $taskName, string $answerId)
    {
        $this->logger->info("Task '{$taskName}' already exists for project id {$projectId} and ppva id {$answerId}. Continue.");
    }

    public function logTaskExistsForPartner(string $projectId, string $taskName, string $partnerName)
    {
        $this->logger->info("Task '{$taskName}' already exists for project id {$projectId} and partner {$partnerName}. Continue.");
    }

    public function logTaskNotCreated(string $projectId, string $taskName, string $answerId)
    {
        $this->logger->warning("Task {$taskName} was not created for project id {$projectId} and ppva id {$answerId}. Please check logs.");
    }

    public function logTaskNotCreatedConditionsNotMet(string $projectId, string $taskName, string $answerId)
    {
        $this->logger->info("Can not create task {$taskName} for project {$projectId} and ppva {$answerId} because one or more conditional tasks have not been completed yet. Continue.");
    }

    public function logTaskNotCreatedMissingCorrespondingAF(string $projectId, string $addRemoveSectionId, string $partnerCode, string $afNumber)
    {
        $this->logger->warning("The 'Saisie des AF/Budgets' add-remove section {$addRemoveSectionId} does not exist for project {$projectId} with partner code {$partnerCode} and AF number {$afNumber}. No task creation possible. Continue.");
    }

    public function logMissingAnswersInAddRemove(string $projectId)
    {
        $this->logger->warning("The add-remove section misses required answers. No task creation possible for this add-remove on the following project: {$projectId}. Continue.");
    }

    public function logTaskToCloseDoesNotExist(string $projectId, string $taskName)
    {
        $this->logger("Task {$taskName} does not exist for project {$projectId}. Can't close it.");
    }

    public function logTaskClosing(string $projectId, string $taskName, string $taskTag)
    {
        $this->logger("Closing Task {$taskName} with tag {$taskTag} for project {$projectId}...");
    }
}