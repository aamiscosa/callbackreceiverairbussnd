<?php

namespace Optimy\CallbackReceiverLambda\Application;


use Optimy\Domain\Project;
use Optimy\Domain\Task;
use Optimy\Infrastructure\Repositories\EmailTemplateRepository;
use Optimy\Infrastructure\Repositories\TaskAssigneeRepository;
use Optimy\Infrastructure\Repositories\TaskRepository;

class TaskService
{
    private $taskRepository;
    private $taskAssigneeRepository;
    private $emailTemplateRepository;

    public function __construct(
        TaskRepository $taskRepository,
        TaskAssigneeRepository $taskAssigneeRepository,
        EmailTemplateRepository $emailTemplateRepository
    )
    {
        $this->taskRepository = $taskRepository;
        $this->taskAssigneeRepository = $taskAssigneeRepository;
        $this->emailTemplateRepository = $emailTemplateRepository;
    }

    /**
     * @param string $projectId
     * @param string $tag
     * @return null|\Optimy\Domain\Task
     */
    public function getTaskByTag(string $projectId, string $tag)
    {
        $tasks = $this->taskRepository->getProjectTasksByTag($projectId, $tag);
        return count($tasks) > 0 ? array_shift($tasks) : null;
    }

    /**
     * @param string $projectId
     * @param string $tag
     * @return array|Task[]
     */
    public function getTasksByTag(string $projectId, string $tag)
    {
        return $this->taskRepository->getProjectTasksByTag($projectId, $tag);
    }

    /**
     * @param Project $project
     * @param string $dueDate
     * @param string $taskName
     * @param string $taskType
     * @param string $assigneeRole
     * @param int $emailTemplateId
     * @param array $tokens
     * @param string $taskTag
     * @return Task
     */
    public function buildTask(
        Project $project,
        string $dueDate,
        string $taskName,
        string $taskType,
        string $assigneeRole,
        int $emailTemplateId,
        array $tokens,
        string $taskTag
    )
    {
        $assigneeId = $this->taskAssigneeRepository->getUserIdFromTeamWithGivenRole($project->getLeaderTeamId(), $assigneeRole);
        $task = new Task($taskName, $assigneeId, $assigneeId, $dueDate, $project->getId());
        $task->setTaskType($taskType);

        $description = $this->emailTemplateRepository->getTemplateById($emailTemplateId, 'fr')
            ->replaceTokens($tokens)
            ->appendTag($taskTag)
            ->getBody();

        $task->addDescription($description);

        return $task;
    }

    /**
     * @param Task $task
     * @return bool
     */
    public function saveTask(Task $task)
    {
        return $this->taskRepository->saveTask($task);
    }
}