<?php

namespace Optimy\CallbackReceiverLambda\Application\BusinessFlows;


use Esky\Enum\Enum;
use Optimy\CallbackReceiverLambda\Application\BusinessFlows\AFBusinessFlow;
use Optimy\CallbackReceiverLambda\Application\BusinessFlows\ATFBusinessFlow;
use Optimy\CallbackReceiverLambda\Application\BusinessFlows\CalendriersBusinessFlow;
use Optimy\CallbackReceiverLambda\Application\BusinessFlows\ComiteBusinessFlow;
use Optimy\CallbackReceiverLambda\Application\BusinessFlows\ContrepartiesBusinessFlow;
use Optimy\CallbackReceiverLambda\Application\BusinessFlows\LABBusinessFlow;
use Optimy\CallbackReceiverLambda\Application\BusinessFlows\ReportsBusinessFlow;
use Optimy\CallbackReceiverLambda\Application\BusinessFlows\VisuelsBusinessFlow;

/**
 * Class BusinessFlows
 *
 * This class consists of an Enum of Business Flows
 * To add a business flow, simply add a constant
 * with the corresponding class
 *
 * @package Optimy\CallbackReceiverLambda\BusinessFlows
 */
class BusinessFlows extends Enum
{
    public const LAB_TASK = LABBusinessFlow::class;
    public const REPORTS_TASK = ReportsBusinessFlow::class;
    public const AF_TASK = AFBusinessFlow::class;
    public const COMITE_TASK = ComiteBusinessFlow::class;
    public const VISUELS_TASK = VisuelsBusinessFlow::class;
    public const ATF_TASK = ATFBusinessFlow::class;
    public const CONTREPARTIES_TASK = ContrepartiesBusinessFlow::class;
    public const CALENDRIERS_TASK = CalendriersBusinessFlow::class;

}