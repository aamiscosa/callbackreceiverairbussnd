<?php

namespace Optimy\CallbackReceiverLambda\Application\BusinessFlows;


use Optimy\Domain\Project;
use Optimy\Domain\ProjectAnswer;
use Optimy\Domain\Task;
use Optimy\CallbackReceiverLambda\Domain\AssigneeRoles;
use Optimy\CallbackReceiverLambda\Domain\EmailTemplates;
use Optimy\CallbackReceiverLambda\Domain\FormParts;
use Optimy\CallbackReceiverLambda\Domain\Forms;
use Optimy\CallbackReceiverLambda\Domain\FormScreens;
use Optimy\CallbackReceiverLambda\Domain\Tasks;
use Optimy\CallbackReceiverLambda\Domain\TaskTags;
use Optimy\CallbackReceiverLambda\Domain\TaskTypes;

/**
 * This class handles the creation, the update and the closing of the task ATF
 *
 * Class ATFBusinessFlow
 * @package Optimy\CallbackReceiverLambda\BusinessFlows
 */
class ATFBusinessFlow extends AbstractBusinessFlow
{
    private const ADD_REMOVES = [
        'Total SA' => '7892e172-095c-5c31-9d88-8c1fcdc40ab3', //Total SA
        'Fondation Total' => '2295c1c7-ef78-5539-8345-bbb8c033f7db', //Fondation Total
        'NRPTF' => 'd8e3a317-ee7d-5deb-a50d-f4608b95fa4c', //NRPTF
        'Naturenautes' => 'aab51db7-b14f-5e8f-a0c3-a8915b3f522c'  //Naturnautes
    ];

    private const AF_ADD_REMOVES = [
        'Total SA' => 'c7968839-2f63-5a3f-8e23-5ca96a89a8e4',
        'Fondation Total' => '2ee8a5b6-e2a2-5329-bb5f-6a93cef1e55b',
        'NRPTF' => '61e45a42-12d8-5098-89cf-f14c5392e2b6',
        'Naturenautes' => '7565d6a0-68cd-5fd6-9bf5-5f07824837a6'
    ];

    private const PARTNER_CODE_ANSWER_NAME = 'Code du Partenaire';
    private const AMOUNT_ANSWER_NAME = 'Montant du/des AF(s)';
    private const AF_NUMBER_ANSWER_NAME = 'Numéro de l\'AF';
    private const AF_RECEPTION_DATE_ANSWER_NAME = 'Date(s) du/des AF(s)'; //Date de réception effective de l’AF
    private const ATF_DATE_ANSWER_NAME = 'Lien vers les documents afférents (AF et ATF)'; //Date prévisonnelle de l'atf
    private const ATF_RECEPTION_DATE_ANSWER_NAME = 'Date de réception de l’ATF correspondante'; //Date de réception de l’ATF correspondante

    private const TOKENS = [
        'amount' => 'answer.amount',
        'partnerCode' => 'answer.partenaireCode',
        'sponsor' => 'answer.sponsor',
    ];

    protected function handle(Project $project): bool
    {
        $version = $this->getLastPublishedVersionOfFormPart($project->getId(), FormParts::GESTION_DES_AF_ATF);

        $tasksHaveAllBeenUpdated = true;
        foreach (self::ADD_REMOVES as $partnerName => $addRemoveSectionId) {

            $candidateToTaskCreation = $this->projectAnswerService->getAddRemoveSectionProjectAnswers(
                $project->getId(),
                $version->getId(),
                Forms::REFERENTIEL_CONVENTIONS,
                FormParts::GESTION_DES_AF_ATF,
                FormScreens::GESTION_DES_AF_ATF,
                [$addRemoveSectionId]
            );

            foreach ($candidateToTaskCreation as $atf) {
                $date = $this->getProjectAnswerByName(self::ATF_DATE_ANSWER_NAME, $atf);
                $partnerCode = $this->getProjectAnswerByName(self::PARTNER_CODE_ANSWER_NAME, $atf);
                $afNumber = $this->getProjectAnswerByName(self::AF_NUMBER_ANSWER_NAME, $atf);
                $afReceptionDate = $this->getProjectAnswerByName(self::AF_RECEPTION_DATE_ANSWER_NAME, $atf);

                if (is_null($date)) {
                    $this->loggerService->logMissingAnswersInAddRemove($project->getId());
                    continue;
                }

                // We get the corresponding AF from 'Saisies des AF/Budgets' screen
                $AFAddRemoveSection = $this->getCorrespondingAFAddRemoveSection(
                    $project->getId(),
                    self::AF_ADD_REMOVES[$partnerName],
                    $partnerCode->getValue(),
                    $afNumber->getValue()
                );

                if (is_null($AFAddRemoveSection)) {
                    $this->loggerService->logTaskNotCreatedMissingCorrespondingAF($project->getId(), self::ADD_REMOVES[$partnerName], $partnerCode->getValue(), $afNumber->getValue());
                    continue;
                }

                // Close related AF Task
                if (!is_null($afReceptionDate)) {
                    $tasksHaveAllBeenUpdated &= $this->closeAFTask($project->getId(), $AFAddRemoveSection);
                }

                $task = $this->taskService->getTaskByTag($project->getId(), TaskTags::ATF . $date->getId());

                if (is_null($task)) {
                    // Create new task
                    $amount = $this->getProjectAnswerByName(self::AMOUNT_ANSWER_NAME, $AFAddRemoveSection);
                    $task = $this->buildTask($project, $amount, $date, $partnerName);

                } else {
                    // Do we need to update the task ?
                    if (!$task->isCompleted() && $task->getdueDate() !== $date->getValue()) {
                        $this->loggerService->logTaskUpdateDueDate($project->getId(), $task->getName(), $date->getId(), $date->getValue());

                        $description = str_replace($task->getDueDate(), $date->getValue(), $task->getDescription());
                        $task->addDescription($description)
                            ->setDueDate($date->getValue());

                    } else {
                        $atfReceptionDate = $this->getProjectAnswerByName(self::ATF_RECEPTION_DATE_ANSWER_NAME, $atf);

                        // Task already exists and should not be completed
                        if (is_null($atfReceptionDate)) {
                            $this->loggerService->logTaskExistsForPartner($project->getId(), $task->getName(), $date->getId());
                            continue;
                        }

                        // Complete task
                        $task->wasCompletedOn((new \DateTime())->format('Y-m-d H:i:s'));
                    }
                }

                $taskCreated = $this->taskService->saveTask($task);
                $tasksHaveAllBeenUpdated &= $taskCreated;

                if (!$taskCreated) {
                    $this->loggerService->logTaskNotCreated($project->getId(), $task->getName(), $date->getId());
                }
            }
        }
        return $tasksHaveAllBeenUpdated;
    }

    /**
     * @param string $projectId
     * @param array $AFAddRemoveSection
     * @return bool
     */
    private function closeAFTask(string $projectId, array $AFAddRemoveSection): bool
    {
        $date = $this->getProjectAnswerByName(self::AF_RECEPTION_DATE_ANSWER_NAME, $AFAddRemoveSection);

        $task = $this->taskService->getTaskByTag($projectId, TaskTags::ATF . $date->getId());

        $isClosed = true;

        if (is_null($task)) {
            $this->loggerService->logTaskToCloseDoesNotExist($projectId, TaskTags::ATF . $date->getId());
            $isClosed = false;
        } else {
            if (!$task->isCompleted()) {
                $this->loggerService->logTaskClosing($projectId, Tasks::AF, TaskTags::ATF . $date->getId());
                $task->wasCompletedOn((new \DateTime())->format('Y-m-d H:i:s'));
                $isClosed = $this->taskService->saveTask($task);
            }
        }

        return $isClosed;
    }

    /**
     * @param \Optimy\Domain\Project $project
     * @param ProjectAnswer $amount
     * @param ProjectAnswer $date
     * @param string $sponsor
     * @return Task
     */
    private function buildTask(
        Project $project,
        ProjectAnswer $amount,
        ProjectAnswer $date,
        string $sponsor
    ): Task
    {
        $tokens = [
            self::TOKENS['partnerCode'] => $date->getValue(),
            self::TOKENS['amount'] => $amount->getValue(),
            self::TOKENS['sponsor'] => $sponsor
        ];

        return $this->taskService->buildTask(
            $project,
            $date->getValue(),
            Tasks::ATF,
            TaskTypes::MODELE_2,
            AssigneeRoles::ASSISTANTE,
            EmailTemplates::ATF_NON_RECU,
            $tokens,
            TaskTags::ATF . $date->getId()
        );
    }

    /**
     * @param string $projectId
     * @param string $partnerAddRemoveSectionId
     * @param string $partnerCodeId
     * @param int $afNumber
     * @return array|null
     * @throws \Optimy\Domain\EndOfProcessException
     */
    private function getCorrespondingAFAddRemoveSection(string $projectId, string $partnerAddRemoveSectionId, string $partnerCodeId, int $afNumber): ?array
    {
        $version = $this->getLastPublishedVersionOfFormPart($projectId, FormParts::SAISIE_DES_AF_BUDGET);
        $addRemoveSections = $this->projectAnswerService->getAddRemoveSectionProjectAnswers(
            $projectId,
            $version->getId(),
            Forms::REFERENTIEL_CONVENTIONS,
            FormParts::SAISIE_DES_AF_BUDGET,
            FormScreens::SAISIE_DES_AF_BUDGET,
            [$partnerAddRemoveSectionId]
        );


        foreach ($addRemoveSections as $addRemoveProjectAnswers) {
            $partnerCode = $this->getProjectAnswerByName(self::PARTNER_CODE_ANSWER_NAME, $addRemoveProjectAnswers);
            $afNumberAnswer = $this->getProjectAnswerByName(self::AF_NUMBER_ANSWER_NAME, $addRemoveProjectAnswers);

            if ($partnerCode->getValue() === $partnerCodeId && $afNumberAnswer->getValue() === $afNumber) {
                return $addRemoveProjectAnswers;
            }
        }

        return null;
    }

}