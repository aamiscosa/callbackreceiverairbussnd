<?php

namespace Optimy\CallbackReceiverLambda\Application\BusinessFlows;


use Optimy\CallbackReceiverLambda\Application\BusinessFlows\AbstractBusinessFlow;
use Optimy\Domain\Project;
use Optimy\Domain\ProjectAnswer;
use Optimy\Domain\Task;
use Optimy\CallbackReceiverLambda\Domain\AssigneeRoles;
use Optimy\CallbackReceiverLambda\Domain\EmailTemplates;
use Optimy\Domain\EndOfProcessException;
use Optimy\CallbackReceiverLambda\Domain\FormParts;
use Optimy\CallbackReceiverLambda\Domain\Forms;
use Optimy\CallbackReceiverLambda\Domain\FormScreens;
use Optimy\CallbackReceiverLambda\Domain\Tasks;
use Optimy\CallbackReceiverLambda\Domain\TaskTags;
use Optimy\CallbackReceiverLambda\Domain\TaskTypes;

class ComiteBusinessFlow extends AbstractBusinessFlow
{
    private const EXCEPTION_TERRITORIALITE_Q = 'b3a6cc9e-8ead-5300-a456-f09713d0b021'; // Question id of the question "Exception de territorialité"
    private const EXCEPTION_TERRITORIALITE_A = 'cf1455d8-9760-5696-b60b-a823bdd87588'; // Answer id of the answer "NON" to the question "Exception de territorialité"
    private const ADD_REMOVE = 'efb06a5d-2037-5520-8e06-8404f4310488'; // "Réunions de suivi"
    private const DATE_ANSWER_NAME = 'Date prévisionnelle';

    protected function handle(Project $project): bool
    {
        $this->validateExceptionTerritoriale($project);
        $version = $this->getLastPublishedVersionOfFormPart($project->getId(), FormParts::SAISIE_DES_CALENDRIERS);

        $candidateToTaskCreation = $this->projectAnswerService->getAddRemoveSectionProjectAnswers(
            $project->getId(),
            $version->getId(),
            Forms::REFERENTIEL_CONVENTIONS,
            FormParts::SAISIE_DES_CALENDRIERS,
            FormScreens::SAISIE_DES_CALENDRIERS,
            [self::ADD_REMOVE]
        );

        // Task creation
        $tasksHaveAllBeenCreated = true;
        foreach ($candidateToTaskCreation as $comite) {
            $date = $this->getProjectAnswerByName(self::DATE_ANSWER_NAME, $comite);

            $task = $this->taskService->getTaskByTag($project->getId(), TaskTags::COMITE . $date->getId());

            if (is_null($task)) {
                // Create new task
                $task = $this->buildTask($project, $date);
            } else {
                // Do we need to update the task ?
                if (!$task->isCompleted() && $task->getdueDate() !== $date->getValue()) {
                    $this->loggerService->logTaskUpdateDueDate($project->getId(), $task->getName(), $date->getId(), $date->getValue());

                    $task->setDueDate($date->getValue());

                } else {
                    $this->loggerService->logTaskExists($project->getId(), $task->getName(), $date->getId());
                    continue;
                }
            }

            $taskCreated = $this->taskService->saveTask($task);
            $tasksHaveAllBeenCreated &= $taskCreated;

            if (!$taskCreated) {
                $this->loggerService->logTaskNotCreated($project->getId(), $task->getName(), $date->getId());
            }
        }

        return $tasksHaveAllBeenCreated;
    }

    /**
     * @param \Optimy\Domain\Project $project
     * @param ProjectAnswer $date
     * @return Task
     */
    private function buildTask(Project $project, ProjectAnswer $date): Task
    {
        return $this->taskService->buildTask(
            $project,
            $date->getValue(),
            Tasks::COMITE,
            TaskTypes::MODELE_2,
            AssigneeRoles::RESPONSABLE_DE_PROGRAMME,
            EmailTemplates::COMITE_NON_TENU,
            [],
            TaskTags::COMITE . $date->getId()
        );
    }

    /**
     * @param \Optimy\Domain\Project $project
     * @throws \Optimy\Domain\EndOfProcessException
     */
    private function validateExceptionTerritoriale(Project $project)
    {
        $version = $this->getLastPublishedVersionOfFormPart($project->getId(), FormParts::INFORMATIONS_JURIDIQUES);
        $answers = $this->projectRepository->getProjectAnswers($project->getId(), $version->getId(), self::EXCEPTION_TERRITORIALITE_Q);
        $exceptionTerritoriale = array_shift($answers);

        if ($exceptionTerritoriale->getId() === self::EXCEPTION_TERRITORIALITE_A) {
            throw new EndOfProcessException("Question 'Exception de territorialité' is NON. End of process.");
        }
    }
}