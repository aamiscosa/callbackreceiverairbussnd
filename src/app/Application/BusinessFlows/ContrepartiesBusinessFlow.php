<?php

namespace Optimy\CallbackReceiverLambda\Application\BusinessFlows;


use Optimy\CallbackReceiverLambda\Application\BusinessFlows\AbstractBusinessFlow;
use Optimy\Domain\Project;
use Optimy\Domain\ProjectAnswer;
use Optimy\Domain\Task;
use Optimy\CallbackReceiverLambda\Domain\AssigneeRoles;
use Optimy\CallbackReceiverLambda\Domain\EmailTemplates;
use Optimy\Domain\EndOfProcessException;
use Optimy\CallbackReceiverLambda\Domain\FormParts;
use Optimy\CallbackReceiverLambda\Domain\Forms;
use Optimy\CallbackReceiverLambda\Domain\FormScreens;
use Optimy\CallbackReceiverLambda\Domain\Tasks;
use Optimy\CallbackReceiverLambda\Domain\TaskTags;
use Optimy\CallbackReceiverLambda\Domain\TaskTypes;

class ContrepartiesBusinessFlow extends AbstractBusinessFlow
{
    private const DATE_ECHEANCE_CONTREPARTIES_Q = 'cdbfbb72-8f04-5c61-bc43-48fd4f962a50';
    private const SOLDE_CONTREPARTIES_Q = 'b735a396-d5f4-5a6e-95ae-b14c4f8ea3f5';

    protected function handle(Project $project): bool
    {
        $version = $this->getLastPublishedVersionOfFormPart($project->getId(), FormParts::CONTREPARTIES);

        $projectAnswers = $this->projectRepository->getFormScreenAnswers($project->getId(), $version->getId(), FormScreens::CONTREPARTIES);

        $dueDate = $this->getProjectAnswerByQuestionId($projectAnswers, self::DATE_ECHEANCE_CONTREPARTIES_Q);
        $balance = $this->getProjectAnswerByQuestionId($projectAnswers, self::SOLDE_CONTREPARTIES_Q);

        if (is_null($dueDate) || is_null($balance)) {
            throw new EndOfProcessException("Can not create task " . Tasks::CONTREPARTIES . " for project: {$project->getId()}. Missing either due date or available balance. End of process.");
        }

        if ($balance->getValue() === 0) {
            throw new EndOfProcessException("Solde des contreparties is 0 for project: {$project->getId()}. Missing either due date or available balance.");
        }

        $task = $this->taskService->getTaskByTag($project->getId(), TaskTags::CONTREPARTIES . $dueDate->getId());

        if (is_null($task)) {
            // Create new task
            $task = $this->buildTask($project, $dueDate, EmailTemplates::ECHEANCES_CONTREPARTIES, TaskTags::CONTREPARTIES);
        } else {
            // Do we need to update the task ?
            if (!$task->isCompleted() && $task->getdueDate() !== $dueDate->getValue()) {
                $this->loggerService->logTaskUpdateDueDate($project->getId(), $task->getName(), $dueDate->getId(), $dueDate->getValue());

                $task->setDueDate($dueDate->getValue());

            } else {
                // Task is completed or doesn't need to be updated
                throw new EndOfProcessException("Task '". $task->getName() ."' already exists for project id {$project->getId()} and ppva id {$dueDate->getId()}. End of process.");
            }
        }

        $taskCreated = $this->taskService->saveTask($task);

        if (!$taskCreated) {
            $this->loggerService->logTaskNotCreated($project->getId(), $task->getName(), $dueDate->getId());
        }

        return $taskCreated;
    }

    /**
     * @param array $projectAnswers
     * @param string $questionId
     * @return null|\Optimy\Domain\ProjectAnswer
     */
    private function getProjectAnswerByQuestionId(array $projectAnswers, string $questionId): ?ProjectAnswer
    {
        $answer = array_values(array_filter(
            $projectAnswers,
            function ($projectAnswer) use ($questionId) {
                return $projectAnswer->getQuestionId() === $questionId;
            }
        ));

        return array_shift($answer);
    }

    /**
     * @param Project $project
     * @param ProjectAnswer $date
     * @param string $taskTagPrefix
     * @return Task
     */
    private function buildTask(Project $project, ProjectAnswer $date, string $taskTagPrefix): Task
    {
        return $this->taskService->buildTask(
            $project,
            $date->getValue(),
            Tasks::CONTREPARTIES,
            TaskTypes::MODELE_6,
            AssigneeRoles::RP,
            EmailTemplates::ECHEANCES_CONTREPARTIES,
            [],
            $taskTagPrefix . $date->getId()
        );
    }
}