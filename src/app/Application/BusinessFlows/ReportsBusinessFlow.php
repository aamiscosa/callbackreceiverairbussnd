<?php

namespace Optimy\CallbackReceiverLambda\Application\BusinessFlows;
use Optimy\CallbackReceiverLambda\Application\BusinessFlows\AbstractBusinessFlow;
use Optimy\Domain\FormAnswer;
use Optimy\Domain\Project;
use Optimy\Domain\ProjectAnswer;
use Optimy\Domain\ProjectPartVersion;
use Optimy\Domain\Task;
use Optimy\CallbackReceiverLambda\Domain\AssigneeRoles;
use Optimy\CallbackReceiverLambda\Domain\EmailTemplates;
use Optimy\CallbackReceiverLambda\Domain\FormParts;
use Optimy\CallbackReceiverLambda\Domain\Forms;
use Optimy\CallbackReceiverLambda\Domain\FormScreens;
use Optimy\CallbackReceiverLambda\Domain\Tasks;
use Optimy\CallbackReceiverLambda\Domain\TaskTags;
use Optimy\CallbackReceiverLambda\Domain\TaskTypes;

/**
 * This class handles the creation of the tasks related to Documents & Reports
 *   - Rapport final non reçu
 *   - Rapport intermédiaire non reçu
 *   - Fiche synthèse non reçue
 *   - Planning non reçu
 *
 * How does it work ?
 * ------------------
 *
 * If the status of the project from the form “Référentiel Convention” is “Projet signé des parties”:
 * 1.  Load the last published version of the form part “SAISIE DES CALENDRIERS”.
 *     If there is none, end of the process.
 *
 * 2.  Check all answers related to the fields “Date prévisionnelle de réception” of this part of the form.
 *     There can be up to 22 different add-remove sections containing this field.
 *     If there is none, end of the process.
 *
 * 3.  For each “Date prévisionnelle de réception”, there needs to be a check that the task for the document type (FS, RI, RF, P) does not exist yet.
 *     If it already exist:
 *          - Update the task IF it is not completed and has a different due date
 *          - Otherwise, end of the process.
 *
 *     If it does not exist yet, create it.
 *
 *
 * Notes
 * ------
 * The assignee of the task is the user who has the role “Chef de projet” in the team that is leader on the project.
 * The due date is the date encoded in the field “Date prévisionnelle de réception”.
 *
 * Class ReportsBusinessFlow
 * @package Optimy\CallbackReceiverLambda\BusinessFlows
 */
class ReportsBusinessFlow extends AbstractBusinessFlow
{
    public const ADD_REMOVE = 'cd3c4eb9-0cff-5d79-8397-ee5ccab3253b';
    private const TOKEN = 'answer.date';

    private const DATE_ANSWER_NAME = 'Date';

    protected function handle(Project $project): bool
    {
        $version = $this->getLastPublishedVersionOfFormPart($project->getId(), FormParts::SAISIE_DES_CALENDRIERS);

        $candidateToTaskCreation = $this->projectAnswerService->getAddRemoveSectionProjectAnswers(
            $project->getId(),
            $version->getId(),
            Forms::REFERENTIEL_CONVENTIONS,
            FormParts::SAISIE_DES_CALENDRIERS,
            FormScreens::SAISIE_DES_CALENDRIERS,
            [self::ADD_REMOVE]
        );

        // Task creation
        $tasksHaveAllBeenCreated = true;
        foreach ($candidateToTaskCreation as $report) {
            $date = $this->getProjectAnswerByName(self::DATE_ANSWER_NAME, $report);
            var_dump('DAAAAR', $date);
            $docType = $this->getDocumentTypeProjectAnswer($report);

            $task = $this->taskService->getTaskByTag($project->getId(), TaskTags::RAPPORTS . $docType->getName());

            if (is_null($task)) {
                // Create new task
                $task = $this->buildTask($project, $docType, $date);
            } else {
                // Do we need to update the task ?
                if (!$task->isCompleted() && $task->getdueDate() !== $date->getValue()) {
                    $this->loggerService->logTaskUpdateDueDate($project->getId(), $task->getName(), $date->getId(), $date->getValue());

                    $description = str_replace($task->getDueDate(), $date->getValue(), $task->getDescription());
                    $task->addDescription($description)
                        ->setDueDate($date->getValue());

                } else {
                    $this->loggerService->logTaskExists($project->getId(), $task->getName(), $date->getId());
                    continue;
                }
            }

            $taskCreated = $this->taskService->saveTask($task);
            $tasksHaveAllBeenCreated &= $taskCreated;

            if (!$taskCreated) {
                $this->loggerService->logTaskNotCreated($project->getId(), $task->getName(), $date->getId());
            }
        }

        return $tasksHaveAllBeenCreated;
    }

    /**
     * In this particular case (dropdowns), the answer name is always different
     * As there are only 2 answers in this add-remove, it is safe to say
     * the docType ProjectAnswer is the one that is not the 'Date prévisionnelle'
     *
     * @param array $addRemoveSection
     * @return ProjectAnswer
     */
    private function getDocumentTypeProjectAnswer(array $addRemoveSection): ProjectAnswer
    {
        $answer = array_values(array_filter(
            $addRemoveSection,
            function ($projectAnswer) {
                return stripos($projectAnswer->getName(), self::DATE_ANSWER_NAME) === false;
            }
        ));

        return array_shift($answer);
    }

    /**
     * @param Project $project
     * @param ProjectAnswer $docType
     * @param ProjectAnswer $date
     * @return Task
     */
    private function buildTask(Project $project, ProjectAnswer $docType, ProjectAnswer $date): Task
    {
        $taskCode = $this->getTaskCode($docType->getName());

        var_dump('TAAAAAAAASKCOOOOOOODE', $taskCode);
        $taskName = Tasks::REPORTS[$taskCode];
        var_dump('TASSSSSSSKKKKKKKKKK', $taskName);

        $tokens = [self::TOKEN => $date->getValue()];

        return $this->taskService->buildTask(
            $project,
            $date->getValue(),
            $taskName,
            TaskTypes::MODELE_2,
            AssigneeRoles::CHEF_DE_PROJET,
            EmailTemplates::RAPPORT_NON_REMIS,
            $tokens,
            TaskTags::RAPPORTS . $docType->getName()
        );
    }

    /**
     * Extracts the task code of the document type answer
     * can either be: FS, RI, RF, P
     *
     * @param string $documentType
     * @return string
     */
    private function getTaskCode(string $documentType): string
    {
        preg_match('/([a-zA-Z])+/', $documentType,$matches);
        return array_shift($matches);
    }
}