<?php

namespace Optimy\CallbackReceiverLambda\Application\BusinessFlows;


use Optimy\Domain\Project;
use Optimy\Domain\Task;
use Optimy\CallbackReceiverLambda\Domain\AssigneeRoles;
use Optimy\CallbackReceiverLambda\Domain\EmailTemplates;
use Optimy\Domain\EndOfProcessException;
use Optimy\CallbackReceiverLambda\Domain\FormParts;
use Optimy\CallbackReceiverLambda\Domain\Forms;
use Optimy\CallbackReceiverLambda\Domain\ProjectStatuses;
use Optimy\CallbackReceiverLambda\Domain\Tasks;
use Optimy\CallbackReceiverLambda\Domain\TaskTags;
use Optimy\CallbackReceiverLambda\Domain\TaskTypes;

/**
 * How does it work?
 * -------------------
 *
 * If the status of the project from the form “Référentiel Convention” is “Projet de convention validé”:
 * 1.  Get all related projects.
 *     If there is none, end of the process.
 *
 * 2.  Load the last published version of the form part “SAISIE DES AF / BUDGET”.
 *     If there is none, end of the process.
 *
 * 3.  Check all answers related to the fields “Code du partenaire” of this part of the form.
 *     They can be in 4 different add-remove sections
 *     (dedicated to
 *         1) Total SA,
 *         2) Fondation Total,
 *         3) Echéancier prévisionnel NR,
 *         4) Echéancier prévisionnel NA ).
 *     If there is none, end of the process.
 *
 * 4.  The “Code du partenaire” is the ID of the related project from the form “Référentiel Partenaires”.
 *     If one of the following conditions is not met, end of the process:
 *          - There is a match between the “Code du partenaire” and a related project
 *          - The related project is a record from the form “Référentiel Partenaires”
 *          - The status of the related project is not “LAB OK” or “LAB N/A”
 *
 * 5.  For each “Code du Partenaire” matching the criteria, check that the task “Vérifier que la procédure de LAB ait lieu pour le Partenaire” does not exist yet.
 *     If it already exist, end of the process.
 *     If it does not exist yet, create it.
 *
 * Notes
 * -----
 * The assignee of the task is the user who has the role “Responsable de programme” in the team that is leader on the project.
 * The due date is the date of creation of the task +7 days. *
 *
 *
 * Class LABBusinessFlow
 * @package Optimy\CallbackReceiverLambda\BusinessFlows
 */
class LABBusinessFlow extends AbstractBusinessFlow
{
    private const DUE_DATE = 'now + 1 week';
    private const TOKEN = 'partenaireName';
    public const ADD_REMOVE_ANSWER = 'Code du Partenaire (copy)';

    /**
     * @param Project $project
     * @return bool
     * @throws \Optimy\Domain\EndOfProcessException
     */
    protected function handle(Project $project): bool
    {
        $version = $this->getLastPublishedVersionOfFormPart($project->getId(), FormParts::SAISIE_DES_AF_BUDGET);

        $relatedProjects = $this->getFilteredRelatedProjects($project->getId());
        $projectAnswers = $this->getAnswersOfType(self::ADD_REMOVE_ANSWER, $project->getId(), $version->getId());

        $projectsCandidateToTaskCreation = $this->getProjectsCandidateToTaskCreation($relatedProjects, $projectAnswers, $project);

        $tasksHaveAllBeenCreated = true;
        foreach ($projectsCandidateToTaskCreation as $relatedProject) {
            // Create task if does not already exist
            $tasks = $this->taskService->getTasksByTag($project->getId(), TaskTags::LAB);

            if ($this->projectHasTask($tasks, $relatedProject->getName())) {
                $this->loggerService->logTaskExistsForPartner($project->getId(), Tasks::LAB, $relatedProject->getName());
                continue;
            }

            $task = $this->buildTask($project, $relatedProject);

            $taskCreated = $this->taskService->saveTask($task);
            $tasksHaveAllBeenCreated &= $taskCreated;

            if (!$taskCreated) {
                $this->loggerService->logTaskNotCreated($project->getId(), Tasks::LAB, $relatedProject->getToolReference());
            }
        }

        return $tasksHaveAllBeenCreated;
    }

    /**
     * This function, instead of checking on each add-removes if there is an answer to a selected question,
     * retrieve all the answers with the name equal to the question.
     * As each add-removes has exactly the same structure so far, we can easily get the questions of the same type by name.
     * Pay attention though:
     * this brings limitation as each question of the same type in each add-remove
     * MUST have the same name.
     *
     *
     * @param string $type
     * @param string $projectId
     * @param string $versionId
     * @return array
     * @throws EndOfProcessException
     */
    private function getAnswersOfType(string $type, string $projectId, string $versionId)
    {
        $answers = $this->projectRepository->getProjectAnswers($projectId, $versionId);

        $typedAnswers = array_values(array_filter(
            $answers,
            function ($answer) use ($type) {
                return $answer->getName() === $type;
            }));

        if (count($typedAnswers) === 0) {
            throw new EndOfProcessException("Last published version {$versionId} for project {$projectId} does not contain any answer of type {$type}. End of process.");
        }

        return $typedAnswers;
    }

    /**
     * @param \Optimy\Domain\Project $project
     * @param Project $relatedProject
     * @return \Optimy\Domain\Task
     */
    private function buildTask(Project $project, Project $relatedProject): Task
    {
        $tokens = [self::TOKEN => $relatedProject->getName()];

        return $this->taskService->buildTask(
            $project,
            (new \DateTime(self::DUE_DATE))->format('Y-m-d'),
            Tasks::LAB,
            TaskTypes::MODELE_7,
            AssigneeRoles::RESPONSABLE_DE_PROGRAMME,
            EmailTemplates::LAB,
            $tokens,
            TaskTags::LAB);
    }

    /**
     * @param \Optimy\Domain\Task[] $tasks
     * @param string $projectName
     * @return bool
     */
    private function projectHasTask(array $tasks, string $projectName): bool
    {
        return 0 < count(
            array_filter(
                $tasks,
                function ($task) use ($projectName) {
                    return strpos($task->getDescription(), $projectName) !== false;
                }
            ));
    }

    /**
     * @param array $relatedProjects
     * @param array $projectAnswers
     * @param Project $project
     * @return Project[]
     * @throws EndOfProcessException
     */
    private function getProjectsCandidateToTaskCreation(array $relatedProjects, array $projectAnswers, Project $project): array
    {
        $projectIdsToConsiderForTaskCreation = array_intersect(
            array_map(function ($answer) { return $answer->getValue();}, $projectAnswers),
            array_map(function ($project) { return $project->getToolReference();}, $relatedProjects)
        );

        if (count($projectIdsToConsiderForTaskCreation) === 0) {
            throw new EndOfProcessException("No match found between related project(s) of project id {$project->getId()} and 'Code du partenaire'. End of process.");
        }

        return array_values(
            array_filter($relatedProjects, function ($project) use ($projectIdsToConsiderForTaskCreation) {
                return in_array($project->getToolReference(), $projectIdsToConsiderForTaskCreation);
            })
        );
    }

    /**
     * @param array $projects
     * @param array $statusIds
     * @param bool $include - set to true if the status should be part of $statuses
     * @return \Optimy\Domain\Project[]
     */
    private function filterProjectsByStatus(array $projects, array $statusIds, bool $include = true): array
    {
        return array_values(array_filter(
            $projects,
            function ($project) use ($statusIds, $include) {
                if ($include) {
                    return in_array($project->getStatusId(), $statusIds);
                }
                return !in_array($project->getStatusId(), $statusIds);
            }
        ));
    }

    /**
     * @param array $projects
     * @param string $formId
     * @return Project[]
     */
    private function filterProjectsByForm(array $projects, string $formId)
    {
        return array_values(
            array_filter(
                $projects,
                function ($project) use ($formId) {
                    return $project->getFormId() === $formId;
                }
            )
        );
    }

    /**
     * @param string $projectId
     * @return Project[]
     * @throws \Optimy\Domain\EndOfProcessException
     */
    private function getFilteredRelatedProjects(string $projectId)
    {
        $relatedProjects = $this->projectRepository->getRelatedProjects($projectId);

        if (count($relatedProjects) === 0) {
            throw new EndOfProcessException("No related projects for project id {$projectId}. End of process.");
        }

        return $this->filterProjectsByStatus(
            $this->filterProjectsByForm($relatedProjects, Forms::REFERENTIEL_PARTENAIRES),
            [ProjectStatuses::LAB_NA, ProjectStatuses::LAB_OK],
            false
        );
    }
}