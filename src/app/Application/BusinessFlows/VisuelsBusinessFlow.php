<?php

namespace Optimy\CallbackReceiverLambda\Application\BusinessFlows;


use Optimy\Domain\Project;
use Optimy\Domain\ProjectAnswer;
use Optimy\Domain\Task;
use Optimy\CallbackReceiverLambda\Domain\AssigneeRoles;
use Optimy\CallbackReceiverLambda\Domain\EmailTemplates;
use Optimy\CallbackReceiverLambda\Domain\FormParts;
use Optimy\CallbackReceiverLambda\Domain\Forms;
use Optimy\CallbackReceiverLambda\Domain\FormScreens;
use Optimy\CallbackReceiverLambda\Domain\Tasks;
use Optimy\CallbackReceiverLambda\Domain\TaskTags;
use Optimy\CallbackReceiverLambda\Domain\TaskTypes;

class VisuelsBusinessFlow extends AbstractBusinessFlow
{
    private const ADD_REMOVE = '54e9d077-0ff2-5789-9141-6d6a171c5d2b';


    private const DATE_ANSWER_NAME = 'Date prévsionnelle de réception';
    private const AMOUNT_ANSWER_NAME = 'Nombre de visuels prévus';
    private const OTHER_ANSWER_NAME = 'Veuillez précisez:';
    private const TYPE_ANSWER_NAMES = ['Photos', 'Affiche', 'Vidéo'];

    private const TOKENS = [
        'date' => 'answer.date',
        'amount' => 'answer.number',
        'type'  => 'answer.type'
    ];

    protected function handle(Project $project): bool
    {
        $version = $this->getLastPublishedVersionOfFormPart($project->getId(), FormParts::SAISIE_DES_CALENDRIERS);

        $candidateToTaskCreation = $this->projectAnswerService->getAddRemoveSectionProjectAnswers(
            $project->getId(),
            $version->getId(),
            Forms::REFERENTIEL_CONVENTIONS,
            FormParts::SAISIE_DES_CALENDRIERS,
            FormScreens::SAISIE_DES_CALENDRIERS,
            [self::ADD_REMOVE]
        );

        // Task creation
        $tasksHaveAllBeenCreated = true;
        foreach ($candidateToTaskCreation as $visuel) {
            $amount = $this->getProjectAnswerByName(self::AMOUNT_ANSWER_NAME, $visuel);
            $date = $this->getProjectAnswerByName(self::DATE_ANSWER_NAME, $visuel);
            $type = $this->getDocumentType($visuel);
            $visuelTypeIsOther = $this->getProjectAnswerByName(self::OTHER_ANSWER_NAME, $visuel) !== null;

            $task = $this->taskService->getTaskByTag($project->getId(), TaskTags::VISUELS . $date->getId());

            if (is_null($task)) {
                // Create new task
                $task = $this->buildTask($project, $visuelTypeIsOther ? $type->getValue() : $type->getName(), $amount, $date);
            } else {
                // Do we need to update the task ?
                if (!$task->isCompleted() && $task->getdueDate() !== $date->getValue()) {
                    $this->loggerService->logTaskUpdateDueDate($project->getId(), $task->getName(), $date->getId(), $date->getValue());

                    $description = str_replace($task->getDueDate(), $date->getValue(), $task->getDescription());
                    $task->addDescription($description)
                        ->setDueDate($date->getValue());

                } else {
                    $this->loggerService->logTaskExists($project->getId(), $task->getName(), $date->getId());
                    continue;
                }

            }

            $taskCreated = $this->taskService->saveTask($task);
            $tasksHaveAllBeenCreated &= $taskCreated;

            if (!$taskCreated) {
                $this->loggerService->logTaskNotCreated($project->getId(), $task->getName(), $date->getId());
            }
        }

        return $tasksHaveAllBeenCreated;
    }

    /**
     * @param array $addRemoveSection
     * @return ProjectAnswer
     */
    private function getDocumentType(array $addRemoveSection): ProjectAnswer
    {
        $type = $this->getProjectAnswerByName(self::OTHER_ANSWER_NAME, $addRemoveSection);
        if (is_null($type)) {
            $type = array_values(
                array_filter(
                    $addRemoveSection,
                    function ($projectAnswer) {
                        return in_array($projectAnswer->getName(), self::TYPE_ANSWER_NAMES);
                    }
                )
            );
            $type = array_shift($type);
        }
        return $type;
    }

    /**
     * @param Project $project
     * @param string $type
     * @param ProjectAnswer $amount
     * @param \Optimy\Domain\ProjectAnswer $date
     * @return \Optimy\Domain\Task
     */
    private function buildTask(Project $project, string $type, ProjectAnswer $amount, ProjectAnswer $date): Task
    {
        $tokens = [
            self::TOKENS['date'] => $date->getValue(),
            self::TOKENS['type'] => $type,
            self::TOKENS['amount'] => $amount->getValue()
        ];

        return $this->taskService->buildTask(
            $project,
            $date->getValue(),
            Tasks::VISUEL,
            TaskTypes::MODELE_3,
            AssigneeRoles::CHEF_DE_PROJET,
            EmailTemplates::VISUELS_NON_RECUS,
            $tokens,
            TaskTags::VISUELS . $date->getId());
    }
}