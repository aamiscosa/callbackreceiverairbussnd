<?php

namespace Optimy\CallbackReceiverLambda\Application\BusinessFlows;


use Optimy\CallbackReceiverLambda\Application\BusinessFlows\AbstractBusinessFlow;
use Optimy\Domain\Project;
use Optimy\CallbackReceiverLambda\Domain\FormParts;
use Optimy\CallbackReceiverLambda\Domain\Forms;
use Optimy\CallbackReceiverLambda\Domain\FormScreens;
use Optimy\CallbackReceiverLambda\Domain\TaskTags;

class CalendriersBusinessFlow extends AbstractBusinessFlow
{
    private const ADD_REMOVES = [
        'COMITE' => '58e2dd6e-79ef-57c0-ac12-23fbcd05069f',
        'RAPPORTS' => '4b84eb49-0c0c-50b1-8adf-b0b05ccd69e3',
        'VISUELS' => '2b42c955-bb34-5851-8552-7570ff830f1f'
    ];

    private const SAISIE_ADD_REMOVES = [
        'COMITE' => 'efb06a5d-2037-5520-8e06-8404f4310488',
        'RAPPORTS' => 'cd3c4eb9-0cff-5d79-8397-ee5ccab3253b',
        'VISUELS' => '54e9d077-0ff2-5789-9141-6d6a171c5d2b'
    ];

    private const TYPE_IDENTIFIER_ANSWER_NAME = 'N°';
    private const DATE_ANSWER_NAME = 'Date prévisionnelle';

    protected function handle(Project $project): bool
    {
        $version = $this->getLastPublishedVersionOfFormPart($project->getId(), FormParts::GESTION_DES_CALENDRIERS);
        $projectAnswers = $this->projectRepository->getFormScreenAnswers($project->getId(), $version->getId(), FormScreens::GESTION_DES_CALENDRIERS);

        $tasksHaveAllBeenUpdated = true;
        foreach (self::ADD_REMOVES as $type => $addRemoveSectionId) {
            $addRemoveAnswers = $this->formRepository->getAddRemoveAnswersStructure(Forms::REFERENTIEL_CONVENTIONS, FormParts::GESTION_DES_CALENDRIERS, FormScreens::GESTION_DES_CALENDRIERS, $addRemoveSectionId);

            $elementsCandidateToTaskClosing = $this->mapProjectAnswersToAddRemoves($projectAnswers, $addRemoveAnswers);

            foreach ($elementsCandidateToTaskClosing as $addRemoveSection) {
                // Element identifier will always be the first element
                $elementIdentifier = array_shift($addRemoveSection);
                $date = $this->getProjectAnswerByName(self::DATE_ANSWER_NAME);

                if (is_null($date)) {
                    $this->logger->info("Reception date has not been filled for {$type} " . $type === 'rapports' ? $elementIdentifier->getName() : $elementIdentifier->getValue() . " in 'Gestion des calendriers' for project {$project->getId()}. Can not close task. Continue.");
                    continue;
                }

                $tag = $this->getTaskTag($project, $type, $elementIdentifier);

                $tasks = $this->taskRepository->getProjectTasksByTag($project->getId(), $tag);

                if (count($tasks) > 0) {
                    // There can only be one task
                    $task = array_shift($tasks);

                    // Do we need to update the task ?
                    if (!$task->isCompleted()) {
                        // Complete task
                        $task->wasCompletedOn((new \DateTime())->format('Y-m-d H:i:s'));
                        $this->logger("Closing Task {$task->getName()} with tag {$tag} for project {$project->getId()}...");
                        $tasksHaveAllBeenUpdated  &= $this->taskRepository->saveTask($task);

                    }

                } else {
                    $this->logger->info("Task {$tag} does not exist. Can not close it. Continue.");
                    continue;
                }


            }
        }
    }

    /**
     * @param Project $project
     * @param $type
     * @param $elementIdentifier
     * @return string
     * @throws \Optimy\Domain\EndOfProcessException
     */
    private function getTaskTag(Project $project, $type, $elementIdentifier): string
    {
        if ($type === 'rapports') {
            preg_match('/^(FS|RF|RI|P)\d/', $elementIdentifier->getName(), $matches);
            $tagId = array_shift($matches);
        } else {
            // We get the corresponding AF from 'Saisies des calendriers' screen
            $saisieAddRemoveSection = $this->getCorrespondingAddRemoveSection(
                $project->getId(),
                self::SAISIE_ADD_REMOVES[$type],
                $elementIdentifier->getValue()
            );
            $tagId = $this->getProjectAnswerByName(self::DATE_ANSWER_NAME, $saisieAddRemoveSection)->getid();
        }

        return TaskTags::createFromConstantName($type)->getValue() . $tagId;
    }

    /**
     * @param string $projectId
     * @param string $typeAddRemoveSectionId
     * @param string $elementIdentifier
     * @return null
     * @throws \Optimy\Domain\EndOfProcessException
     */
    private function getCorrespondingAddRemoveSection(string $projectId, string $typeAddRemoveSectionId, string $elementIdentifier)
    {
        $version = $this->getLastPublishedVersionOfFormPart($projectId, FormParts::SAISIE_DES_CALENDRIERS);
        $projectAnswers = $this->getFormScreenAnswers($projectId, $version->getId(), FormScreens::SAISIE_DES_CALENDRIERS);

        $addRemoveAnswers = $this->formRepository->getAddRemoveAnswersStructure(Forms::REFERENTIEL_CONVENTIONS, FormParts::SAISIE_DES_CALENDRIERS, FormScreens::SAISIE_DES_CALENDRIERS, $typeAddRemoveSectionId);
        $addRemoveSections = $this->mapProjectAnswersToAddRemoves($projectAnswers, $addRemoveAnswers);

        foreach ($addRemoveSections as $addRemoveProjectAnswers) {
            $typeIdentifier = $this->getProjectAnswerByName(self::TYPE_IDENTIFIER_ANSWER_NAME, $addRemoveProjectAnswers);

            if ($typeIdentifier->getValue() === $elementIdentifier) {
                return $addRemoveProjectAnswers;
            }
        }

        return null;
    }

}