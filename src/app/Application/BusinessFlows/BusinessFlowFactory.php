<?php

namespace Optimy\CallbackReceiverLambda\Application\BusinessFlows;


use Optimy\Application\ProjectAnswerService;
use Optimy\CallbackReceiverLambda\Application\LoggerService;
use Optimy\CallbackReceiverLambda\Application\TaskService;
use Optimy\Infrastructure\Repositories\FormRepository;
use Optimy\Infrastructure\Repositories\ProjectRepository;

class BusinessFlowFactory
{
    /**
     * @var LoggerService
     */
    private $logger;

    /**
     * @var ProjectRepository
     */
    private $projectRepository;

    /**
     * @var \Optimy\Infrastructure\Repositories\FormRepository
     */
    protected $formRepository;

    /**
     * @var ProjectAnswerService
     */
    protected $projectAnswerService;

    /**
     * @var TaskService
     */
    protected $taskService;

    public function __construct(
        ProjectRepository $projectRepository,
        FormRepository $formRepository,
        ProjectAnswerService $projectAnswerService,
        TaskService $taskService,
        LoggerService $logger
    )
    {
        $this->logger = $logger;
        $this->projectRepository = $projectRepository;
        $this->taskService = $taskService;
        $this->projectAnswerService = $projectAnswerService;
        $this->formRepository = $formRepository;
    }

    /**
     * This factory accepts as argument
     * one of the values of BusinessFlows::class
     *
     * To add a new business flow, simply add a constant
     * in that file with the name of the new class;
     *
     * @param string $businessFlow
     * @return AbstractBusinessFlow
     */
    public function create(string $businessFlow): AbstractBusinessFlow
    {
        return new $businessFlow(
            $this->projectRepository,
            $this->formRepository,
            $this->projectAnswerService,
            $this->taskService,
            $this->logger
        );
    }

}