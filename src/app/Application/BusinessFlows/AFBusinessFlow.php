<?php

namespace Optimy\CallbackReceiverLambda\Application\BusinessFlows;


use Optimy\CallbackReceiverLambda\Application\BusinessFlows\AbstractBusinessFlow;
use Optimy\Domain\Project;
use Optimy\Domain\ProjectAnswer;
use Optimy\Domain\Task;
use Optimy\CallbackReceiverLambda\Domain\AssigneeRoles;
use Optimy\CallbackReceiverLambda\Domain\EmailTemplates;
use Optimy\CallbackReceiverLambda\Domain\FormParts;
use Optimy\CallbackReceiverLambda\Domain\Forms;
use Optimy\CallbackReceiverLambda\Domain\FormScreens;
use Optimy\CallbackReceiverLambda\Domain\Tasks;
use Optimy\CallbackReceiverLambda\Domain\TaskTags;
use Optimy\CallbackReceiverLambda\Domain\TaskTypes;

class AFBusinessFlow extends AbstractBusinessFlow
{
    private const ADD_REMOVES = [
        'c7968839-2f63-5a3f-8e23-5ca96a89a8e4', //Total SA
        '2ee8a5b6-e2a2-5329-bb5f-6a93cef1e55b', //Fondation Total
        '61e45a42-12d8-5098-89cf-f14c5392e2b6', //NRPTF
        '7565d6a0-68cd-5fd6-9bf5-5f07824837a6'  //Naturnautes
    ];

    private const DATE_ANSWER_NAME = 'Date(s) du/des AF(s)';
    private const AMOUNT_ANSWER_NAME = 'Montant du/des AF(s)';

    private const TOKENS = [
        'amount' => 'answer.amount',
        'date' => 'answer.date'
    ];

    protected function handle(Project $project): bool
    {
        $version = $this->getLastPublishedVersionOfFormPart($project->getId(), FormParts::SAISIE_DES_AF_BUDGET);

        $candidateToTaskCreation = $this->projectAnswerService->getAddRemoveSectionProjectAnswers(
            $project->getId(),
            $version->getId(),
            Forms::REFERENTIEL_CONVENTIONS,
            FormParts::SAISIE_DES_AF_BUDGET,
            FormScreens::SAISIE_DES_AF_BUDGET,
            self::ADD_REMOVES
        );

        // Task creation
        $tasksHaveAllBeenCreated = true;
        foreach ($candidateToTaskCreation as $af) {
            $date = $this->getProjectAnswerByName(self::DATE_ANSWER_NAME, $af);
            $amount = $this->getProjectAnswerByName(self::AMOUNT_ANSWER_NAME, $af);

            if (is_null($date) || is_null($amount)) {
                $this->loggerService->logMissingAnswersInAddRemove($project->getId());
                continue;
            }

            if (!$this->afMetAllTheConditions($af, $project)) {
                $this->loggerService->logTaskNotCreatedConditionsNotMet($project->getId(), Tasks::AF, $date->getId());
                continue;
            }

            $task = $this->taskService->getTaskByTag($project->getId(), TaskTags::AF . $date->getId());

            if (is_null($task)) {
                // Create new task
                $task = $this->buildTask($project, $amount, $date);
            } else {
                // Do we need to update the task ?
                if (!$task->isCompleted() && $task->getdueDate() !== $date->getValue()) {
                    $this->loggerService->logTaskUpdateDueDate($project->getId(), $task->getName(), $date->getId(), $date->getValue());

                    $description = str_replace($task->getDueDate(), $date->getValue(), $task->getDescription());
                    $task->addDescription($description)
                        ->setDueDate($date->getValue());

                } else {
                    $this->loggerService->logTaskExists($project->getId(), $task->getName(), $date->getId());
                    continue;
                }
            }

            $taskCreated = $this->taskService->saveTask($task);
            $tasksHaveAllBeenCreated &= $taskCreated;

            if (!$taskCreated) {
                $this->loggerService->logTaskNotCreated($project->getId(), $task->getName(), $date->getId());
            }
        }

        return $tasksHaveAllBeenCreated;
    }

    /**
     * @param array $addRemoveSection
     * @param \Optimy\Domain\Project $project
     * @return bool
     */
    private function afMetAllTheConditions(array $addRemoveSection, Project $project): bool
    {
        $allReportTasksCompleted = true;

        if (!$this->taskIsNotConditioned($addRemoveSection)) {
            // Create task only if the other task(s) has been completed
            $reportTypes = $this->getReportProjectAnswers($addRemoveSection);

            foreach ($reportTypes as $reportType) {
                $task = $this->taskService->getTaskByTag($project->getId(), TaskTags::RAPPORTS . $reportType);
                // The task must exist and be completed
                $allReportTasksCompleted &= !is_null($task) && $task->isCompleted();
            }
        }

        return $allReportTasksCompleted;
    }

    /**
     * Get list of document type for which the completion of task is necessary
     *
     * @param array $addRemoveSection
     * @return array of document type (string)
     */
    private function getReportProjectAnswers(array $addRemoveSection)
    {
        return array_filter(
            array_map(function ($projectAnswer) {
                preg_match('/^(FS|RF|RI|P)\d/', $projectAnswer->getName(),$matches);
                return array_shift($matches);
            }, $addRemoveSection)
        );
    }

    /**
     * @param Project $project
     * @param ProjectAnswer $amount
     * @param ProjectAnswer $date
     * @return Task
     */
    private function buildTask(Project $project, ProjectAnswer $amount, ProjectAnswer $date): Task
    {
        $tokens = [
            self::TOKENS['date'] => $date->getValue(),
            self::TOKENS['amount'] => $amount->getValue()
        ];

        return $this->taskService->buildTask(
            $project,
            $date->getValue(),
            Tasks::AF,
            TaskTypes::MODELE_1,
            AssigneeRoles::ASSISTANTE,
            EmailTemplates::AF_NON_RECU,
            $tokens,
            TaskTags::AF.$date->getId());
    }

    /**
     * Check if the task is NOT conditioned
     * by the completion of an other one (Report task)
     *
     * @param array $addRemoveSection
     * @return bool
     */
    private function taskIsNotConditioned(array $addRemoveSection): bool
    {
        return count(
            array_filter(
                $addRemoveSection,
                function ($projectAnswer) {
                    return stripos($projectAnswer, 'non') !== false;
                }
            )
        ) > 0;
    }

}