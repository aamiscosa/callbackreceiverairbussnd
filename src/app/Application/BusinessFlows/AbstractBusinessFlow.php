<?php

namespace Optimy\CallbackReceiverLambda\Application\BusinessFlows;


use Optimy\Application\ProjectAnswerService;
use Optimy\CallbackReceiverLambda\Application\LoggerService;
use Optimy\CallbackReceiverLambda\Application\TaskService;
use Optimy\Domain\Project;
use Optimy\Domain\ProjectAnswer;
use Optimy\Domain\ProjectPartVersion;
use Optimy\Infrastructure\Repositories\EmailTemplateRepository;
use Optimy\Infrastructure\Repositories\FormRepository;
use Optimy\Infrastructure\Repositories\ProjectRepository;
use Optimy\Infrastructure\Repositories\TaskAssigneeRepository;
use Optimy\Infrastructure\Repositories\TaskRepository;
use Optimy\Domain\EndOfProcessException;
use Psr\Log\LoggerInterface;

abstract class AbstractBusinessFlow
{
    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var \Optimy\Infrastructure\Repositories\ProjectRepository
     */
    protected $projectRepository;

    /**
     * @var FormRepository
     */
    protected $formRepository;

    /**
     * @var ProjectAnswerService
     */
    protected $projectAnswerService;

    /**
     * @var TaskService
     */
    protected $taskService;

    public function __construct(
        ProjectRepository $projectRepository,
        FormRepository $formRepository,
        ProjectAnswerService $projectAnswerService,
        TaskService $taskService,
        LoggerService $loggerService
    )
    {
        $this->projectRepository = $projectRepository;
        $this->formRepository = $formRepository;
        $this->projectAnswerService = $projectAnswerService;
        $this->taskService = $taskService;
        $this->loggerService = $loggerService;
    }

    // todo refactor 2 first calls
    public function execute(Project $project): bool
    {
        try {
            //todo log which callback we execute
//            $this->logger->info("Executing {$event->getType()->getValue()} with " . join(',', $event->toArray()));
            return $this->handle($project);
            // log end of callback

        } catch (EndOfProcessException $e) {
            // Log info and process for this flow
            // todo remove when testing is done
            throw $e;
//            $this->logger->info($e->getMessage());
            return true;

        } catch (\Exception $exception) {
            // todo remove when testing is done
            throw $exception;

//            $this->logger->warning("Failed: {$event->getType()->getValue()} with " . join(',', $event->toArray()), [$exception->getMessage()]);
            return false;
        }
    }

    abstract protected function handle(Project $project): bool;

    /**
     * @param string $projectId
     * @param string $formPartId
     * @return \Optimy\Domain\ProjectPartVersion
     * @throws EndOfProcessException
     */
    protected function getLastPublishedVersionOfFormPart(string $projectId, string $formPartId): ?ProjectPartVersion
    {
        $lastPublishedVersionOfFormPart = $this->projectRepository->getProjectLastPublishedVersionOfFormPart($projectId, $formPartId);

        if (is_null($lastPublishedVersionOfFormPart)) {
            throw new EndOfProcessException("No last published version of form part {$formPartId} for project id {$projectId}. End of process.");
        }

        return $lastPublishedVersionOfFormPart;
    }

    /**
     * @param string $projectId
     * @param string $versionId
     * @param string $screenId
     * @return \Optimy\Domain\ProjectAnswer[]
     * @throws \Optimy\Domain\EndOfProcessException
     */
//    protected function getFormScreenAnswers(string $projectId, string $versionId, string $screenId)
//    {
//        $answers = $this->projectRepository->getProjectAnswers($projectId, $versionId);
//
//        $answers = array_values(array_filter(
//            $answers,
//            function ($answer) use ($screenId) {
//                return $answer->getScreenId() === $screenId;
//            }));
//
//        if (count($answers) === 0) {
//            throw new EndOfProcessException("Last published version {$versionId} for project {$projectId} does not have any answers published in screen {$screenId}. End of process.");
//        }
//
//        return $answers;
//    }

//    /**
//     * Get all the ProjectAnswers split by add-remove section
//     *
//     * @param \Optimy\Domain\ProjectAnswer[] $answers of the screen
//     * @param array $addRemoveElements
//     * @return array
//     */
//    protected function mapProjectAnswersToAddRemoves(array $answers, array $addRemoveElements): array
//    {
//        // Get all ProjectAnswer ids
//        $answerIds = array_map(
//            function ($answer) {
//                return $answer->getId();
//            },
//            $answers
//        );
//
//        $mappings = [];
//
//        foreach ($addRemoveElements as $addRemoveAnswers) {
//            // If there is a ProjectAnswer for the first element of the add-remove section,
//            // that means the add-remove section has been filled in the project.
//            // Mapping corresponding ProjectAnswers.
//            $projectAnswersExistForAddRemove = in_array($addRemoveAnswers[0]->getId(), $answerIds);
//            if (!$projectAnswersExistForAddRemove) {
//                continue;
//            }
//
//            // We start from the add-remove to keep the order of the answers
//            $mapping = array_map(
//                function ($addRemoveAnswer) use ($answers) {
//                    $projectAnswer = array_filter(
//                        $answers,
//                        function ($answer) use ($addRemoveAnswer) {
//                            return $answer->getId() === $addRemoveAnswer->getId();
//                        });
//                    return array_shift($projectAnswer);
//                },
//                $addRemoveAnswers);
//
//            // We need to filter out null values
//            array_push($mappings, array_values(array_filter($mapping)));
//        }
//
//        return $mappings;
//    }

    /**
     * @param string $answerName
     * @param array $addRemoveSection
     * @return null|\Optimy\Domain\ProjectAnswer
     */
    protected function getProjectAnswerByName(string $answerName, array $addRemoveSection): ?ProjectAnswer
    {
        $answer = array_values(array_filter(
            $addRemoveSection,
            function ($projectAnswer) use ($answerName) {
                return stripos($projectAnswer->getName(), $answerName) !== false;
            }
        ));

        return array_shift($answer);
    }
}