<?php

namespace Optimy\CallbackReceiverLambda\Infrastructure\EventHandlers;


use Optimy\CallbackReceiverLambda\Application\BusinessFlows\AFBusinessFlow;
use Optimy\CallbackReceiverLambda\Application\BusinessFlows\BusinessFlowFactory;
use Optimy\CallbackReceiverLambda\Application\BusinessFlows\BusinessFlows;
use Optimy\CallbackReceiverLambda\Application\BusinessFlows\ReportsBusinessFlow;
use Optimy\Infrastructure\Events\AbstractEvent;
use Optimy\Infrastructure\Events\AbstractEventHandler;
use Optimy\Domain\Project;
use Optimy\Domain\ProjectPartVersion;
use Optimy\Infrastructure\Repositories\ProjectRepository;
use Optimy\Domain\EndOfProcessException;
use Optimy\CallbackReceiverLambda\Utils\FormElements;
use Optimy\CallbackReceiverLambda\Domain\FormParts;
use Optimy\CallbackReceiverLambda\Domain\Forms;
use Optimy\CallbackReceiverLambda\Domain\ProjectStatuses;
use Psr\Log\LoggerInterface;

class ProjectPartVersionSubmittedEventHandler extends AbstractEventHandler
{
    /**
     * @var ProjectRepository
     */
    private $projectRepository;

    /**
     * @var \Optimy\CallbackReceiverLambda\Application\BusinessFlows\BusinessFlowFactory
     */
    private $businessFlowFactory;

    public function __construct(LoggerInterface $logger, BusinessFlowFactory $businessFlowFactory, ProjectRepository $projectRepository)
    {
        parent::__construct($logger);
        $this->businessFlowFactory = $businessFlowFactory;
        $this->projectRepository = $projectRepository;
    }

    /**
     * This handler executes logic for when the event [project.partVersionSubmitted] is triggered.
     * Depending on which form part was submitted and what was done/modified in that version,
     * possible actions can occur (Tasks creation).
     * This handler identifies which form part was modified and dispatch data to the right businessFlow(s)
     * in order to create ad hoc tasks.
     *
     * @param AbstractEvent ProjectPartVersionSubmittedEvent $event
     * @return bool
     * @throws \Exception
     * @throws EndOfProcessException
     */
    protected function handle(AbstractEvent $event)
    {
        $project = $this->getProject($event->getObjectId());
        $version = $this->getProjectVersion($project->getId(), $event->getProjectPartVersionId());

        $allBusinessFlowsWereExecuted = true;
        switch ($version->getFormPartId()) {
            case FormParts::SAISIE_DES_CALENDRIERS:
                if ($project->getStatusId() === ProjectStatuses::PROJET_SIGNE_DES_PARTIES) {
                    $allBusinessFlowsWereExecuted &= $this->businessFlowFactory->create(BusinessFlows::REPORTS_TASK)->execute($project);
                    $allBusinessFlowsWereExecuted &= $this->businessFlowFactory->create(BusinessFlows::COMITE_TASK)->execute($project);
                    $allBusinessFlowsWereExecuted &= $this->businessFlowFactory->create(BusinessFlows::VISUELS_TASK)->execute($project);
                }
                break;
            case FormParts::SAISIE_DES_AF_BUDGET:
                if ($project->getStatusId() === ProjectStatuses::PROJET_CONVENTION_VALIDE) {
                    $allBusinessFlowsWereExecuted &= $this->businessFlowFactory->create(BusinessFlows::LAB_TASK)->execute($project);
                } elseif ($project->getStatusId() === ProjectStatuses::PROJET_SIGNE_DES_PARTIES) {
                    $allBusinessFlowsWereExecuted &= $this->businessFlowFactory->create(BusinessFlows::AF_TASK)->execute($project);
                }
                break;
            case FormParts::GESTION_DES_AF_ATF:
                if ($project->getStatusId() === ProjectStatuses::PROJET_SIGNE_DES_PARTIES) {
                    $allBusinessFlowsWereExecuted &= $this->businessFlowFactory->create(BusinessFlows::ATF_TASK)->execute($project);
                }
                break;
            case FormParts::GESTION_DES_CALENDRIERS:
                $allBusinessFlowsWereExecuted &= $this->businessFlowFactory->create(BusinessFlows::ATF_TASK)->execute($project);
                break;
            default:
                throw new EndOfProcessException("The following status: {$event->getProjectPartVersionId()} is not actionable by this service. End of process.");
        }

        return $allBusinessFlowsWereExecuted;
    }

    /**
     * @param string $projectId
     * @param string $versionId
     * @return null|\Optimy\Domain\ProjectPartVersion
     * @throws \Exception
     */
    private function getProjectVersion(string $projectId, string $versionId): ProjectPartVersion
    {
        $version = $this->projectRepository->getProjectVersionById($projectId, $versionId);

        if (is_null($version)) {
            throw new \Exception('Version does not exist or service was unreachable. Check logs for more details.');
        }

        return $version;
    }

    /**
     * @param string $projectId
     * @return \Optimy\Domain\Project
     * @throws \Exception
     */
    private function getProject(string $projectId): Project
    {
        $project = $this->projectRepository->getProject($projectId);

        if (is_null($project)) {
            throw new \Exception('Project does not exist or service was unreachable.');
        }

        if ($project->getFormId() !== Forms::REFERENTIEL_CONVENTIONS) {
            throw new EndOfProcessException("Wrong formId {$project->getFormId()} for projectId {$projectId}. End of process.");
        }

        return $project;
    }
}