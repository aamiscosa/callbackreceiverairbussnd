<?php

namespace Optimy\CallbackReceiverLambda\Infrastructure\EventHandlers;


use Optimy\CallbackReceiverLambda\Application\BusinessFlows\BusinessFlowFactory;
use Optimy\CallbackReceiverLambda\Application\BusinessFlows\BusinessFlows;
use Optimy\Infrastructure\Events\AbstractEvent;
use Optimy\Infrastructure\Events\AbstractEventHandler;
use Optimy\Domain\ProjectPartVersion;
use Optimy\Infrastructure\Repositories\ProjectRepository;
use Optimy\Domain\EndOfProcessException;
use Optimy\CallbackReceiverLambda\Domain\Forms;
use Optimy\CallbackReceiverLambda\Domain\ProjectStatuses;
use Psr\Log\LoggerInterface;

class ProjectStatusChangedEventHandler extends AbstractEventHandler
{
    /**
     * @var ProjectRepository
     */
    private $projectRepository;

    /**
     * @var BusinessFlowFactory
     */
    private $businessFlowFactory;

    public function __construct(
        LoggerInterface $logger,
        BusinessFlowFactory $businessFlowFactory,
        ProjectRepository $projectRepository
    )
    {
        parent::__construct($logger);
        $this->businessFlowFactory = $businessFlowFactory;
        $this->projectRepository = $projectRepository;
    }

    /**
     * This handler executes logic for when the event [project.statusChanged] is triggered.
     * Depending on the status of a project,
     * possible actions can occur (Tasks creation).
     * This handler identifies which status was set and dispatches data to the right businessFlow(s)
     * in order to create ad hoc tasks.
     *
     * @param AbstractEvent $event
     * @return bool
     * @throws \Optimy\Domain\EndOfProcessException
     * @throws \Exception
     */
    protected function handle(AbstractEvent $event): bool
    {
        $project = $this->getProject($event->getObjectId());
        $allBusinessFlowsWereExecuted = true;

        switch ($event->getProjectStatusId()) {
            case ProjectStatuses::PROJET_CONVENTION_VALIDE:
                $allBusinessFlowsWereExecuted &= $this->businessFlowFactory->create(BusinessFlows::LAB_TASK)->execute($project);
                break;

            case ProjectStatuses::PROJET_SIGNE_DES_PARTIES:
                $allBusinessFlowsWereExecuted &= $this->businessFlowFactory->create(BusinessFlows::REPORTS_TASK)->execute($project);
                $allBusinessFlowsWereExecuted &= $this->businessFlowFactory->create(BusinessFlows::COMITE_TASK)->execute($project);
                $allBusinessFlowsWereExecuted &= $this->businessFlowFactory->create(BusinessFlows::VISUELS_TASK)->execute($project);
                $allBusinessFlowsWereExecuted &= $this->businessFlowFactory->create(BusinessFlows::ATF_TASK)->execute($project);
                $allBusinessFlowsWereExecuted &= $this->businessFlowFactory->create(BusinessFlows::AF_TASK)->execute($project);

                break;

            default:
                throw new EndOfProcessException("The following status: {$event->getProjectStatusId()} is not actionable by this service. End of process.");
        }

        return $allBusinessFlowsWereExecuted;
    }

    /**
     * @param string $projectId
     * @return mixed
     * @throws \Exception
     */
    private function getProject(string $projectId)
    {
        $project = $this->projectRepository->getProject($projectId);

        if (is_null($project)) {
            throw new \Exception('Project does not exist or service was unreachable.');
        }

        if ($project->getFormId() !== Forms::REFERENTIEL_CONVENTIONS) {
            throw new EndOfProcessException("Wrong formId {$project->getFormId()} for projectId {$projectId}. End of process.");
        }

        return $project;
    }

    /**
     * @param array $payload
     */
    public function handleStatusChangeEvent(array $payload)
    {
        $partA = '009e6316-6aea-5093-8d7d-21adef487df2';
        $partB = '156da5fa-8748-57c7-9fb0-9040221bcff7';
        $partC = 'ba56f93d-7ea3-59ce-a39a-56beb74d0e3a';

        $project = $this->projectRepository->getProject($payload['object_id']);
        $versions = $this->projectRepository->getProjectVersions($project->getId());

        if($project->getCurrentFormPartId()==$partA) {
            if($versions > 1 && $project->getStatusId() == ProjectStatuses::PROJECT_PENDING) {
                $history = $this->projectRepository->getProjectHistory($project->getId());
                for($i=count($history)-1;$i>0;$i--) { // tldr; ...but never version 1. $i>0
                    if(ProjectStatuses::parseStatus 
                        ($history[$i]->getEventDescription()) != ProjectStatuses::PROJECT_PENDING
                    ) {
                        $response = $this->projectRepository->setProjectStatus (
                            $project->getId(), 
                            ProjectStatuses::parseStatus($history[$i]->getEventDescription())
                        );
                        print_r($response);
                        break;
                    }
                }
            }
        }
        else if ($project->getCurrentFormPartId()==$partB) {
            // do nothing
        }
        else if ($project->getCurrentFormPartId()==$partC) {
            if($project->getStatusId() == ProjectStatuses::PROJECT_PENDING) {
                $history = $this->projectRepository->getProjectHistory($project->getId());
                for($i=count($history)-1;$i>=0;$i--) { // ...all versions are concerned. $i>=0
                    if(ProjectStatuses::parseStatus 
                        ($history[$i]->getEventDescription()) != ProjectStatuses::PROJECT_PENDING
                    ) {
                        $response = $this->projectRepository->setProjectStatus (
                            $project->getId(), 
                            ProjectStatuses::parseStatus($history[$i]->getEventDescription())
                        );
                        print_r($response);
                        break;
                    }
                }
            }
        }
    }
}