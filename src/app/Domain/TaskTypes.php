<?php

namespace Optimy\CallbackReceiverLambda\Domain;


use Esky\Enum\Enum;

class TaskTypes extends Enum
{
    public const MODELE_1 = 'df57e6c5-5900-5b0d-8d7f-6d1aebfcaf8b';
    public const MODELE_2 = '88cc77fc-e5bd-5a41-bd25-15a20380035f';
    public const MODELE_3 = '9bf137c1-0f64-5b95-9b01-d75a411cfcee';
    public const MODELE_4 = '04adf01f-9d73-55d1-ad77-3860d9bcc6fe';
    public const MODELE_5 = '942391fd-c14b-5d41-9cbf-4f25f93931fc';
    public const MODELE_6 = '2ed5973e-d075-5eda-9ecd-e25a8e4dccdc';
    public const MODELE_7 = '623d15b9-f2d7-51ca-9ac1-541a9d7f9d60'; // LAB
}