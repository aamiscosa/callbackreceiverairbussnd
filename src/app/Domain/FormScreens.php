<?php

namespace Optimy\CallbackReceiverLambda\Domain;


use Esky\Enum\Enum;

class FormScreens extends Enum
{
    public const SAISIE_DES_AF_BUDGET = '87aba2c9-68b0-59c9-a952-c662eb433106';
    public const SAISIE_DES_CALENDRIERS = '286c5fb3-3456-590d-a04b-b5b4e2213718';
    public const INFORMATIONS_JURIDIQUES = 'c8624833-ea12-54b9-b015-53a5d27e652c';
    public const GESTION_DES_AF_ATF = '4d984026-597c-531d-bfa7-913c671cb509';
    public const CONTREPARTIES = '1bdfe53b-c613-5778-8f4e-9f4633ce4aef';
    public const GESTION_DES_CALENDRIERS = '/0e10b949-6c68-5ebf-b1f7-0bfd91a93474';
}