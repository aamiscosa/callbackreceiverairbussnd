<?php

namespace Optimy\CallbackReceiverLambda\Domain;


class AssigneeRoles
{
    public const RESPONSABLE_DE_PROGRAMME = '89d0847b-5a26-5195-991e-a9a4017facbe';
    public const CHEF_DE_PROJET = '16d95479-4d95-502b-8bc7-0a8285c4fd6f';
    public const RP = 'a6860d74-eeea-5047-ab45-837cc24c33cf';
    public const ASSISTANTE = 'd90a4748-e332-5e7f-96c1-4b1b5343b50b';
}