<?php

namespace Optimy\CallbackReceiverLambda\Domain;


use Esky\Enum\Enum;

class EmailTemplates extends Enum
{
    public const LAB = 15420;
    public const RAPPORT_NON_REMIS = 14378;
    public const AF_NON_RECU = 14373;
    public const COMITE_NON_TENU = 14374;
    public const VISUELS_NON_RECUS = 14379;
    public const ATF_NON_RECU = 14381;
    public const ECHEANCES_CONTREPARTIES = 14382;
}