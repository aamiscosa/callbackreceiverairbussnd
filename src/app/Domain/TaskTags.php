<?php

namespace Optimy\CallbackReceiverLambda\Domain;


use Esky\Enum\Enum;

class TaskTags extends Enum
{
    public const LAB = 'api-lab-check';
    public const RAPPORTS =  'missing_report_';
    public const VISUELS = 'missing_visual_';
    public const COMITE = 'comittee_';
    public const CONTREPARTIES = 'echeance_contrepartie_';
    public const ATF = 'atf_not_received_';
    public const AF = 'af_unpaid_';
}