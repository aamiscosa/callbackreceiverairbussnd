<?php

namespace Optimy\CallbackReceiverLambda\Domain;


use Esky\Enum\Enum;

class Tasks extends Enum
{
    public const LAB = 'Vérifier que la procédure de LAB ait lieu pour le Partenaire';
    public const REPORTS = [
        'FS' => 'Fiche de synthèse non reçue',
        'RI' => 'Rapport intermédiaire non reçu',
        'RF' => 'Rapport final non reçu',
        'P' => 'Planning'
    ];
    public const COMITE = 'Comité non tenu';
    public const VISUEL = 'Visuels non reçu';
    public const AF = 'AF non reçue';
    public const ATF = 'ATF non reçue';
    public const CONTREPARTIES = 'Echéance des Contreparties';

}