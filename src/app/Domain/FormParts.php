<?php

namespace Optimy\CallbackReceiverLambda\Domain;


use Esky\Enum\Enum;

class FormParts extends Enum
{
    public const SAISIE_DES_AF_BUDGET = '058b6cd8-74da-5c46-ac40-4fd682e60939';
    public const SAISIE_DES_CALENDRIERS = '9546eb08-c9fb-51bf-ad28-e65f0415292e';
    public const INFORMATIONS_JURIDIQUES = '14b21e3b-5ab7-5a0e-9848-2d74fc87caa3';
    public const GESTION_DES_AF_ATF = '9f39f9a2-89cf-5e87-a778-a01f2f6dd40a';
    public const GESTION_DES_CALENDRIERS = '33c19640-cb48-5857-9e54-20655d7fcb2a';
    public const CONTREPARTIES = '48d05763-ccac-506a-b825-94d86372d007';
}