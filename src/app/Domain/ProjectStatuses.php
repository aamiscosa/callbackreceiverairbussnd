<?php

namespace Optimy\CallbackReceiverLambda\Domain;


use Esky\Enum\Enum;

class ProjectStatuses extends Enum
{
    // public const PROJET_SIGNE_DES_PARTIES = 'd571a857-9aa0-5406-b232-2d335f58cef4';
    // public const PROJET_TERMINE = '74f1cfa6-46b5-5a10-95db-1768432cb4f1';
    // public const PROJET_CONVENTION_VALIDE = '0da98647-c9c7-5e3a-8844-ae6d4912fbf9';
    // public const LAB_NA = 'de3346ec-dbff-58d8-8763-0c1c7b0537e6';
    // public const LAB_OK = 'e2844538-3592-5e36-84fc-c9119492c1e1';
    //

    /**
     * Need to replace values below with client values. 
     * Current values are values from local.
     */
    // public const PROJECT_SND_PENDING = '02fcb5ce-59d0-57a8-8bf3-5543bf07440f';
    public const PROJECT_PENDING = 'e0bf2faa-c445-516f-9b08-6e3c3d14a182';
    public const PROJECT_EXCLUDED = '28917933-32d7-5921-a371-9f28301d5d42';
    public const PROJECT_APPROVED = 'a1e29ebd-fa54-5be9-9fd6-fa1d19b2f181';

    public static function parseStatus(string $status) {
        switch($status) {
            case "Excluded by Optimy Tool ": // space after Tool is intended.
                return ProjectStatuses::PROJECT_EXCLUDED;
            case "Approved":
                return ProjectStatuses::PROJECT_APPROVED;
            default:
                return ProjectStatuses::PROJECT_PENDING;
        }
    }
}