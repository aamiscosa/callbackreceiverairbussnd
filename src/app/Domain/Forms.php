<?php

namespace Optimy\CallbackReceiverLambda\Domain;


use Esky\Enum\Enum;

class Forms extends Enum
{
    public const REFERENTIEL_CONVENTIONS = 'ea7dda4d-80c1-57cc-8b7f-8d5c5e027cc9';
    public const REFERENTIEL_PARTENAIRES = '0dcbae7e-c128-5710-9646-cf26dacf86f5';
}