provider "aws" {}
data "aws_caller_identity" "current" {}
variable "lambda_filename" {}
terraform {
	backend "consul" {
		address = "https://consul.kubernetes.infra.optimy.net"
		path = "terraform/prod/callback-receiver"
		gzip = true
	}
}


# -----------------------------------------------------------------------------
# -- API Gateway --------------------------------------------------------------
# -----------------------------------------------------------------------------

resource "aws_api_gateway_rest_api" "api" {
	name = "callback-receiver"
}

resource "aws_api_gateway_method" "method" {
	rest_api_id = "${aws_api_gateway_rest_api.api.id}"
	resource_id = "${aws_api_gateway_rest_api.api.root_resource_id}"
	http_method = "POST"
	authorization = "NONE"
}

resource "aws_api_gateway_integration" "integration" {
	rest_api_id = "${aws_api_gateway_rest_api.api.id}"
	resource_id = "${aws_api_gateway_rest_api.api.root_resource_id}"
	http_method = "${aws_api_gateway_method.method.http_method}"
	integration_http_method = "POST"
	type = "AWS" 
	passthrough_behavior = "WHEN_NO_MATCH"
	uri = "arn:aws:apigateway:eu-central-1:lambda:path/2015-03-31/functions/${aws_lambda_function.lambda.arn}/invocations"
}

resource "aws_api_gateway_method_response" "200" {
	rest_api_id = "${aws_api_gateway_rest_api.api.id}"
	resource_id = "${aws_api_gateway_rest_api.api.root_resource_id}"
	http_method = "${aws_api_gateway_method.method.http_method}"
	status_code = "200"
}

resource "aws_api_gateway_integration_response" "integration_response" {
	rest_api_id = "${aws_api_gateway_rest_api.api.id}"
	resource_id = "${aws_api_gateway_rest_api.api.root_resource_id}"
	http_method = "${aws_api_gateway_method.method.http_method}"
	status_code = "${aws_api_gateway_method_response.200.status_code}"
	depends_on = ["aws_api_gateway_integration.integration"]
}


# -----------------------------------------------------------------------------
# -- Lambda function ----------------------------------------------------------
# -----------------------------------------------------------------------------

resource "aws_lambda_permission" "lambda" {
	statement_id = "AllowExecutionFromAPIGateway"
	action = "lambda:InvokeFunction"
	function_name = "${aws_lambda_function.lambda.arn}"
	principal = "apigateway.amazonaws.com"
	source_arn = "arn:aws:execute-api:eu-central-1:${data.aws_caller_identity.current.account_id}:${aws_api_gateway_rest_api.api.id}/*/*"
}

resource "aws_lambda_function" "lambda" {
	filename = "./../../dist/${var.lambda_filename}"
	function_name = "callback-receiver"
	memory_size =  128
	timeout = 10
	role = "${aws_iam_role.role.arn}"
	handler = "php.handler"
	runtime = "nodejs6.10"
}

resource "aws_api_gateway_deployment" "prod" {
	depends_on = ["aws_api_gateway_method.method", "aws_api_gateway_integration.integration", "aws_api_gateway_integration_response.integration_response"]
	rest_api_id = "${aws_api_gateway_rest_api.api.id}"
	stage_name = "prod"
}

resource "aws_iam_role" "role" {
	name = "callback-receiver-role"
	assume_role_policy = <<EOF
{
	"Version": "2012-10-17",
	"Statement": [
		{
			"Action": "sts:AssumeRole",
			"Principal": {
				"Service": "lambda.amazonaws.com"
			},
			"Effect": "Allow",
			"Sid": ""
		}
	]
}
EOF
}

resource "aws_iam_policy" "policy" {
	name = "callback-receiver-policy"
	policy = <<EOF
{
	"Version": "2012-10-17",
	"Statement": [
		{
			"Effect": "Allow",
			"Action": [
				"logs:CreateLogGroup",
				"logs:CreateLogStream",
				"logs:PutLogEvents"
			],
			"Resource": "arn:aws:logs:*:*:*"
		}
	]
}
EOF
}

resource "aws_iam_role_policy_attachment" "attach" {
	role = "${aws_iam_role.role.name}"
	policy_arn = "${aws_iam_policy.policy.arn}"
}


# -----------------------------------------------------------------------------
# -- Output -------------------------------------------------------------------
# -----------------------------------------------------------------------------

output "api_gateway_url" {
	value = "${aws_api_gateway_deployment.prod.invoke_url}"
}