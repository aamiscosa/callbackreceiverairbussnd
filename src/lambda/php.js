process.env['PATH'] = process.env['PATH'] + ':' + process.env['LAMBDA_TASK_ROOT'];

const spawn = require('child_process').spawn;

exports.handler = function(event, context, callback) {
	var php = spawn('bin/php',['run.php']);
	var error = null;
	var outcome = 'OK';

	// send the input event JSON as string via STDIN to the PHP process
	php.stdin.write(JSON.stringify(event));

	// close the PHP stream to unblock the PHP process
	php.stdin.end();

	// dynamically logging PHP output
	php.stdout.on('data', function(data) {
		console.log('STDOUT: ' + data);
	});

	// dynamically logging errors
	php.stderr.on('data', function(data) {
		if (error === null) {
			error = '';
		}
		error += data + '\n';
		console.log('STDERR: ' + data);
	});

	// finalize when PHP process is done.
	php.on('close', function(code) {
		if (code !== 0) {
			if (error === null) {
				error = '';
			}
			error += 'PHP process exited with non-zero code [' + code + ']';
		}
		
		if (error !== null) {
			outcome = 'ERROR';
		}
		
		console.log('PHP process closed with code [' + code + ']');
		callback(error, outcome);
	});
}